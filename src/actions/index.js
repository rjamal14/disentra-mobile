import AsyncStorage from '@react-native-async-storage/async-storage';
import {ToastAndroid} from 'react-native';

const removeToken = async () => {
  const keys = [
    'user_email',
    'access_token',
    'token_type',
    'created_at',
    'name',
    'verified',
    'photo',
  ];
  await AsyncStorage.multiRemove(keys);
};
const Func = {
  removeToken: async () => {
    removeToken();
    ToastAndroid.showWithGravityAndOffset(
      'Sesi login telah berakhir !',
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50,
    );
  },
  FormatDate(date) {
    if (date !== undefined) {
      var tahun = date.getFullYear();
      var bulan = date.getMonth();
      var tanggal = date.getDate();
      var hari = date.getDay();
      switch (hari) {
        case 0:
          hari = 'Minggu';
          break;
        case 1:
          hari = 'Senin';
          break;
        case 2:
          hari = 'Selasa';
          break;
        case 3:
          hari = 'Rabu';
          break;
        case 4:
          hari = 'Kamis';
          break;
        case 5:
          hari = "Jum'at";
          break;
        case 6:
          hari = 'Sabtu';
          break;
      }
      switch (bulan) {
        case 0:
          bulan = 'Januari';
          break;
        case 1:
          bulan = 'Februari';
          break;
        case 2:
          bulan = 'Maret';
          break;
        case 3:
          bulan = 'April';
          break;
        case 4:
          bulan = 'Mei';
          break;
        case 5:
          bulan = 'Juni';
          break;
        case 6:
          bulan = 'Juli';
          break;
        case 7:
          bulan = 'Agustus';
          break;
        case 8:
          bulan = 'September';
          break;
        case 9:
          bulan = 'Oktober';
          break;
        case 10:
          bulan = 'November';
          break;
        case 11:
          bulan = 'Desember';
          break;
      }
      var tampilTanggal = hari + ', ' + tanggal + ' ' + bulan + ' ' + tahun;
      return tampilTanggal;
    }
  },
};

export default Func;
