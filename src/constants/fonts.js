const ft = {
  mul: 'Mukta-Light',
  mur: 'Mukta-Regular',
  mum: 'Mukta-Medium',
  mub: 'Mukta-Bold',
  musb: 'Mukta-SemiBold',
  mui: 'Mukta-Italic',
  mubi: 'Mukta-BoldItalic',
  asl: 'Asap-Light',
  asr: 'Asap-Regular',
  asm: 'Asap-Medium',
  asb: 'Asap-Bold',
  asi: 'Asap-Italic',
  asbi: 'Asap-BoldItalic',
  Mukta: 'Mukta',
  xsm: 7,
  sml: 8,
  med: 10,
  lag: 12,
  xla: 15,
};

export default ft;
