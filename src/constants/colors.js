export const white = '#ffffff';
export const white1 = '#f6f6f6';
export const blue = '#7ED8FF';
export const blue1 = '#29AAE2';
export const blue2 = '#165581';
export const red = '#E22929';
export const yellow = '#EFCA18';
