import {StyleSheet} from 'react-native';
import _ from '../../helpers/responsive';
import {blue2, white} from '../../constants/colors';
import ft from '../../constants/fonts';

export default StyleSheet.create({
  textSolid: {
    color: white,
    fontSize: _.fz(ft.sml),
    fontFamily: ft.mur,
  },
  textOutlined: {
    color: blue2,
    fontSize: _.fz(ft.sml),
    fontFamily: ft.mur,
  },
  buttonSolid: {
    width: _.wp(80),
    height: _.wp(13),
    borderRadius: 5,
    backgroundColor: blue2,
    marginVertical: _.hp(2),
  },
  buttonOutlined: {
    width: _.wp(80),
    height: _.wp(13),
    borderRadius: 5,
    borderColor: blue2,
    borderWidth: 2,
  },
});
