import {StyleSheet} from 'react-native';
import _ from '../../helpers/responsive';
import {blue2, white, white1, red} from '../../constants/colors';
import ft from '../../constants/fonts';
export default StyleSheet.create({
  textSolid: {
    color: white,
    fontSize: _.fz(ft.sml),
    fontFamily: ft.mur,
  },
  textOutlined: {
    color: blue2,
    fontSize: _.fz(ft.sml),
    fontFamily: ft.mur,
  },
  inputContainerStyle: {
    width: _.wp(80),
    backgroundColor: white,
    borderWidth: 0.5,
    borderColor: blue2,
    paddingHorizontal: _.wp(2),
    borderRadius: 5,
    alignSelf: 'center',
    alignItems: 'center',
  },
  inputContainerStyleFocus: {
    width: _.wp(80),
    backgroundColor: white,
    borderWidth: 1,
    borderColor: blue2,
    paddingHorizontal: _.wp(2),
    borderRadius: 5,
    alignSelf: 'center',
    alignItems: 'center',
  },
  inputContainerStyleFocus2: {
    width: _.wp(65),
    height: _.hp(6),
    backgroundColor: white,
    borderWidth: 1,
    borderColor: '#C4C4C4',
    paddingHorizontal: _.wp(2),
    borderRadius: 5,
    alignItems: 'center',
  },
  inputContainerStyleFocus3: {
    width: _.wp(90),
    height: _.hp(6),
    backgroundColor: white,
    borderWidth: 1,
    borderColor: '#C4C4C4',
    paddingHorizontal: _.wp(2),
    borderRadius: 5,
    alignItems: 'center',
  },
  inputContainerStyleFocus4: {
    width: _.wp(85),
    height: _.hp(6),
    backgroundColor: white,
    borderWidth: 1,
    borderColor: '#C4C4C4',
    paddingHorizontal: _.wp(2),
    borderRadius: 5,
    alignItems: 'center',
  },
  inputContainerStyleFocus5: {
    width: _.wp(95),
    height: _.hp(6),
    backgroundColor: white,
    borderWidth: 1,
    borderColor: '#C4C4C4',
    paddingHorizontal: _.wp(2),
    borderRadius: 5,
    alignItems: 'center',
  },
  inputContainerStyleError: {
    width: _.wp(80),
    backgroundColor: white,
    borderWidth: 1,
    borderColor: red,
    paddingHorizontal: _.wp(2),
    borderRadius: 5,
    alignSelf: 'center',
    alignItems: 'center',
  },
  inputStyle: {
    fontFamily: ft.mur,
    fontSize: _.fz(ft.sml),
  },
  errorMessage: {
    fontFamily: ft.mur,
    fontSize: _.fz(ft.xsm),
    color: red,
    width: _.wp(80),
    position: 'absolute',
    alignSelf: 'center',
    top: 50,
  },
  successMessage: {
    fontFamily: ft.mur,
    fontSize: _.fz(ft.xsm),
    color: blue2,
    width: _.wp(80),
    position: 'absolute',
    alignSelf: 'center',
    top: _.hp(6),
  },
});
