import React from 'react';
import {Button} from 'react-native-elements';
import styles from '../components/styles/ButtonStyle';

export class ButtonL extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFocused: false,
    };
  }

  render() {
    if (this.props.type === 'outline') {
      return (
        <Button
          loading={this.props.loading}
          title={this.props.title}
          type={this.props.type}
          buttonStyle={styles.buttonOutlined}
          titleStyle={styles.textOutlined}
          onPress={this.props.onPress}
        />
      );
    } else if (this.props.type === 'outline-icon') {
      return (
        <Button
          loading={this.props.loading}
          icon={this.props.icon}
          iconRight
          title={this.props.title}
          type={'outlined'}
          buttonStyle={styles.buttonOutlined}
          titleStyle={styles.textOutlined}
          onPress={this.props.onPress}
        />
      );
    } else {
      return (
        <Button
          loading={this.props.loading}
          title={this.props.title}
          type={this.props.type}
          buttonStyle={styles.buttonSolid}
          titleStyle={styles.textSolid}
          onPress={this.props.onPress}
        />
      );
    }
  }
}
