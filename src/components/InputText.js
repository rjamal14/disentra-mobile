import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {Input} from 'react-native-elements';
import styles from '../components/styles/InputTextStyle';
import {blue2} from '../constants/colors';
export default class InputText extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFocused: false,
      isMessage: false,
    };
  }
  _onChangeText = (updatedValue) => {
    const {attrName, updateMasterState} = this.props;
    updateMasterState(attrName, updatedValue);
  };
  render() {
    if (this.props.type === 'email') {
      return (
        <View>
          <Input
            inputContainerStyle={
              this.props.message.type === 'error'
                ? styles.inputContainerStyleError
                : this.state.isFocused
                ? styles.inputContainerStyleFocus
                : styles.inputContainerStyle
            }
            selectionColor={blue2}
            inputStyle={styles.inputStyle}
            placeholder={this.props.placeholder}
            onFocus={() => this.setState({isFocused: true})}
            onBlur={() => this.setState({isFocused: false})}
            onChangeText={this._onChangeText}
            value={this.props.value}
            keyboardType={'email-address'}
            autoCapitalize={'none'}
            leftIcon={this.props.icons}
          />
          {!this.props.showMessage ? null : this.props.message.type ===
            'error' ? (
            <Text style={styles.errorMessage}>{this.props.message.text}</Text>
          ) : (
            <Text style={styles.successMessage}>{this.props.message.text}</Text>
          )}
        </View>
      );
    } else if (this.props.type === 'password') {
      return (
        <View>
          <Input
            inputContainerStyle={
              this.props.message.type === 'error'
                ? styles.inputContainerStyleError
                : this.state.isFocused
                ? styles.inputContainerStyleFocus
                : styles.inputContainerStyle
            }
            selectionColor={blue2}
            inputStyle={styles.inputStyle}
            placeholder={this.props.placeholder}
            onFocus={() => this.setState({isFocused: true})}
            onBlur={() => this.setState({isFocused: false})}
            secureTextEntry={true}
            onChangeText={this._onChangeText}
            value={this.props.value}
            leftIcon={this.props.icons}
          />
          {!this.props.showMessage ? null : this.props.message.type ===
            'error' ? (
            <Text style={styles.errorMessage}>{this.props.message.text}</Text>
          ) : (
            <Text style={styles.successMessage}>{this.props.message.text}</Text>
          )}
        </View>
      );
    } else if (this.props.type === 'number') {
      return (
        <View>
          <Input
            inputContainerStyle={
              this.props.message.type === 'error'
                ? styles.inputContainerStyleError
                : this.state.isFocused
                ? styles.inputContainerStyleFocus
                : styles.inputContainerStyle
            }
            selectionColor={blue2}
            inputStyle={styles.inputStyle}
            placeholder={this.props.placeholder}
            onFocus={() => this.setState({isFocused: true})}
            onBlur={() => this.setState({isFocused: false})}
            onChangeText={this._onChangeText}
            value={this.props.value}
            keyboardType={'numeric'}
            leftIcon={this.props.icons}
          />
          {!this.props.showMessage ? null : this.props.message.type ===
            'error' ? (
            <Text style={styles.errorMessage}>{this.props.message.text}</Text>
          ) : (
            <Text style={styles.successMessage}>{this.props.message.text}</Text>
          )}
        </View>
      );
    } else if (this.props.type === 'search') {
      return (
        <View>
          <Input
            inputContainerStyle={
              this.props.message.type === 'error'
                ? styles.inputContainerStyleError
                : this.state.isFocused
                ? styles.inputContainerStyleFocus2
                : styles.inputContainerStyleFocus2
            }
            selectionColor={blue2}
            inputStyle={styles.inputStyle}
            autoCompleteType="off"
            placeholder={this.props.placeholder}
            onFocus={() => this.setState({isFocused: true})}
            onBlur={() => this.setState({isFocused: false})}
            onChangeText={this._onChangeText}
            onEndEditing={this.props.onEndEditing}
            value={this.props.value}
            keyboardType={'default'}
            rightIcon={this.props.icons}
          />
          {!this.props.showMessage ? null : this.props.message.type ===
            'error' ? (
            <Text style={styles.errorMessage}>{this.props.message.text}</Text>
          ) : (
            <Text style={styles.successMessage}>{this.props.message.text}</Text>
          )}
        </View>
      );
    } else if (this.props.type === 'searchMed') {
      return (
        <View>
          <Input
            inputContainerStyle={
              this.props.message.type === 'error'
                ? styles.inputContainerStyleError
                : this.state.isFocused
                ? styles.inputContainerStyleFocus4
                : styles.inputContainerStyleFocus4
            }
            selectionColor={blue2}
            inputStyle={styles.inputStyle}
            autoCompleteType="off"
            placeholder={this.props.placeholder}
            onFocus={() => this.setState({isFocused: true})}
            onBlur={() => this.setState({isFocused: false})}
            onChangeText={this._onChangeText}
            onEndEditing={this.props.onEndEditing}
            value={this.props.value}
            keyboardType={'default'}
            rightIcon={this.props.icons}
          />
          {!this.props.showMessage ? null : this.props.message.type ===
            'error' ? (
            <Text style={styles.errorMessage}>{this.props.message.text}</Text>
          ) : (
            <Text style={styles.successMessage}>{this.props.message.text}</Text>
          )}
        </View>
      );
    } else if (this.props.type === 'searchlong') {
      return (
        <View>
          <Input
            inputContainerStyle={
              this.props.message.type === 'error'
                ? styles.inputContainerStyleError
                : this.state.isFocused
                ? styles.inputContainerStyleFocus3
                : styles.inputContainerStyleFocus3
            }
            selectionColor={blue2}
            inputStyle={styles.inputStyle}
            autoCompleteType="off"
            placeholder={this.props.placeholder}
            onFocus={() => this.setState({isFocused: true})}
            onBlur={() => this.setState({isFocused: false})}
            onChangeText={this._onChangeText}
            onEndEditing={this.props.onEndEditing}
            value={this.props.value}
            keyboardType={'default'}
            rightIcon={this.props.icons}
          />
          {!this.props.showMessage ? null : this.props.message.type ===
            'error' ? (
            <Text style={styles.errorMessage}>{this.props.message.text}</Text>
          ) : (
            <Text style={styles.successMessage}>{this.props.message.text}</Text>
          )}
        </View>
      );
    } else if (this.props.type === 'searchBisinis') {
      return (
        <View>
          <Input
            inputContainerStyle={
              this.props.message.type === 'error'
                ? styles.inputContainerStyleError
                : this.state.isFocused
                ? styles.inputContainerStyleFocus5
                : styles.inputContainerStyleFocus5
            }
            selectionColor={blue2}
            inputStyle={styles.inputStyle}
            autoCompleteType="off"
            placeholder={this.props.placeholder}
            onFocus={() => this.setState({isFocused: true})}
            onBlur={() => this.setState({isFocused: false})}
            onChangeText={this._onChangeText}
            onEndEditing={this.props.onEndEditing}
            value={this.props.value}
            keyboardType={'default'}
            rightIcon={this.props.icons}
          />
          {!this.props.showMessage ? null : this.props.message.type ===
            'error' ? (
            <Text style={styles.errorMessage}>{this.props.message.text}</Text>
          ) : (
            <Text style={styles.successMessage}>{this.props.message.text}</Text>
          )}
        </View>
      );
    } else {
      return (
        <View>
          <Input
            inputContainerStyle={
              this.props.message.type === 'error'
                ? styles.inputContainerStyleError
                : this.state.isFocused
                ? styles.inputContainerStyleFocus
                : styles.inputContainerStyle
            }
            selectionColor={blue2}
            inputStyle={styles.inputStyle}
            placeholder={this.props.placeholder}
            onFocus={() => this.setState({isFocused: true})}
            onBlur={() => this.setState({isFocused: false})}
            onChangeText={this._onChangeText}
            value={this.props.value}
            keyboardType={'default'}
            leftIcon={this.props.icons}
          />
          {!this.props.showMessage ? null : this.props.message.type ===
            'error' ? (
            <Text style={styles.errorMessage}>{this.props.message.text}</Text>
          ) : (
            <Text style={styles.successMessage}>{this.props.message.text}</Text>
          )}
        </View>
      );
    }
  }
}
