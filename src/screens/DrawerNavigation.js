import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';

function WelcomeScreen(props) {
  const {navigation} = props;
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      navigation.openDrawer();
      navigation.goBack();
    });

    return unsubscribe;
  }, [navigation]);

  return <SafeAreaView />;
}

export default WelcomeScreen;
