/* eslint-disable react/no-did-update-set-state */
import React from 'react';
import {Image, Text, View, KeyboardAvoidingView} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from '../screens/styles/ForgotPasswordStyle';
import {white, white1} from '../constants/colors';
import {ButtonL} from '../components/Buttons';
import InputText from '../components/InputText';
import LinearGradient from 'react-native-linear-gradient';
import IconLock from '../assets/icons/ic_lock.svg';
import PaperPlane from '../assets/images/success-check.svg';

export default class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFocused: false,
      password: '',
      passwordConfirm: '',
      isErrorPassword: false,
      messagePassword: '',
      showMessagePassword: true,
      isErrorPasswordConfirm: false,
      messagePasswordConfirm: '',
      showMessagePasswordConfirm: false,
      isLoading: false,
      isPasswordSend: false,
    };
  }

  _updateMasterState = (attrName, value) => {
    if (attrName === 'password' && this.state.isErrorPassword) {
      this.setState({
        isErrorPassword: false,
        showMessagePassword: false,
      });
    }
    if (attrName === 'passwordConfirm' && this.state.isErrorPasswordConfirm) {
      this.setState({
        isErrorPasswordConfirm: false,
        showMessagePasswordConfirm: false,
      });
    }
    this.setState({[attrName]: value});
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.password !== this.state.password ||
      prevState.passwordConfirm !== this.state.passwordConfirm
    ) {
      if (
        this.state.passwordConfirm !== '' &&
        this.state.passwordConfirm !== this.state.password
      ) {
        this.setState({
          isErrorPasswordConfirm: true,
          messagePasswordConfirm: 'Kata Sandi Tidak Cocok',
          showMessagePasswordConfirm: true,
        });
      } else {
        if (this.state.passwordConfirm !== '') {
          this.setState({
            isErrorPasswordConfirm: false,
            messagePasswordConfirm: 'Kata Sandi Cocok',
            showMessagePasswordConfirm: true,
          });
        } else {
          this.setState({
            isErrorPasswordConfirm: false,
            messagePasswordConfirm: 'Kata Sandi Cocok',
            showMessagePasswordConfirm: false,
          });
        }
      }
    }
  }

  render() {
    if (!this.state.isPasswordSend) {
      return (
        <LinearGradient colors={[white, white1]} style={styles.gradient}>
          <KeyboardAvoidingView style={styles.container}>
            <SafeAreaView style={styles.innerContainer}>
              <Image
                style={styles.logoIcon}
                source={require('../assets/images/logo-bjb-color.png')}
              />
              <Image
                style={styles.handAbove}
                source={require('../assets/images/bg-hand-above.png')}
              />
              <Image
                style={styles.logo}
                source={require('../assets/images/logo-color.png')}
              />
              <View style={styles.ContentContainer}>
                <Text style={styles.textHeader}>Buat Kata Sandi</Text>
                <Text style={styles.textContent}>
                  Buat kata sandi baru minimal 8 karakter yang terdiri dari
                  kombinasi huruf, angka, dan simbol
                </Text>
              </View>
              {this.state.isError ? (
                <Text style={styles.errorMessage}>
                  {this.state.errorMessage}
                </Text>
              ) : null}
              <InputText
                type={'password'}
                placeholder={'Password'}
                showMessage={this.state.showMessagePassword}
                value={this.state.password}
                attrName={'password'}
                updateMasterState={this._updateMasterState}
                message={{
                  type: this.state.isErrorPassword ? 'error' : '',
                  text: this.state.messagePassword,
                }}
                icons={<IconLock />}
              />
              <InputText
                type={'password'}
                placeholder={'Konfirmasi Kata Sandi'}
                showMessage={this.state.showMessagePasswordConfirm}
                value={this.state.passwordConfirm}
                attrName={'passwordConfirm'}
                updateMasterState={this._updateMasterState}
                message={{
                  type: this.state.isErrorPasswordConfirm ? 'error' : '',
                  text: this.state.messagePasswordConfirm,
                }}
                icons={<IconLock />}
              />
              <ButtonL
                title="KIRIM"
                loading={this.state.isLoading}
                onPress={() => {
                  if (this.state.password === '') {
                    this.setState({
                      isErrorPassword: true,
                      messagePassword: 'Kata Sandi Tidak Boleh Kosong',
                      showMessagePassword: true,
                    });
                  }
                  if (this.state.passwordConfirm === '') {
                    this.setState({
                      isErrorPasswordConfirm: true,
                      messagePasswordConfirm:
                        'Konfirmasi Kata Sandi Tidak Boleh Kosong',
                      showMessagePasswordConfirm: true,
                    });
                  }
                  if (
                    this.state.passwordConfirm === '' ||
                    this.state.password === '' ||
                    this.state.passwordConfirm !== this.state.password
                  ) {
                  } else {
                    this.setState({
                      isErrorEmail: false,
                      showMessagePassword: false,
                      isLoading: true,
                    });
                    setTimeout(() => {
                      this.setState({
                        isLoading: false,
                        isPasswordSend: true,
                      });
                    }, 2000);
                  }
                }}
              />
              <Text
                onPress={() => {
                  this.props.navigation.navigate('Login');
                }}
                style={styles.textLink2}>
                Punya akun?<Text style={styles.textLink}> masuk</Text>
              </Text>
            </SafeAreaView>
          </KeyboardAvoidingView>
        </LinearGradient>
      );
    } else {
      return (
        <LinearGradient colors={[white, white1]} style={styles.gradient}>
          <SafeAreaView style={styles.innerContainer}>
            <Image
              style={styles.logoIcon}
              source={require('../assets/images/logo-bjb-color.png')}
            />
            <Image
              style={styles.handAbove}
              source={require('../assets/images/bg-hand-above.png')}
            />
            <Image
              style={styles.logo}
              source={require('../assets/images/logo-color.png')}
            />
            <View style={styles.ContentContainer}>
              <PaperPlane />
            </View>
            <View style={styles.ContentContainer}>
              <Text style={styles.textHeader}>Kata Sandi Berhasil Diubah</Text>
              <Text style={styles.textContent}>
                Silahkan masuk kembali dengan menggunakan kata sandi baru Anda
              </Text>
            </View>
            <Text
              onPress={() => {
                this.props.navigation.navigate('Login');
              }}
              style={styles.textLink}>
              Kembali ke halaman login
            </Text>
          </SafeAreaView>
        </LinearGradient>
      );
    }
  }
}
