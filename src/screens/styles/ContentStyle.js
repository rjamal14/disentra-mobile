import {StyleSheet, Dimensions} from 'react-native';
import _ from '../../helpers/responsive';
import ft from '../../constants/fonts';
const {width: screenWidth} = Dimensions.get('window');

export default StyleSheet.create({
  gradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerBanner: {
    height: _.hp(20),
    width: _.wp(87),
    marginVertical: _.hp(1),
    alignSelf: 'center',
    borderRadius: 4,
  },
  konten: {
    alignSelf: 'center',
  },
  textContent: {
    color: '#323A3F',
    fontSize: _.fz(ft.med),
    fontFamily: ft.asi,
    textAlign: 'center',
    alignSelf: 'center',
    width: _.wp(80),
    marginTop: _.hp(6),
  },
  textContent2: {
    color: '#323A3F',
    fontSize: _.fz(ft.sml),
    fontFamily: ft.asi,
    textAlign: 'center',
    alignSelf: 'center',
    width: _.wp(80),
    marginTop: _.hp(1),
  },
  logoHedaer: {
    alignSelf: 'center',
  },
  header: {
    backgroundColor: '#FFFFFF',
    height: 72,
  },
  notification: {
    position: 'absolute',
    marginTop: _.hp(0.5),
    right: 5,
  },
  notificationIcon: {
    width: 24,
    height: 24,
  },
  pageIndicator: {
    marginBottom: _.hp(1),
  },
  pageIndicatorActive: {
    backgroundColor: 'white',
  },
  imageContainer: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 8,
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    resizeMode: 'contain',
  },
  Carousel: {
    marginTop: _.hp(2),
  },
  item: {
    width: screenWidth - 60,
    height: _.hp(29),
  },
  dotStyle: {
    width: 8,
    height: 8,
    borderRadius: 5,
    backgroundColor: '#FFFFFF',
  },
  containerStyle: {
    marginTop: -_.hp(7),
    marginBottom: -_.hp(1),
  },
  inactiveDotStyle: {
    backgroundColor: '#C4C4C4',
  },
});
