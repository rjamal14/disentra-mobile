import {StyleSheet} from 'react-native';
import _ from '../../helpers/responsive';
import {blue1, blue2, red, white, white1} from '../../constants/colors';
import ft from '../../constants/fonts';

export default StyleSheet.create({
  gradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    paddingVertical: 10,
  },
  container1: {
    flex: 1,
  },
  containerHeader: {
    alignSelf: 'center',
    alignItems: 'center',
  },
  map: {
    width: _.wp(80),
    height: _.wp(40),
    borderWidth: 5,
    borderColor: white1,
    paddingHorizontal: _.wp(2),
    borderRadius: 45,
    alignSelf: 'center',
    alignItems: 'center',
  },
  input: {
    width: _.wp(80),
    maxHeight: 40,
  },
  headerContainer: {
    width: _.wp(95),
    padding: 7,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  innerContainer: {
    paddingBottom: _.wp(15),
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  logoIcon: {
    height: _.hp(10),
    width: _.hp(10),
    resizeMode: 'center',
  },
  logoIcon1: {
    height: _.hp(5),
    width: _.hp(10),
    resizeMode: 'center',
  },
  dotActive: {
    height: _.wp(6),
    width: _.wp(6),
    borderRadius: _.wp(6) / 2,
    backgroundColor: blue2,
    borderWidth: 2,
    borderColor: blue2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textDotActive: {
    color: white1,
    fontSize: _.fz(ft.xsm),
    fontFamily: ft.mur,
  },
  dotInactive: {
    height: _.wp(6),
    width: _.wp(6),
    borderRadius: _.wp(6) / 2,
    backgroundColor: white1,
    borderWidth: 2,
    borderColor: blue2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textDotInactive: {
    color: blue2,
    fontSize: _.fz(ft.sml),
    fontFamily: ft.mur,
  },
  textLabelInactive: {
    color: blue2,
    fontSize: _.fz(ft.med),
    fontFamily: ft.mur,
  },
  textLabelActive: {
    color: blue2,
    fontSize: _.fz(ft.med),
    fontFamily: ft.musb,
  },
  dotContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: _.hp(1),
  },
  dotLabelContainer: {
    width: _.wp(80),
    paddingLeft: 8,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  handAbove: {
    position: 'absolute',
    height: _.hp(40),
    width: _.hp(50),
    top: _.hp(0),
    right: _.wp(0),
    opacity: 0.4,
  },
  textAlert: {
    marginVertical: _.hp(2),
    fontFamily: ft.mur,
    alignSelf: 'center',
    textAlign: 'center',
  },
  viewPager: {
    flex: 1,
  },
  listItem: {
    width: _.wp(100),
    borderBottomColor: '#d3d3d3',
    borderBottomWidth: 1,
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  listItemText: {
    fontFamily: ft.mur,
    fontSize: _.fz(8.5),
  },
  textHeader: {
    color: blue2,
    fontSize: _.fz(ft.lag),
    fontFamily: ft.musb,
    textAlign: 'center',
    alignSelf: 'center',
    marginTop: _.hp(10),
  },
  textContent: {
    color: blue2,
    fontSize: _.fz(ft.med),
    fontFamily: ft.asi,
    textAlign: 'center',
    alignSelf: 'center',
    width: _.wp(80),
    marginVertical: _.hp(1),
  },
  textContent1: {
    color: blue2,
    fontSize: _.fz(ft.sml),
    fontFamily: ft.mur,
    textAlign: 'center',
    alignSelf: 'center',
    width: _.wp(80),
    marginVertical: _.hp(1),
  },
  welcomeMessage: {
    alignItems: 'center',
  },
  errorMessage: {
    fontFamily: ft.musb,
    fontSize: _.fz(ft.sml),
    color: red,
    width: _.wp(80),
    textAlign: 'center',
    alignSelf: 'center',
  },
  errorMessage1: {
    fontFamily: ft.mur,
    fontSize: _.fz(ft.xsm),
    color: red,
    width: _.wp(80),
    position: 'absolute',
    alignSelf: 'center',
    top: 70,
  },
  errorMessage2: {
    fontFamily: ft.mur,
    fontSize: _.fz(ft.xsm),
    color: red,
    width: _.wp(80),
    position: 'absolute',
    alignSelf: 'center',
    top: 145,
  },
  errorMessage3: {
    fontFamily: ft.mur,
    fontSize: _.fz(ft.xsm),
    color: red,
    width: _.wp(80),
    textAlign: 'center',
    alignSelf: 'center',
  },
  errorMessage4: {
    fontFamily: ft.mur,
    fontSize: _.fz(ft.xsm),
    color: red,
    width: _.wp(80),
    position: 'absolute',
    alignSelf: 'center',
    top: 220,
  },
  errorMessage5: {
    fontFamily: ft.mur,
    fontSize: _.fz(ft.xsm),
    color: red,
    width: _.wp(80),
    position: 'absolute',
    alignSelf: 'center',
    top: 110,
  },
  underline: {
    borderBottomWidth: 2,
    borderColor: 'gray',
    width: _.wp(20),
  },
  underline1: {
    borderBottomWidth: 2,
    borderColor: blue2,
    width: _.wp(55),
    marginHorizontal: _.wp(1),
  },
  containerUnderline: {
    flexDirection: 'row',
    marginBottom: _.hp(2),
    alignItems: 'center',
  },
  text: {
    fontFamily: ft.mur,
    fontSize: _.fz(ft.sml),
    marginHorizontal: _.wp(2),
  },
  textLink: {
    fontFamily: ft.mur,
    fontSize: _.fz(ft.sml),
    color: blue2,
    marginTop: _.hp(3),
  },
  textLink2: {
    fontFamily: ft.mur,
    fontSize: _.fz(ft.sml),
    color: '#111111',
    marginStart: 10,
    marginBottom: 30,
  },
  portraitImage: {
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: _.hp(4),
    borderRadius: _.hp(15) / 2,
    borderWidth: 2,
    borderStyle: 'dashed',
    borderColor: blue2,
    marginBottom: 20,
  },
  images: {
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: _.hp(7),
    borderRadius: _.hp(15) / 2,
    borderWidth: 2,
    borderColor: blue2,
    marginBottom: 20,
  },
  textLabelInput: {
    color: blue2,
    fontSize: _.fz(ft.sml),
    fontFamily: ft.mur,
    marginStart: 10,
    zIndex: 1,
  },
  textItem: {
    color: white,
    fontSize: _.fz(ft.sml),
    fontFamily: ft.mur,
    marginStart: 10,
    paddingVertical: 10,
  },
  idImage: {
    width: '100%',
    height: '20%',
    padding: _.hp(9),
    borderRadius: _.hp(15) / 8,
    position: 'relative',
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginHorizontal: 10,
    marginBottom: 20,
  },
  idPlaceholder: {
    width: '100%',
    padding: _.hp(9),
    borderRadius: _.hp(15) / 8,
    borderWidth: 2,
    borderStyle: 'dashed',
    borderColor: blue2,
    position: 'relative',
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginHorizontal: 10,
    marginBottom: 20,
  },
  cameraIcon: {},
  bottomLayout: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
    display: 'flex',
    flexDirection: 'row',
    shadowColor: 'grey',
    backgroundColor: white,
    padding: 10,
    justifyContent: 'space-between',
  },
  leftBottomButton: {
    backgroundColor: white,
    alignContent: 'flex-start',
    alignItems: 'flex-start',
    alignSelf: 'flex-start',
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderRadius: 10,
    borderColor: blue2,
    borderWidth: 2,
  },
  rightBottomButton: {
    backgroundColor: blue2,
    alignContent: 'flex-end',
    alignItems: 'flex-end',
    alignSelf: 'flex-end',
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderRadius: 10,
  },
  textLeftButton: {
    color: blue2,
    fontFamily: ft.musb,
  },
  textRightButton: {
    color: white,
    fontFamily: ft.musb,
  },
  inputContainerStyle: {
    width: _.wp(80),
    backgroundColor: white,
    borderWidth: 0.5,
    borderColor: blue2,
    paddingHorizontal: _.wp(2),
    textAlignVertical: 'top',
    borderRadius: 5,
    marginStart: 8,
    marginBottom: 20,
    fontFamily: ft.mur,
    fontSize: _.fz(ft.sml),
    padding: 12,
  },
  autocomplete: {
    width: _.wp(80),
    backgroundColor: white,
    borderWidth: 0.5,
    borderColor: blue2,
    paddingHorizontal: _.wp(2),
    borderRadius: 5,
    marginStart: 8,
    marginBottom: 20,
    fontFamily: ft.mur,
    fontSize: _.fz(ft.sml),
    padding: 12,
  },
  searchContainer: {
    flexDirection: 'row',
  },
  closeButton: {
    paddingStart: 10,
    paddingVertical: 5,
  },
  flatList: {
    width: _.wp(80),
    marginStart: 10,
    backgroundColor: blue2,
    display: 'flex',
    position: 'absolute',
  },
});
