import {StyleSheet} from 'react-native';
import _ from '../../helpers/responsive';
import ft from '../../constants/fonts';

export default StyleSheet.create({
  avatar: {
    width: _.wp(25),
    height: _.wp(25),
    alignSelf: 'center',
    marginVertical: _.hp(2),
    borderRadius: _.wp(25),
  },
  close: {
    position: 'absolute',
    right: 5,
    top: 14,
    zIndex: 10,
  },
  name: {
    alignSelf: 'center',
    fontSize: _.fz(11),
    fontFamily: ft.mur,
  },
  verified: {
    fontSize: _.fz(8),
    fontFamily: ft.mur,
    marginRight: _.wp(2),
    color: '#323A3F',
  },
  verifiedIcon: {
    width: _.wp(3.5),
    height: _.wp(3.5),
  },
  verifiedArea: {
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  email: {
    alignSelf: 'center',
    fontSize: _.fz(8),
    fontFamily: ft.mur,
    marginBottom: _.hp(2),
    color: '#323A3F',
  },
  logoHeader: {
    height: _.wp(10),
    width: _.wp(20),
  },
  areaHeader: {
    zIndex: 9,
  },
  divider: {
    height: 1,
    width: '100%',
    backgroundColor: '#E5E5E5',
  },
  spacing: {
    marginVertical: _.hp(1),
  },
  listMenu: {
    marginLeft: _.wp(5),
    width: '100%',
    marginVertical: _.hp(2),
  },
  textMenu: {
    color: '#165581',
    fontSize: _.fz(10),
    fontFamily: ft.mur,
  },
  header: {
    backgroundColor: '#FFFFFF',
  },
  flex: {
    flex: 1,
  },
});
