import {StyleSheet} from 'react-native';
import _ from '../../helpers/responsive';
import ft from '../../constants/fonts';
import {white, blue2} from '../../constants/colors';

export default StyleSheet.create({
  gradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
  },
  header: {
    backgroundColor: white,
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
  },
  backButton: {
    width: _.wp(11),
    height: _.wp(11),
    justifyContent: 'center',
    alignItems: 'center',
  },
  textHeader: {
    color: '#323A3F',
    fontFamily: ft.mur,
  },
  innerContainer: {
    padding: _.wp(5),
    flex: 1,
    backgroundColor: white,
    alignItems: 'center',
    marginBottom: _.hp(6),
  },
  textLabel: {
    color: blue2,
    fontFamily: ft.mur,
    fontSize: _.fz(8),
    zIndex: 10,
    marginStart: _.wp(3),
  },
  textLabel2: {
    color: blue2,
    fontFamily: ft.mur,
    fontSize: _.fz(8),
    zIndex: 10,
    marginStart: _.wp(2),
  },
  avatar: {
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: _.wp(30) / 2,
    width: _.wp(30),
    height: _.wp(30),
  },
  infoContainer: {
    alignItems: 'center',
    padding: _.wp(5),
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
  },
  infoContainer1: {
    padding: _.wp(5),
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
  },
  infoContainer2: {
    padding: _.wp(5),
  },
  textLabel3: {
    color: blue2,
    fontFamily: ft.mur,
    fontSize: _.fz(8),
  },
  textDesc: {
    color: '#323A3F',
    fontFamily: ft.musb,
    fontSize: _.fz(9),
    marginLeft: _.wp(2),
    alignSelf: 'center',
  },
  underline: {
    width: _.wp(50),
    borderBottomColor: '#E5E5E5',
    borderWidth: 1,
  },
  btnPicker: {
    backgroundColor: white,
    width: _.wp(40),
    paddingVertical: _.hp(1),
    paddingHorizontal: _.wp(2),
  },
  errorMessage4: {
    fontFamily: ft.mur,
    fontSize: _.fz(ft.xsm),
    color: 'red',
    width: _.wp(80),
  },
  delete: {
    fontFamily: ft.mur,
    color: 'red',
    marginBottom: _.hp(1),
  },
  required: {
    color: 'red',
  },
  bottomAction: {
    width: _.wp(100),
    height: _.hp(10),
    flexDirection: 'row',
    backgroundColor: white,
    borderTopColor: '#323A3F',
    position: 'absolute',
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  btnConfirmSolid: {
    flexDirection: 'row',
    backgroundColor: blue2,
    width: _.wp(30),
    paddingVertical: _.hp(1.5),
    margin: _.hp(1),
    paddingHorizontal: _.wp(2),
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 5,
    borderColor: blue2,
    borderWidth: 2,
  },
  btnConfirmOutline: {
    flexDirection: 'row',
    backgroundColor: white,
    width: _.wp(30),
    paddingVertical: _.hp(1.5),
    margin: _.hp(1),
    paddingHorizontal: _.wp(2),
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 5,
    borderColor: blue2,
    borderWidth: 2,
  },
  textBtnSolid: {
    color: white,
    fontFamily: ft.mur,
    fontSize: _.fz(8),
  },
  textBtnOutlined: {
    color: blue2,
    fontFamily: ft.mur,
    fontSize: _.fz(8),
  },
  fotoKtp: {
    width: _.wp(80),
    height: _.hp(25),
  },
  text: {
    fontFamily: ft.mur,
    color: '#111111'
  },
  textAlert: {
    marginVertical: _.hp(2),
    fontFamily: ft.mur,
  },
  modalAlert: {
    padding: _.wp(10),
    alignItems: 'center',
  },
  maps: {
    width: _.wp(80),
    height: _.hp(30),
    minHeight: _.hp(30),
    alignSelf: 'center',
    marginBottom: _.hp(1),
  },
});
