import {StyleSheet} from 'react-native';
import _ from '../../helpers/responsive';
import {blue2, red, white1, yellow} from '../../constants/colors';
import ft from '../../constants/fonts';

export default StyleSheet.create({
  textBannerHeader: {
    fontFamily: ft.musb,
    fontSize: _.fz(ft.lag),
    color: white1,
  },
  textBannerContent: {
    fontFamily: ft.mur,
    fontSize: _.fz(ft.sml),
    color: white1,
  },
  contentContainer: {
    paddingVertical: _.hp(2),
    alignItems: 'center',
  },
  containerBannerText: {
    height: _.hp(6),
    width: _.wp(45),
    justifyContent: 'space-between',
    position: 'absolute',
    left: _.wp(3.5),
    top: _.wp(12),
  },
  banner: {
    height: _.hp(25),
    width: _.wp(100),
  },
  cards: {
    width: _.wp(90),
    height: _.hp(20),
    borderRadius: _.wp(3),
    backgroundColor: white1,
    padding: _.wp(5),
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    marginBottom: _.hp(2),
  },
  cardsContent: {
    width: _.wp(60),
    marginLeft: _.wp(5),
  },
  cardTitle: {
    fontFamily: ft.musb,
    fontSize: _.fz(ft.lag) - 2,
    color: blue2,
  },
  cardContent: {
    fontFamily: ft.mur,
    marginVertical: _.hp(0.5),
    fontSize: _.fz(ft.sml),
  },
  cardContentMail: {
    marginVertical: _.hp(0.5),
    fontFamily: ft.mur,
    textDecorationLine: 'underline',
    fontSize: _.fz(ft.sml),
    color: '#29AAE2',
  },
  callCenterButton: {
    backgroundColor: yellow,
    padding: _.wp(1),
    borderRadius: _.wp(2),
    width: _.wp(30),
    alignItems: 'center',
  },
  callCenterButtonText: {
    color: blue2,
    fontFamily: ft.mur,
    fontSize: _.fz(ft.sml),
  },
});
