import {StyleSheet} from 'react-native';
import _ from '../../helpers/responsive';
import ft from '../../constants/fonts';
import {blue1, blue2, white} from '../../constants/colors';

export default StyleSheet.create({
  gradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
  },
  header: {
    backgroundColor: white,
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
  },
  backButton: {
    width: _.wp(11),
    height: _.wp(11),
    justifyContent: 'center',
    alignItems: 'center',
  },
  textHeader: {
    color: '#323A3F',
    fontFamily: ft.mur,
  },
  innerContainer: {
    padding: _.wp(5),
    flex: 1,
    backgroundColor: white,
  },
  textLabel: {
    marginLeft: _.wp(5),
    marginVertical: _.hp(1),
    fontFamily: ft.mur,
  },
  textAlert: {
    marginVertical: _.hp(2),
    fontFamily: ft.mur,
  },
  modalAlert: {
    padding: _.wp(10),
    alignItems: 'center',
  },
  maps: {
    width: _.wp(90),
    height: _.hp(30),
    minHeight: _.hp(30),
    alignSelf: 'center',
  },
  cardContentEmpty: {
    flex: 1,
    backgroundColor: white,
    justifyContent: 'center',
  },
  textContent: {
    alignSelf: 'center',
  },
  notifItem: {
    borderBottomColor: '#323A3F',
    borderBottomWidth: 0.5,
    padding: _.wp(2),
    flexDirection: 'row',
    alignItems: 'center',
  },
  notifImage: {
    backgroundColor: blue1,
    alignSelf: 'center',
    padding: 10,
    margin: 10,
    borderRadius: 10,
  },
  notifTitle: {
    fontFamily: ft.musb,
    fontSize: _.fz(9),
    color: blue2,
  },
  notifTitle1: {
    fontFamily: ft.musb,
    fontSize: _.fz(12),
    color: blue2,
  },
  notifTitle2: {
    fontFamily: ft.musb,
    fontSize: _.fz(12),
    color: blue2,
    alignSelf: 'center',
    marginLeft: 20,
  },
  notifSubtitle: {
    fontFamily: ft.mur,
    fontSize: _.fz(8),
  },
  notifSubtitle1: {
    fontFamily: ft.mur,
    fontSize: _.fz(8),
    marginTop: _.hp(1),
  },
  notifTime: {
    fontFamily: ft.mur,
    fontSize: _.fz(6),
  },
  notifTime1: {
    fontFamily: ft.mur,
    fontSize: _.fz(7),
    color: '#323A3F',
  },
  contentContainer: {
    width: _.wp(70),
  },
  modalContentContainer: {
    flex: 1,
    backgroundColor: white,
  },
  modalContent: {
    padding: _.wp(5),
  },
  eventImage: {
    height: _.wp(30),
    width: _.wp(35),
    borderTopLeftRadius: 100,
  },
  cards: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 10,
    overflow: 'hidden',
  },
  link: {
    fontSize: _.fz(9),
    fontFamily: ft.mur,
    color: blue1,
  },
  badges: {
    backgroundColor: 'red',
    padding: 4,
    borderRadius: 10,
    position: 'absolute',
    top: 2,
    right: 2,
  },
});
