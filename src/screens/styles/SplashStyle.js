import {StyleSheet} from 'react-native';
import _ from '../../helpers/responsive';

export default StyleSheet.create({
  gradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },
  logo: {
    height: _.hp(25),
    width: _.hp(25),
    resizeMode: 'center',
    alignSelf: 'center',
    marginTop: _.hp(35),
  },
  handAbove: {
    position: 'absolute',
    height: _.hp(40),
    width: _.hp(50),
    top: _.hp(0),
    right: _.wp(0),
    opacity: 0.1,
  },
  handBelow: {
    position: 'absolute',
    height: _.hp(40),
    width: _.hp(50),
    bottom: _.hp(0),
    left: _.wp(0),
    opacity: 0.1,
  },
});
