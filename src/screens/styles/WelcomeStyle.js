import {StyleSheet} from 'react-native';
import _ from '../../helpers/responsive';
import {blue2} from '../../constants/colors';
import ft from '../../constants/fonts';

export default StyleSheet.create({
  gradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },
  logo: {
    height: _.hp(15),
    width: _.hp(25),
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: _.hp(8),
  },
  logoIcon: {
    height: _.hp(5),
    width: _.hp(8),
    position: 'absolute',
    right: _.wp(3),
    top: _.wp(3),
  },
  handAbove: {
    position: 'absolute',
    height: _.hp(40),
    width: _.hp(50),
    top: _.hp(0),
    right: _.wp(0),
    zIndex: -1,
    opacity: 0.4,
  },
  textHeader: {
    color: blue2,
    fontSize: _.fz(ft.lag),
    fontFamily: ft.musb,
    textAlign: 'center',
    alignSelf: 'center',
    marginTop: _.hp(10),
  },
  textContent: {
    color: blue2,
    fontSize: _.fz(ft.med),
    fontFamily: ft.asi,
    textAlign: 'center',
    alignSelf: 'center',
    width: _.wp(80),
    marginVertical: _.hp(6),
  },
  welcomeMessage: {
    alignItems: 'center',
  },
});
