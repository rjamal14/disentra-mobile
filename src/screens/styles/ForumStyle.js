import {StyleSheet} from 'react-native';
import _ from '../../helpers/responsive';
import ft from '../../constants/fonts';

export default StyleSheet.create({
  gradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: _.hp(100),
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  konten: {
    alignSelf: 'center',
  },
  textContent: {
    color: '#323A3F',
    fontSize: _.fz(ft.med),
    fontFamily: ft.asi,
    textAlign: 'center',
    alignSelf: 'center',
    width: _.wp(80),
    marginTop: _.hp(6),
  },
  textContent2: {
    color: '#323A3F',
    fontSize: _.fz(ft.sml),
    fontFamily: ft.asi,
    textAlign: 'center',
    alignSelf: 'center',
    width: _.wp(80),
    marginTop: _.hp(1),
  },
  logoHedaer: {
    alignSelf: 'center',
  },
  header: {
    backgroundColor: '#FFFFFF',
    height: 72,
  },
  notification: {
    position: 'absolute',
    marginTop: _.hp(0.5),
    right: 5,
  },
  notificationIcon:{
    width: 24,
    height: 24,
  },
});
