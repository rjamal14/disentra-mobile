import {StyleSheet} from 'react-native';
import _ from '../../helpers/responsive';
import ft from '../../constants/fonts';
import {white} from '../../constants/colors';

export default StyleSheet.create({
  gradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
  },
  header: {
    backgroundColor: white,
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
    zIndex: 2,
  },
  backButton: {
    width: _.wp(11),
    height: _.wp(11),
    justifyContent: 'center',
    alignItems: 'center',
  },
  textHeader: {
    color: '#323A3F',
    fontFamily: ft.mur,
  },
  innerContainer: {
    flex: 1,
    backgroundColor: 'white',
    padding: 24,
  },
  insideContainer: {
    alignItems: 'center',
    zIndex: 1,
  },
  textLabel: {
    fontFamily: ft.mur,
    width: _.wp(80),
    marginBottom: _.hp(1),
  },
  textAlert: {
    fontFamily: ft.mur,
  },
  modalAlert: {
    padding: _.wp(10),
    alignItems: 'center',
  },
  maps: {
    width: _.wp(90),
    height: _.hp(30),
    minHeight: _.hp(30),
    alignSelf: 'center',
  },
});
