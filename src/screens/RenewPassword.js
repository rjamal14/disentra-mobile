import React, {Component} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {SafeAreaView} from 'react-native-safe-area-context';
import {TouchableOpacity, Image, View, Text, ScrollView} from 'react-native';
import {white, white1, blue2} from '../constants/colors';
import InputText from '../components/InputText';
import {Header, Left, Right, Body, Title} from 'native-base';
import {Overlay} from 'react-native-elements';
import styles from '../screens/styles/RenewPasswordStyle';
import AsyncStorage from '@react-native-async-storage/async-storage';
import IconLock from '../assets/icons/ic_lock.svg';
import ImageSuccess from '../assets/images/success-check.svg';
import Cf from '../config';
import axios from 'axios';
import {ButtonL} from '../components/Buttons';

export default class RenewPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalAlert: false,
      current_password: '',
      showMessageCurrent: false,
      currentMessage: '',
      password: '',
      showMessagePassword: false,
      passwordMessage: '',
      passwordConf: '',
      showMessagePasswordConf: false,
      passwordConfMessage: '',
      isLoading: false,
    };
  }

  _handleChangePassword() {
    this.setState({
      isLoading: true,
    });
    AsyncStorage.getItem('access_token').then((token) => {
      axios
        .patch(
          Cf.base_url + Cf.user_profile,
          {
            user: {
              current_password: this.state.current_password,
              password: this.state.password,
              password_confirmation: this.state.passwordConf,
            },
          },
          {
            headers: {
              Authorization: 'Bearer ' + token,
            },
          },
        )
        .then((response) => {
          if (response.data.code === 200) {
            this.setState({
              modalAlert: true,
              isLoading: false,
            });
          }
        })
        .catch((error) => {
          this.setState({
            showMessageCurrent: true,
            currentMessage: 'Password Salah',
          });
        });
    });
  }

  validate() {
    if (this.state.password === '') {
      this.setState({
        passwordMessage: 'Kata sandi baru tidak boleh kosong.',
        showMessagePassword: true,
      });
    } else {
      this.setState({
        passwordMessage: '',
        showMessagePassword: false,
      });
    }

    if (this.state.passwordConf === '') {
      this.setState({
        passwordConfMessage: 'Konfirmasi kata sandi baru tidak boleh kosong.',
        showMessagePasswordConf: true,
      });
    } else if (this.state.passwordConf !== this.state.password) {
      this.setState({
        passwordConfMessage:
          'Kata sandi baru dan kondirmasi kata sandi tidak cocok.',
        showMessagePasswordConf: true,
      });
    } else {
      this.setState({
        passwordConfMessage: '',
        showMessagePasswordConf: false,
      });
    }

    if (this.state.current_password === '') {
      this.setState({
        currentMessage: 'Kata sandi lama tidak boleh kosong.',
        showMessageCurrent: true,
      });
    } else {
      this.setState({
        currentMessage: '',
        showMessageCurrent: true,
      });
    }

    if (
      this.state.current_password === '' ||
      this.state.password === '' ||
      this.state.passwordConf === '' ||
      this.state.passwordConf !== this.state.password
    ) {
    } else {
      this._handleChangePassword();
    }
  }

  _updateMasterState = (attrName, value) => {
    if (
      this.state.currentMessage !== '' ||
      this.state.passwordMessage !== '' ||
      this.state.passwordConfMessage !== ''
    ) {
      this.setState({
        currentMessage: '',
        passwordMessage: '',
        passwordConfMessage: '',
      });
    }
    this.setState({[attrName]: value});
  };

  render() {
    return (
      <LinearGradient colors={[white, white1]} style={styles.gradient}>
        <SafeAreaView style={styles.container}>
          <Header style={styles.header} androidStatusBarColor={blue2}>
            <Left>
              <TouchableOpacity
                style={styles.backButton}
                onPress={() => {
                  this.props.navigation.goBack();
                }}>
                <Image source={require('../assets/icons/arrow-left.png')} />
              </TouchableOpacity>
            </Left>
            <Body>
              <Title style={styles.textHeader}>Ubah Kata Sandi</Title>
            </Body>
            <Right />
          </Header>
          <ScrollView style={styles.innerContainer}>
            <View style={styles.insideContainer}>
              <Text style={styles.textLabel}>
                Mohon masukkan kata sandi lama Anda.
              </Text>
              <InputText
                type={'password'}
                placeholder={'Kata Sandi Lama'}
                showMessage={this.state.showMessageCurrent}
                value={this.state.current_password}
                attrName={'current_password'}
                updateMasterState={this._updateMasterState}
                message={{
                  type: this.state.currentMessage !== '' ? 'error' : '',
                  text: this.state.currentMessage,
                }}
                icons={<IconLock />}
              />
              <Text style={styles.textLabel}>
                Buat kata sandi baru minimal 8 karakter yang terdiri dari
                kombinasi huruf, angka, dan simbol.
              </Text>
              <InputText
                type={'password'}
                placeholder={'Kata Sandi Baru'}
                showMessage={this.state.showMessagePassword}
                value={this.state.password}
                attrName={'password'}
                updateMasterState={this._updateMasterState}
                message={{
                  type: this.state.passwordMessage !== '' ? 'error' : '',
                  text: this.state.passwordMessage,
                }}
                icons={<IconLock />}
              />
              <InputText
                type={'password'}
                placeholder={'Konfirmasi Kata Sandi'}
                showMessage={this.state.showMessagePasswordConf}
                value={this.state.passwordConf}
                attrName={'passwordConf'}
                updateMasterState={this._updateMasterState}
                message={{
                  type: this.state.passwordConfMessage !== '' ? 'error' : '',
                  text: this.state.passwordConfMessage,
                }}
                icons={<IconLock />}
              />
              <ButtonL
                title="KIRIM"
                loading={this.state.isLoading}
                onPress={() => {
                  this.validate();
                }}
              />
            </View>
          </ScrollView>

          <Overlay
            isVisible={this.state.modalAlert}
            onShow={() => {
              setTimeout(() => {
                this.setState({modalAlert: false});
                this.props.navigation.goBack();
              }, 2500);
            }}>
            <View style={styles.modalAlert}>
              <ImageSuccess />
              <Text style={styles.textAlert}>Kata sandi berhasil diubah.</Text>
            </View>
          </Overlay>
        </SafeAreaView>
      </LinearGradient>
    );
  }
}
