import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Linking,
  Share,
  ActivityIndicator,
} from 'react-native';
import {Header, Title, Left, Right, Body} from 'native-base';
import {WebView} from 'react-native-webview';
import PDFView from 'react-native-view-pdf';
import styles from '../../screens/styles/LearningCenterStyle';
import {blue2} from '../../constants/colors';
import IconShare from '../../assets/icons/ic_share2.svg';
import IconDownload from '../../assets/icons/ic_download.svg';

export default class WebViewScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      params: this.props.route.params,
    };
  }

  async onShare(link) {
    try {
      await Share.share({
        message: link,
      });
    } catch (error) {}
  }

  render() {
    return (
      <View style={styles.container}>
        <Header style={styles.header} androidStatusBarColor={blue2}>
          <Left>
            <TouchableOpacity
              style={styles.backButton}
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <Image source={require('../../assets/icons/arrow-left.png')} />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title style={styles.textHeader}>
              {this.state.params.title || this.state.params.header}
            </Title>
          </Body>
          <Right>
            {this.state.params.header === 'Berita' ? (
              <TouchableOpacity
                style={styles.headerButton}
                onPress={() => {
                  this.onShare(this.state.params.link);
                }}>
                <IconShare />
                <Text style={styles.headerButtonText}>Bagikan</Text>
              </TouchableOpacity>
            ) : this.state.params.header === 'Video' ? (
              <TouchableOpacity
                style={styles.headerButton}
                onPress={() => {
                  this.onShare(this.state.params.link);
                }}>
                <IconShare />
                <Text style={styles.headerButtonText}>Bagikan</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={styles.headerButton}
                onPress={() => {
                  Linking.openURL(this.state.params.link);
                }}>
                <IconDownload />
                <Text style={styles.headerButtonText}>Unduh PDF</Text>
              </TouchableOpacity>
            )}
          </Right>
        </Header>
        {this.state.params.header === 'Berita' ||
        this.state.params.header === 'Video' ? (
          <WebView
            style={styles.webviewContainer}
            source={{uri: this.state.params.link}}
            startInLoadingState={true}
            renderLoading={() => {
              return (
                <ActivityIndicator
                  color="#009688"
                  size="large"
                  style={styles.activityIndicatorStyle}
                />
              );
            }}
          />
        ) : (
          <PDFView
            fadeInDuration={250.0}
            style={styles.container}
            resource={this.state.params.link}
            resourceType={'url'}
            onError={(error) => {}}
          />
        )}
      </View>
    );
  }
}
