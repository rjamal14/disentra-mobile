import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Share,
  ScrollView,
} from 'react-native';
import {Header, Title, Left, Right, Body} from 'native-base';
import styles from '../../screens/styles/LearningCenterStyle';
import {blue2} from '../../constants/colors';
import IconShare from '../../assets/icons/ic_share2.svg';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Cf from '../../config/index';
import actions from '../../actions';

export default class WebViewScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      params: [],
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('access_token').then((result) => {
      const uri = 'api/v1/mobile/news/' + this.props.route.params.item;
      fetch(Cf.base_url + uri, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + result,
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            params: responseJson.data,
          });
        })
        .catch((error) => {
          if (error.response.status === 401) {
            actions.removeToken();
            this.props.navigation.replace('Auth');
          }
        });
    });
  }

  async onShare(link) {
    try {
      await Share.share({
        message: link,
      });
    } catch (error) {}
  }

  render() {
    const item = this.state.params.item;
    return (
      <View style={styles.container}>
        <Header style={styles.header} androidStatusBarColor={blue2}>
          <Left>
            <TouchableOpacity
              style={styles.backButton}
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <Image source={require('../../assets/icons/arrow-left.png')} />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title style={styles.textHeader}>Berita</Title>
          </Body>
          <Right>
            <TouchableOpacity
              style={styles.headerButton}
              onPress={() => {
                this.onShare(item.link || '');
              }}>
              <IconShare />
              <Text style={styles.headerButtonText}>Bagikan</Text>
            </TouchableOpacity>
          </Right>
        </Header>
        <ScrollView>
          {/* <Text style={styles.titleCard}>{item.title || ''}</Text> */}
          <Text style={styles.textDate}>8 Februari 2021</Text>
          <Image
            style={styles.imgCover2}
            source={{
              uri:
                'https://assets.pikiran-rakyat.com/crop/0x0:0x0/x/photo/2021/02/08/3494583764.png',
            }}
          />
          <Text style={styles.descDetail}>
            PIKIRAN RAKYAT - Membayar pajak kendaraan bermotor (PKB) merupakan
            sesuatu yang mudah. Namun di balik kemudahan itu, selalu ada problem
            klasik yang mengurangi daya tarik masyarakat sebagai wajib pajak
            dalam memenuhi kewajibannya, termasuk akses pembayaran pajak yang
            harus dilakukan di Kantor Samsat, serta antrean dalam proses
            pendaftaran hingga pencetakan STNK baru.
          </Text>
        </ScrollView>
      </View>
    );
  }
}
