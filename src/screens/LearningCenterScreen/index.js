/* eslint-disable radix */
import React, {Component} from 'react';
import {
  Image,
  Text,
  View,
  FlatList,
  RefreshControl,
  TouchableOpacity,
  Share,
} from 'react-native';
import {Button} from 'react-native-elements';
import styles from '../../screens/styles/LearningCenterStyle';
import {Container, Card, Tab, Tabs} from 'native-base';
import Cf from '../../config/index';
import FilterIcon from '../../assets/icons/ic_filter';
import SearchIcon from '../../assets/icons/ic_search';
import UserIcon from '../../assets/icons/ic_user';
import ArrowIcon from '../../assets/icons/ic_arrow';
import ShareIcon from '../../assets/icons/ic_share';
import AsyncStorage from '@react-native-async-storage/async-storage';
import InputText from '../../components/InputText';
import FilterCoimponent from './filter';
import actions from '../../actions';
export default class LearningCenter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEmailSend: false,
      queryVideo: '',
      queryEbook: '',
      queryNews: '',
      contentVideo: [],
      contentEbook: [],
      contentArticle: [],
      refreshVideo: false,
      modalVisible: false,
      time: '',
      banner: '',
      paginationVidio: '',
      paginationEbook: '',
      paginationArticle: '',
    };
  }

  componentDidMount() {
    this.fetchBanner('learning');
    this.fetchContent('video');
    this.fetchContent('ebook');
    this.fetchContent('news');
  }

  fetchBanner(type) {
    fetch(Cf.base_url + Cf.get_banner + type, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({banner: responseJson.data[0].image.url});
      });
  }

  async onShare(link) {
    try {
      await Share.share({
        message: link,
      });
    } catch (error) {}
  }

  fetchContent(contentType = '', query = '', time = '', next = false) {
    AsyncStorage.getItem('access_token').then((result) => {
      var d = new Date();
      var date =
        d.getDate() +
        '-' +
        (parseInt(d.getMonth() + 1) < 10
          ? '0' + parseInt(d.getMonth() + 1)
          : parseInt(d.getMonth() + 1)) +
        '-' +
        d.getFullYear();
      const uri = 'api/v1/mobile/learning_centers';
      const type = '?q[content_type_eq]=' + contentType;
      const filter_time = '&filter_time=' + time;
      const search = '&search=' + (query === '' ? '' : query);
      const rangeDate =
        '&q[created_at_gteq]=' +
        date +
        ' 00:00&q[created_at_lteq]=' +
        date +
        ' 23:59';
      let path = uri;
      if (contentType === 'video') {
        if (time === 'today') {
          path += type + search + rangeDate;
        } else {
          path += type + search + filter_time;
        }
        path += '&page=' + (next ? this.state.paginationVidio.next_page : 1);
      } else if (contentType === 'ebook') {
        path +=
          type +
          search +
          '&page=' +
          (next ? this.state.paginationEbook.next_page : 1);
      } else if (contentType === 'news') {
        path +=
          type +
          search +
          '&page=' +
          (next ? this.state.paginationArticle.next_page : 1);
      }
      fetch(Cf.base_url + path, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + result,
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          if (contentType === 'video') {
            if (next) {
              var data = this.state.contentVideo;
              var dataCon = data.concat(responseJson.data);
              this.setState({
                contentVideo: dataCon,
              });
            } else {
              this.setState({
                contentVideo: responseJson.data || [],
              });
            }
            this.setState({
              paginationVidio: responseJson.pagination,
            });
          } else if (contentType === 'ebook') {
            this.setState({
              paginationEbook: responseJson.pagination,
            });
            if (next) {
              var data = this.state.contentEbook;
              var dataCon = data.concat(responseJson.data);
              this.setState({
                contentEbook: dataCon,
              });
            } else {
              this.setState({
                contentEbook: responseJson.data || [],
              });
            }
          } else if (contentType === 'news') {
            this.setState({
              paginationArticle: responseJson.pagination,
            });
            if (next) {
              var data = this.state.contentArticle;
              var dataCon = data.concat(responseJson.data);
              this.setState({
                contentArticle: dataCon,
              });
            } else {
              this.setState({
                contentArticle: responseJson.data || [],
              });
            }
          }
        })
        .catch((error) => {
          if (error.response.status === 401) {
            actions.removeToken();
            this.props.navigation.replace('Auth');
          }
        });
    });
  }

  _updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  _onRefresh = () => {
    this.fetchContent('video', this.state.queryVideo, this.state.time);
    this.fetchContent('ebook', this.state.queryEbook);
    this.fetchContent('news', this.state.queryNews);
  };

  renderContentVideo() {
    if (this.state.contentVideo.length > 0) {
      return (
        <FlatList
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshVideo}
              onRefresh={this._onRefresh2}
            />
          }
          onEndReached={() => {
            if (
              this.state.paginationVidio !== null &&
              this.state.paginationVidio.next_page !== null
            ) {
              this.fetchContent(
                'video',
                this.state.queryVideo,
                this.state.time,
                true,
              );
            }
          }}
          onEndReachedThreshold={7}
          data={this.state.contentVideo}
          renderItem={({item}) => (
            <Card style={styles.cards}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('WebView', {
                    link: item.link,
                    header: 'Video',
                  });
                }}>
                <Image
                  source={
                    item.image.url
                      ? {uri: item.image.url}
                      : require('../../assets/images/logo-color.png')
                  }
                  style={styles.imageThumbnail}
                />
                <View style={styles.cardContentContainer}>
                  <Text style={styles.titleText}>{item.title}</Text>
                  <Text style={styles.authorText}>{item.author_name}</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.onShare(item.link);
                }}
                style={styles.shareIcon}>
                <ShareIcon />
              </TouchableOpacity>
            </Card>
          )}
        />
      );
    } else {
      return (
        <View style={styles.cardContentEmpty}>
          <Text>Data tidak ditemukan</Text>
        </View>
      );
    }
  }

  renderContentEbook() {
    if (this.state.contentEbook.length > 0) {
      return (
        <FlatList
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshVideo}
              onRefresh={this._onRefresh2}
            />
          }
          onEndReached={() => {
            if (
              this.state.paginationEbook !== null &&
              this.state.paginationEbook.next_page !== null
            ) {
              this.fetchContent(
                'ebook',
                this.state.queryEbook,
                this.state.time,
                true,
              );
            }
          }}
          onEndReachedThreshold={7}
          data={this.state.contentEbook}
          renderItem={({item}) => (
            <Card style={styles.cards}>
              <View style={styles.cardEbookContainer}>
                <Image
                  source={
                    item.image.url
                      ? {uri: item.image.url}
                      : require('../../assets/images/logo-color.png')
                  }
                  style={styles.imageThumbnailEbook}
                />
                <View style={styles.cardContentContainer}>
                  <Text style={styles.titleText}>{item.title}</Text>
                  <View style={styles.authorContainer}>
                    <UserIcon />
                    <Text style={styles.authorText}>{item.author_name}</Text>
                  </View>
                </View>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('WebView', {
                    link: item.link,
                    header: 'E-Book',
                  });
                }}
                style={styles.buttonContainer}>
                <Text style={styles.textButton}>Lihat</Text>
                <ArrowIcon />
              </TouchableOpacity>
            </Card>
          )}
        />
      );
    } else {
      return (
        <View style={styles.cardContentEmpty}>
          <Text>Data tidak ditemukan</Text>
        </View>
      );
    }
  }

  renderContentNews() {
    if (this.state.contentArticle.length > 0) {
      return (
        <FlatList
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshVideo}
              onRefresh={this._onRefresh2}
            />
          }
          onEndReached={() => {
            if (
              this.state.paginationArticle !== null &&
              this.state.paginationArticle.next_page !== null
            ) {
              this.fetchContent(
                'news',
                this.state.queryNews,
                this.state.time,
                true,
              );
            }
          }}
          onEndReachedThreshold={7}
          data={this.state.contentArticle}
          renderItem={({item}) => (
            <Card style={styles.cards}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('WebView', {
                    link: item.link,
                    header: 'Berita',
                  });
                }}>
                <Image
                  source={
                    item.image.url
                      ? {uri: item.image.url}
                      : require('../../assets/images/logo-color.png')
                  }
                  style={styles.imageThumbnail}
                />
                <View style={styles.cardContentContainer}>
                  <Text style={styles.titleText}>{item.title}</Text>
                  <Text style={styles.authorText}>{item.author_name}</Text>
                </View>
              </TouchableOpacity>
            </Card>
          )}
        />
      );
    } else {
      return (
        <View style={styles.cardContentEmpty}>
          <Text>Data tidak ditemukan</Text>
        </View>
      );
    }
  }

  render() {
    return (
      <Container>
        <FilterCoimponent
          modalVisible={this.state.modalVisible}
          time_props={this.state.time}
          valueFilter={(time) => {
            this.setState({time: time}, () => {
              this.fetchContent(
                'video',
                this.state.queryVideo,
                this.state.time,
              );
            });
          }}
          setModalVisible={(val) => {
            this.setState({modalVisible: val});
          }}
        />
        <Image style={styles.logo} source={{uri: this.state.banner}} />
        <Tabs tabContainerStyle={styles.tabContainer}>
          <Tab
            tabStyle={styles.tabVideo}
            activeTabStyle={styles.activeTabVideo}
            textStyle={styles.tabTextStyle}
            activeTextStyle={styles.activeTabTextStyle}
            style={styles.tabContentContainer}
            heading="VIDEO">
            <View style={styles.searchContainer}>
              <View>
                <Button
                  icon={<FilterIcon />}
                  title={'Saring'}
                  type={'outlined'}
                  buttonStyle={styles.filterButton}
                  titleStyle={styles.cardContent}
                  onPress={() => {
                    this.setState({
                      modalVisible: true,
                    });
                  }}
                />
              </View>
              <View>
                <InputText
                  type={'search'}
                  placeholder={'Cari Video'}
                  showMessage={''}
                  attrName={'queryVideo'}
                  value={this.state.query}
                  updateMasterState={this._updateMasterState}
                  onEndEditing={() =>
                    this.fetchContent('video', this.state.queryVideo)
                  }
                  message={''}
                  icons={<SearchIcon />}
                />
              </View>
            </View>
            {this.renderContentVideo()}
          </Tab>
          <Tab
            tabStyle={styles.tabEbook}
            activeTabStyle={styles.activeTabEbook}
            textStyle={styles.tabTextStyle}
            activeTextStyle={styles.activeTabTextStyle}
            style={styles.tabContentContainer}
            heading="E-BOOK">
            <View style={styles.searchContainer}>
              <InputText
                type={'searchlong'}
                placeholder={'Cari E-Book'}
                showMessage={''}
                attrName={'queryEbook'}
                value={this.state.queryEbook}
                updateMasterState={this._updateMasterState}
                onEndEditing={() => this.fetchContent(this.state.queryEbook)}
                message={''}
                icons={<SearchIcon />}
              />
            </View>
            {this.renderContentEbook()}
          </Tab>
          <Tab
            tabStyle={styles.tabBerita}
            activeTabStyle={styles.activeTabBerita}
            textStyle={styles.tabTextStyle}
            activeTextStyle={styles.activeTabTextStyle}
            style={styles.tabContentContainer}
            heading="BERITA">
            <View style={styles.searchContainer}>
              <InputText
                type={'searchlong'}
                placeholder={'Cari Berita'}
                showMessage={''}
                attrName={'queryNews'}
                value={this.state.query}
                updateMasterState={this._updateMasterState}
                onEndEditing={() =>
                  this.fetchContent('news', this.state.queryNews)
                }
                message={''}
                icons={<SearchIcon />}
              />
            </View>
            {this.renderContentNews()}
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
