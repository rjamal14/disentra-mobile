import React, {Component} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {SafeAreaView} from 'react-native-safe-area-context';
import {
  TouchableOpacity,
  Image,
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  PermissionsAndroid,
} from 'react-native';
import {white, white1, blue2} from '../constants/colors';
import InputText from '../components/InputText';
import {Header, Left, Right, Body, Title, Button} from 'native-base';
import {Overlay} from 'react-native-elements';
import styles from '../screens/styles/RenewProfileStyle';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImageSuccess from '../assets/images/success-check.svg';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import MapView, {Marker} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';

import axios from 'axios';
import Cf from '../config';
import actions from '../actions';

const uri = 'api/v1/user';
const pickerOption = {
  mediaType: 'photo',
  quality: 0.1,
  includeBase64: false,
};

export default class RenewPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: '',
      userProfile: '',
      name: '',
      messageName: '',
      isErrorName: false,
      showMessageName: false,
      phone: '',
      messagePhone: '',
      isErrorPhone: false,
      showMessagePhone: false,
      bankAccount: '',
      messageBankAccount: '',
      isErrorBankAccount: false,
      showMessageBankAccount: false,
      email: '',
      messageEmail: '',
      isErrorEmail: false,
      showMessageEmail: false,
      businessName: '',
      messageBusinessName: '',
      isErrorBusinessName: false,
      showMessageBusinessName: false,
      businessDesc: '',
      messageBusinessDesc: '',
      isErrorBusinessDesc: false,
      showMessageBusinessDesc: false,
      businessAddress: '',
      messageBusinessAddress: '',
      isErrorBusinessAddress: false,
      showMessageBusinessAddress: false,
      latitude: 0,
      longitude: 0,
      refreshData: false,
      modalConfirm: false,
      modalPicker: false,
      modalAlert: false,
      userPhoto: null,
      isLoading: false,
      identityImage: '',
      messageIdentityImage: '',
      type: props.route.params,
      isMapReady: false,
      id: '',
    };
    this.ScrollView = React.createRef();
    this.ScrollView1 = React.createRef();
  }
  componentDidMount() {
    this.fetchUserData();
  }

  _updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  _onRefresh = () => {
    this.fetchUserData();
  };

  onMapLayout = () => {
    this.setState({isMapReady: true});
  };

  onRegionChange(region) {
    this.setState({
      latitude: region.latitude,
      longitude: region.longitude,
    });
  }

  async _requestLocation() {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Izin Lokasi',
        message:
          'BJB Disentra perlu izin untuk mengakses layanan lokasi perangkat',
        buttonNegative: 'Tolak',
        buttonPositive: 'Izinkan',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
        interval: 10000,
        fastInterval: 5000,
      })
        .then((data) => {
          Geolocation.getCurrentPosition(
            (position) => {
              this.setState({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
              });
            },
            (error) => {},
            {enableHighAccuracy: false, timeout: 15000, maximumAge: 0},
          );
        })
        .catch((err) => {});
    } else {
    }
  }

  renderMenu() {
    if (this.state.type.type === 'profile') {
      return (
        <ScrollView ref={this.ScrollView}>
          <View style={styles.innerContainer}>
            <View style={styles.infoContainer}>
              <Text style={styles.textLabel2}>Foto Diri</Text>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    modalPicker: true,
                  });
                }}>
                {this.state.userPhoto === '' ? (
                  <Image
                    source={require('../assets/images/avatar.png')}
                    style={styles.avatar}
                  />
                ) : (
                  <Image
                    source={{uri: this.state.userPhoto}}
                    style={styles.avatar}
                  />
                )}
              </TouchableOpacity>
            </View>
            <View style={styles.infoContainer}>
              <View style={styles.separator}>
                <Text style={styles.textLabel}>
                  Nama Lengkap<Text style={styles.required}>*</Text>
                </Text>
                <InputText
                  type={'text'}
                  placeholder={'Nama Lengkap'}
                  showMessage={this.state.messageName}
                  value={this.state.name}
                  attrName={'name'}
                  updateMasterState={this._updateMasterState}
                  message={{
                    type: this.state.isErrorName ? 'error' : '',
                    text: this.state.messageName,
                  }}
                />
              </View>
              <View style={styles.separator}>
                <Text style={styles.textLabel}>
                  Nomor Handphone<Text style={styles.required}>*</Text>
                </Text>
                <InputText
                  type={'number'}
                  placeholder={'Nomor Handphone'}
                  showMessage={this.state.showMessagePhone}
                  value={this.state.phone}
                  attrName={'phone'}
                  updateMasterState={this._updateMasterState}
                  message={{
                    type: this.state.isErrorPhone ? 'error' : '',
                    text: this.state.messagePhone,
                  }}
                />
              </View>
              <View style={styles.separator}>
                <Text style={styles.textLabel}>
                  Nomor Rekening Bank BJB
                  <Text style={styles.required}>*</Text>
                </Text>
                <InputText
                  type={'number'}
                  placeholder={'Nomor Rekening'}
                  showMessage={this.state.showMessageBankAccount}
                  value={this.state.bankAccount}
                  attrName={'bankAccount'}
                  updateMasterState={this._updateMasterState}
                  message={{
                    type: this.state.messageBankAccount !== '' ? 'error' : '',
                    text: this.state.messageBankAccount,
                  }}
                />
              </View>
              <View style={styles.separator}>
                <Text style={styles.textLabel2}>
                  Foto KTP<Text style={styles.required}>*</Text>
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this._openCamera('identity');
                  }}>
                  {this.state.identityImage === '' ? (
                    <Image
                      source={require('../assets/images/placeholder.png')}
                      style={styles.fotoKtp}
                    />
                  ) : (
                    <Image
                      source={{uri: this.state.identityImage}}
                      style={styles.fotoKtp}
                    />
                  )}
                </TouchableOpacity>
                {this.state.messageIdentityImage === '' ? null : (
                  <Text style={styles.errorMessage4}>
                    {this.state.messageIdentityImage}
                  </Text>
                )}
                {this.state.identityImage !== '' ? (
                  <Text
                    onPress={() => {
                      this.setState({identityImage: ''});
                    }}
                    style={styles.delete}>
                    Hapus
                  </Text>
                ) : null}
              </View>
              <View style={styles.separator}>
                <Text style={styles.textLabel}>
                  E-Mail<Text style={styles.required}>*</Text>
                </Text>
                <InputText
                  type={'text'}
                  placeholder={'E-Mail'}
                  showMessage={this.state.showMessageEmail}
                  value={this.state.email}
                  attrName={'email'}
                  updateMasterState={this._updateMasterState}
                  message={{
                    type: this.state.isErrorEmail ? 'error' : '',
                    text: this.state.messageEmail,
                  }}
                />
              </View>
              <Text style={styles.required}>
                *<Text style={styles.text}>Tidak Boleh Kosong.</Text>
              </Text>
            </View>
          </View>
        </ScrollView>
      );
    } else {
      return (
        <ScrollView>
          <View style={styles.innerContainer}>
            <View style={styles.infoContainer}>
              <View style={styles.separator}>
                <Text style={styles.textLabel}>
                  Nama Usaha<Text style={styles.required}>*</Text>
                </Text>
                <InputText
                  type={'text'}
                  placeholder={'Nama Usaha'}
                  showMessage={this.state.messageBusinessName}
                  value={this.state.businessName}
                  attrName={'businessName'}
                  updateMasterState={this._updateMasterState}
                  message={{
                    type: this.state.messageBusinessName !== '' ? 'error' : '',
                    text: this.state.messageBusinessName,
                  }}
                />
              </View>
              <View style={styles.separator}>
                <Text style={styles.textLabel}>
                  Deskripsi Usaha (Minimal 150 karakter)
                  <Text style={styles.required}>*</Text>
                </Text>
                <InputText
                  type={'text'}
                  placeholder={'Deskripsi Usaha'}
                  showMessage={this.state.showMessageBusinessDesc}
                  value={this.state.businessDesc}
                  attrName={'businessDesc'}
                  updateMasterState={this._updateMasterState}
                  message={{
                    type: this.state.messageBusinessDesc !== '' ? 'error' : '',
                    text: this.state.messageBusinessDesc,
                  }}
                />
              </View>
              <View style={styles.separator}>
                <Text style={styles.textLabel}>
                  Alamat Usaha
                  <Text style={styles.required}>*</Text>
                </Text>
                <InputText
                  type={'text'}
                  placeholder={'Alamat Usaha'}
                  showMessage={this.state.showMessageBusinessAddress}
                  value={this.state.businessAddress}
                  attrName={'businessAddress'}
                  updateMasterState={this._updateMasterState}
                  message={{
                    type:
                      this.state.messageBusinessAddress !== '' ? 'error' : '',
                    text: this.state.messageBusinessAddress,
                  }}
                />
              </View>
              <View style={styles.separator}>
                <Text style={styles.textLabel2}>
                  Pilih Lokasi Pada Peta<Text style={styles.required}>*</Text>
                </Text>
                {this.state.longitude !== null &&
                this.state.longitude !== null ? (
                  <MapView
                    style={styles.maps}
                    onPress={(e) => {
                      let region = {
                        latitude: e.nativeEvent.coordinate.latitude,
                        longitude: e.nativeEvent.coordinate.longitude,
                        latitudeDelta: 0.005,
                        longitudeDelta: 0.005,
                      };
                      this.onRegionChange(region);
                    }}
                    region={{
                      latitude: this.state.latitude,
                      longitude: this.state.longitude,
                      latitudeDelta: 0.005,
                      longitudeDelta: 0.005,
                    }}>
                    <Marker
                      draggable
                      onDragEnd={(e) => {
                        this.setState({
                          latitude: e.nativeEvent.coordinate.latitude,
                          longitude: e.nativeEvent.coordinate.longitude,
                        });
                      }}
                      coordinate={{
                        latitude:
                          this.state.latitude === null
                            ? 0.0
                            : this.state.latitude,
                        longitude:
                          this.state.longitude === null
                            ? 0.0
                            : this.state.longitude,
                      }}
                    />
                  </MapView>
                ) : null}
              </View>
              <Text style={styles.required}>
                *<Text style={styles.text}>Tidak Boleh Kosong.</Text>
              </Text>
            </View>
          </View>
        </ScrollView>
      );
    }
  }

  _openCamera(type) {
    launchCamera(pickerOption, (result) => {
      if (!result.didCancel) {
        if (type === 'identity') {
          this.setState({
            identityImage: result.uri,
            modalPicker: false,
            messageIdentityImage: '',
          });
        } else {
          this.setState({userPhoto: result.uri, modalPicker: false});
        }
      }
    });
  }

  _openGalery() {
    launchImageLibrary(pickerOption, (result) => {
      if (!result.didCancel) {
        this.setState({userPhoto: result.uri, modalPicker: false});
      }
    });
  }

  fetchUserData() {
    AsyncStorage.getItem('access_token').then((token) => {
      axios
        .get(Cf.base_url + uri, {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        })
        .then((res) => {
          if (res.data.data.user_profile === null) {
            this.setState({
              name: res.data.data.name,
              phone: res.data.data.phone_number,
              email: res.data.data.email,
              userPhoto: res.data.data.photo.url,
            });
            this._requestLocation();
          } else {
            this.setState({
              id: res.data.data.user_profile.id,
              name: res.data.data.name,
              phone: res.data.data.phone_number,
              email: res.data.data.email,
              bankAccount: res.data.data.user_profile.account_number,
              userData: res.data.data,
              userPhoto: res.data.data.photo.url,
              userProfile: res.data.data.user_profile,
              identityImage: res.data.data.user_profile.identity_photo.url,
              businessName: res.data.data.user_profile.bussiness_name,
              businessDesc: res.data.data.user_profile.bussiness_description,
              businessAddress: res.data.data.user_profile.bussiness_address,
            });
            if (res.data.data.user_profile.latitude === null) {
              this.setState({
                longitude: 0.0,
                latitude: 0.0,
              });
              this._requestLocation();
            } else {
              this.setState({
                longitude: res.data.data.user_profile.longitude,
                latitude: res.data.data.user_profile.latitude,
              });
            }
          }
        })
        .catch((error) => {
          if (error.response.status === 401) {
            actions.removeToken();
            this.props.navigation.replace('Auth');
          }
        });
    });
  }
  _renewData(type) {
    if (this.state.bankAccount === '') {
      this.setState({
        messageBankAccount: 'No Rekening Tidak Boleh Kosong',
        showMessageBankAccount: true,
      });
      this.ScrollView.current.scrollTo({y: 227});
    } else if (this.state.bankAccount.length < 13) {
      this.setState({
        messageBankAccount: 'No Rekening Tidak Sesuai, Minimal 13 Digit',
        showMessageBankAccount: true,
      });
      this.ScrollView.current.scrollTo({y: 227});
    } else {
      this.setState({messageBankAccount: ''});
    }

    if (this.state.identityImage === '') {
      this.setState({
        messageIdentityImage: 'Ambil Foto KTP terlebih dahulu',
      });
      this.ScrollView.current.scrollTo({y: 227});
    } else {
      this.setState({
        messageIdentityImage: '',
      });
    }

    if (this.state.businessName === '' || this.state.businessName === null) {
      this.setState({
        messageBusinessName: 'Nama Usaha Tidak Boleh Kosong.',
        showMessageBusinessName: true,
      });
    } else {
      this.setState({
        messageBankAccount: '',
        showMessageBankAccount: true,
      });
    }

    if (this.state.businessDesc === '' || this.state.businessDesc === null) {
      this.setState({
        messageBusinessDesc: 'Deskripsi Usaha Tidak Boleh Kosong.',
        showMessageBusinessDesc: true,
      });
    } else {
      this.setState({
        messageBusinessDesc: '',
        showMessageBusinessDesc: true,
      });
    }

    if (
      this.state.businessAddress === '' ||
      this.state.businessAddress === null
    ) {
      this.setState({
        messageBusinessAddress: 'Alamat Usaha Tidak Boleh Kosong.',
        showMessageBusinessAddress: true,
      });
    } else {
      this.setState({
        messageBusinessAddress: '',
        showMessageBusinessAddress: true,
      });
    }

    if (type === 'profile') {
      if (
        this.state.name === '' ||
        this.state.phone === '' ||
        this.state.bankAccount === '' ||
        this.state.identityImage === '' ||
        this.state.userPhoto === '' ||
        this.state.email === ''
      ) {
      } else {
        this.setState({isLoading: true});
        var data = new FormData();
        if (type === 'profile') {
          data.append('user[name]', this.state.name);
          data.append('user[phone_number]', this.state.phone);
          data.append(
            'user[user_profile_attributes][account_number]',
            this.state.bankAccount,
          );
          data.append('user[user_profile_attributes][id]', this.state.id);
          data.append('user[photo]', {
            uri: this.state.userPhoto,
            type: 'image/jpeg',
            name: 'image-' + new Date() + '.jpg',
          });
          data.append('user[user_profile_attributes][identity_photo]', {
            uri: this.state.identityImage,
            type: 'image/jpeg',
            name: 'images-' + new Date() + '.jpg',
          });
        } else {
          data.append('user[user_profile_attributes][id]', this.state.id);
          data.append(
            'user[user_profile_attributes][bussiness_name]',
            this.state.businessName,
          );
          data.append(
            'user[user_profile_attributes][bussiness_description]',
            this.state.businessDesc,
          );
          data.append(
            'user[user_profile_attributes][bussiness_address]',
            this.state.businessAddress,
          );
          data.append(
            'user[user_profile_attributes][longitude]',
            this.state.longitude,
          );
          data.append(
            'user[user_profile_attributes][latitude]',
            this.state.latitude,
          );
        }
        AsyncStorage.getItem('access_token').then((token) => {
          axios
            .put(Cf.base_url + uri, data, {
              headers: {
                Authorization: 'Bearer ' + token,
                '*/*': 'multipart/form-data',
              },
            })
            .then((res) => {
              if (res.data.code === 200) {
                this.setState({
                  modalAlert: true,
                  isLoading: false,
                });
              }
            })
            .catch((error) => {
              this.setState({
                isLoading: false,
              });
              if (error.response.status === 401) {
                actions.removeToken();
                this.props.navigation.replace('Auth');
              }
            });
        });
      }
    } else {
      if (
        this.state.businessName === null ||
        this.state.businessDesc === null ||
        this.state.businessAddress === null ||
        this.state.businessName === '' ||
        this.state.businessDesc === '' ||
        this.state.businessAddress === '' ||
        this.state.longitude === 0.0 ||
        this.state.latitude === 0.0
      ) {
      } else {
        this.setState({isLoading: true});
        var data = new FormData();
        data.append('user[user_profile_attributes][id]', this.state.id);
        data.append(
          'user[user_profile_attributes][bussiness_name]',
          this.state.businessName,
        );
        data.append(
          'user[user_profile_attributes][bussiness_description]',
          this.state.businessDesc,
        );
        data.append(
          'user[user_profile_attributes][bussiness_address]',
          this.state.businessAddress,
        );
        data.append(
          'user[user_profile_attributes][longitude]',
          this.state.longitude,
        );
        data.append(
          'user[user_profile_attributes][latitude]',
          this.state.latitude,
        );
        AsyncStorage.getItem('access_token').then((token) => {
          axios
            .put(Cf.base_url + uri, data, {
              headers: {
                Authorization: 'Bearer ' + token,
                '*/*': 'multipart/form-data',
              },
            })
            .then((res) => {
              if (res.data.code === 200) {
                this.setState({
                  modalAlert: true,
                  isLoading: false,
                });
              }
            })
            .catch((error) => {
              this.setState({
                isLoading: false,
              });
              if (error.response.status === 401) {
                actions.removeToken();
                this.props.navigation.replace('Auth');
              }
            });
        });
      }
    }
  }

  render() {
    return (
      <LinearGradient colors={[white, white1]} style={styles.gradient}>
        <SafeAreaView style={styles.container}>
          <Header style={styles.header} androidStatusBarColor={blue2}>
            <Left>
              <TouchableOpacity
                style={styles.backButton}
                onPress={() => {
                  this.props.navigation.goBack();
                }}>
                <Image source={require('../assets/icons/arrow-left.png')} />
              </TouchableOpacity>
            </Left>
            <Body>
              <Title style={styles.textHeader}>Perbarui Data Diri</Title>
            </Body>
            <Right />
          </Header>
          {this.renderMenu()}
          <Overlay
            isVisible={this.state.modalPicker}
            onBackdropPress={() => {
              this.setState({modalPicker: false});
            }}>
            <View>
              <Text style={styles.textDesc}>Ganti Foto Profil</Text>
              <View style={styles.underline} />
              <View style={styles.pickerContainer}>
                <TouchableOpacity
                  onPress={() => {
                    this._openCamera('profile');
                  }}
                  style={styles.btnPicker}>
                  <Text style={styles.textHeader}>Ambil Dari Kamera</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({modalPicker: false});
                    this._openGalery();
                  }}
                  style={styles.btnPicker}>
                  <Text style={styles.textHeader}>Ambil Dari Galery</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Overlay>
          <Overlay
            isVisible={this.state.modalAlert}
            onShow={() => {
              setTimeout(() => {
                this.setState({modalAlert: false});
                this.props.navigation.goBack();
              }, 1500);
            }}>
            <View style={styles.modalAlert}>
              <ImageSuccess />
              <Text style={styles.textAlert}>
                Data {this.state.type.type === 'profile' ? 'diri ' : 'usaha '}
                berhasil diperbarui.
              </Text>
            </View>
          </Overlay>
          <View style={styles.bottomAction}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}
              style={styles.btnConfirmOutline}>
              <Text style={styles.textBtnOutlined}>Batal</Text>
            </TouchableOpacity>
            <TouchableOpacity
              disabled={this.state.isLoading}
              onPress={() => {
                this._renewData(this.state.type.type);
              }}
              style={styles.btnConfirmSolid}>
              {this.state.isLoading === false ? (
                <Text style={styles.textBtnSolid}>Simpan</Text>
              ) : (
                <ActivityIndicator size="small" color={white} />
              )}
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </LinearGradient>
    );
  }
}
