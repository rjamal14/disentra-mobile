import React from 'react';
import {Image, Text, View, KeyboardAvoidingView} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from '../screens/styles/ForgotPasswordStyle';
import {white, white1} from '../constants/colors';
import {ButtonL} from '../components/Buttons';
import InputText from '../components/InputText';
import LinearGradient from 'react-native-linear-gradient';
import {regEmail} from '../constants/commons';
import IconMail from '../assets/icons/ic_mail.svg';
import PaperPlane from '../assets/images/paper-plane.svg';
import Cf from '../config';

export default class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showMessage: false,
      errorMessage: '',
      email: '',
      messageEmail: '',
      isErrorEmail: false,
      showMessageEmail: false,
      isLoading: false,
      isEmailSend: false,
    };
  }

  translateMessage(message) {
    if (message === 'user_not_found') {
      return 'Email tidak terdaftar, tolong cek kembali email anda';
    } else {
      return message;
    }
  }

  _updateMasterState = (attrName, value) => {
    if (attrName === 'email' && this.state.isErrorEmail) {
      this.setState({
        isErrorEmail: false,
        showMessageEmail: false,
      });
    }
    this.setState({[attrName]: value});
  };

  _handleRequestForgot() {
    fetch(Cf.base_url + '/api/v1/users/password', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        scope: 'mobile',
      }),
    })
      .then((res) => res.json())
      .then((response) => {
        if (response.data.error !== undefined) {
          this.setState({
            showMessageEmail: true,
            messageEmail: this.translateMessage(response.data.error),
            isErrorEmail: true,
          });
        } else {
          this.setState({
            isErrorEmail: false,
            showMessageEmail: false,
            isLoading: true,
          });
          setTimeout(() => {
            this.setState({
              isLoading: false,
              isEmailSend: true,
            });
          }, 2000);
        }
      });
  }

  render() {
    if (!this.state.isEmailSend) {
      return (
        <LinearGradient colors={[white, white1]} style={styles.gradient}>
          <SafeAreaView style={styles.container}>
            <KeyboardAvoidingView behavior={'height'} style={styles.container}>
              <View style={styles.innerContainer}>
                <Image
                  style={styles.logoIcon}
                  source={require('../assets/images/logo-bjb-color.png')}
                />
                <Image
                  style={styles.handAbove}
                  source={require('../assets/images/bg-hand-above.png')}
                />
                <Image
                  style={styles.logo}
                  source={require('../assets/images/logo-color.png')}
                />
                <View style={styles.ContentContainer}>
                  <Text style={styles.textHeader}>Lupa Kata Sandi?</Text>
                  <Text style={styles.textContent}>
                    Mohon masukkan alamat email Anda. Kami akan mengirimkan
                    tautan untuk mengatur ulang kata sandi Anda.
                  </Text>
                </View>
                {this.state.showMessage ? (
                  <Text style={styles.errorMessage}>
                    {this.state.errorMessage}
                  </Text>
                ) : null}
                <InputText
                  type={'email'}
                  placeholder={'Email'}
                  showMessage={this.state.showMessageEmail}
                  value={this.state.email}
                  attrName={'email'}
                  updateMasterState={this._updateMasterState}
                  message={{
                    type: this.state.isErrorEmail ? 'error' : '',
                    text: this.state.messageEmail,
                  }}
                  icons={<IconMail />}
                />
                <ButtonL
                  title="KIRIM"
                  loading={this.state.isLoading}
                  onPress={() => {
                    if (this.state.email === '') {
                      this.setState({
                        isErrorEmail: true,
                        showMessageEmail: true,
                        messageEmail: 'Email Tidak Boleh Kosong',
                      });
                    } else if (!regEmail.test(this.state.email)) {
                      this.setState({
                        isErrorEmail: true,
                        showMessageEmail: true,
                        messageEmail: 'Email Tidak Valid',
                      });
                    } else {
                      if (
                        this.state.email === '' ||
                        !regEmail.test(this.state.email)
                      ) {
                      } else {
                        this._handleRequestForgot();
                      }
                    }
                  }}
                />
                <Text
                  onPress={() => {
                    this.props.navigation.navigate('Login');
                  }}
                  style={styles.textLink2}>
                  Punya akun?<Text style={styles.textLink}> masuk</Text>
                </Text>
              </View>
            </KeyboardAvoidingView>
          </SafeAreaView>
        </LinearGradient>
      );
    } else {
      return (
        <LinearGradient colors={[white, white1]} style={styles.gradient}>
          <SafeAreaView style={styles.innerContainer}>
            <Image
              style={styles.logoIcon}
              source={require('../assets/images/logo-bjb-color.png')}
            />
            <Image
              style={styles.handAbove}
              source={require('../assets/images/bg-hand-above.png')}
            />
            <Image
              style={styles.logo}
              source={require('../assets/images/logo-color.png')}
            />
            <View style={styles.ContentContainer}>
              <PaperPlane />
            </View>
            <View style={styles.ContentContainer}>
              <Text style={styles.textHeader}>Email Terkirim</Text>
              <Text style={styles.textContent}>
                Kami telah mengirimkan tautan ke alamat email
                {' ' + this.state.email} untuk mengatur ulang kata sandi Anda.
              </Text>
            </View>
            <Text
              onPress={() => {
                this.props.navigation.navigate('Login');
              }}
              style={styles.textLink}>
              Kembali ke halaman login
            </Text>
          </SafeAreaView>
        </LinearGradient>
      );
    }
  }
}
