import React from 'react';
import {Image, StatusBar} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from './styles/SplashStyle';
import {blue, blue1, blue2} from '../constants/colors';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class SplashScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFocused: false,
      isError: false,
    };
  }

  _navigateTo = (routeName: string) => {
    this.props.navigation.replace(routeName);
  };

  async componentDidMount() {
    const value = await AsyncStorage.getItem('access_token');
    setTimeout(() => {
      if (value === null || value === undefined) {
        this._navigateTo('Auth');
      } else {
        this._navigateTo('Drawers');
      }
    }, 2000);
  }

  render() {
    return (
      <LinearGradient
        colors={[blue2, blue1, blue]}
        style={styles.gradient}
        useAngle={true}
        angle={45}
        locations={[0.2, 0.9, 1.1]}
        angleCenter={{x: 0.5, y: 0.5}}>
        <StatusBar
          animated={true}
          backgroundColor={blue2}
          barStyle={'content-light'}
        />
        <SafeAreaView style={styles.container}>
          <Image
            style={styles.logo}
            source={require('../assets/images/logo-disentra-white.png')}
          />
          <Image
            style={styles.handAbove}
            source={require('../assets/images/bg-hand-above.png')}
          />
          <Image
            style={styles.handBelow}
            source={require('../assets/images/bg-hand-below.png')}
          />
        </SafeAreaView>
      </LinearGradient>
    );
  }
}
