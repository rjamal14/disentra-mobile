import React, {useState, useEffect} from 'react';
import {
  FlatList,
  Image,
  Linking,
  RefreshControl,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {white, white1} from '../constants/colors';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from '../screens/styles/BusinessClinicStyle';
import {ScrollView} from 'react-native-gesture-handler';
import {Card} from 'native-base';
import Profession from '../assets/icons/ic_profession.svg';
import Office from '../assets/icons/ic_office.svg';
import WA from '../assets/icons/ic_whatsapp.svg';
import config from '../config';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import actions from '../actions';

function BusinessClinic(props) {
  const [token, setToken] = useState(null);
  const [datas, setDatas] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  const [banner, setBanner] = useState('');
  const [pagination, setPagination] = useState('');

  useEffect(() => {
    getBanner('clinic');
  }, []);
  function getBanner(type) {
    fetch(config.base_url + config.get_banner + type, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        setBanner(responseJson.data[0].image.url);
      });
  }
  const getToken = async () => {
    const value = await AsyncStorage.getItem('access_token');
    setToken(value);
  };
  getToken();
  useEffect(() => {
    if (token !== null) {
      getData();
    }
  }, [token]);

  function getData(next = false) {
    axios
      .get(
        config.base_url +
          config.business_clinics_url +
          '?page=' +
          (next ? pagination.next_page : 1),
        {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        },
      )
      .then((response) => {
        if (next) {
          setDatas([...datas, ...response.data.data]);
        } else {
          setDatas(response.data.data);
        }
        setPagination(response.data.pagination);
        setRefreshing(false);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          actions.removeToken();
          props.navigation.replace('Auth');
        }
      });
  }

  const renderItem = ({item}) => <Items value={item} />;

  function onRefresh() {
    setRefreshing(true);
    getData();
  }

  return (
    <LinearGradient colors={[white, white1]} style={styles.gradient}>
      <SafeAreaView style={styles.container}>
        <Image style={styles.logo} source={{uri: banner}} />
        <ScrollView
          style={styles.scroll}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          {datas.length > 0 ? (
            <FlatList
              data={datas}
              renderItem={renderItem}
              keyExtractor={(item) => item.phone_number}
              onEndReached={() => {
                if (pagination !== null && pagination.next_page !== null) {
                  getData(true);
                }
              }}
              onEndReachedThreshold={7}
            />
          ) : (
            <Text style={styles.textNodata}>Data tidak ditemukan</Text>
          )}
        </ScrollView>
      </SafeAreaView>
    </LinearGradient>
  );
}

function Items(props) {
  const {value} = props;
  const url = config.whatsapp_url + value.phone_number;

  function sendWhatsApp() {
    Linking.openURL(url)
      .then(() => {})
      .catch(() => {
        ToastAndroid.show(
          'Pastikan WhatsApp telah terinstall',
          ToastAndroid.LONG,
        );
      });
  }

  return (
    <View>
      <Card style={styles.radiusCard}>
        <View style={styles.displayCard}>
          <Image style={styles.propic} source={{uri: value.photo.url}} />
          <View style={styles.field}>
            <Text style={styles.name}>
              {value.name !== undefined ? value.name : value.id}
            </Text>
            <View style={styles.textWithLeftIcon}>
              <Profession />
              <Text style={styles.textDesc}>{value.account_description}</Text>
            </View>
            <View style={styles.textWithLeftIcon}>
              <Office marginLeft={-2} />
              <Text style={styles.textDesc}>{value.consultant_profession}</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            sendWhatsApp();
          }}>
          <View
            style={{
              backgroundColor: '#EFCA18',
              width: '100%',
              height: 40,
              borderBottomEndRadius: 10,
              borderBottomStartRadius: 10,
              display: 'flex',
              flexDirection: 'row',
              alignContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
            }}>
            <WA marginRight={3} />
            <Text style={styles.activeTextChat}>CHAT</Text>
          </View>
        </TouchableOpacity>
      </Card>
    </View>
  );
}

export default BusinessClinic;
