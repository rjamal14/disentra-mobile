import React, {useState} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
} from 'react-native';
import {Header, Body} from 'native-base';
import styles from '../styles/BusinessSeminarStyle';

function WelcomeScreen(props) {
  const {modalVisible, setModalVisible, time_props, valueFilter} = props;
  const [time, setTime] = useState('');

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(false);
      }}>
      <View style={styles.modalContainer} />
      <View style={styles.modalContentContainer3}>
        <Header style={styles.header}>
          <Body style={styles.header2}>
            <View style={styles.saring}>
              <Text>Saring</Text>
            </View>
            <TouchableOpacity
              style={styles.close}
              onPress={() => {
                if (time_props === time) {
                  setModalVisible(false);
                } else {
                  Alert.alert(
                    'Perubahan belum diterapkan',
                    'Anda akan menerapkan perubahan?',
                    [
                      {
                        text: 'Tidak',
                        onPress: () => {
                          setTime(time_props);
                          setModalVisible(false);
                        },
                        style: 'cancel',
                      },
                      {
                        text: 'Ya',
                        onPress: () => {
                          valueFilter(time);
                          setModalVisible(false);
                        },
                      },
                    ],
                    {cancelable: false},
                  );
                }
              }}>
              <Image source={require('../../assets/icons/close.png')} />
            </TouchableOpacity>
          </Body>
        </Header>
        <ScrollView style={styles.modalContentContainer2}>
          <Text style={styles.filterHeader}>WAKTU</Text>
          <View style={styles.filterItemContainer}>
            <TouchableOpacity
              style={
                time === 'today' ? styles.filterItemActive : styles.filterItem
              }
              onPress={() => {
                setTime('today');
              }}>
              <Text style={styles.filterItemText}>Hari Ini</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                time === 'this_week'
                  ? styles.filterItemActive
                  : styles.filterItem
              }
              onPress={() => {
                setTime('this_week');
              }}>
              <Text style={styles.filterItemText}>Minggu Ini</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                time === 'this_month'
                  ? styles.filterItemActive
                  : styles.filterItem
              }
              onPress={() => {
                setTime('this_month');
              }}>
              <Text style={styles.filterItemText}>Bulan Ini</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={
                time === 'this_year'
                  ? styles.filterItemActive
                  : styles.filterItem
              }
              onPress={() => {
                setTime('this_year');
              }}>
              <Text style={styles.filterItemText}>Tahun Ini</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <View style={styles.action}>
          <TouchableOpacity
            style={styles.actionReset}
            onPress={() => {
              setModalVisible(false);
              valueFilter('');
              setTime('');
            }}>
            <Text style={styles.textReset}>Reset</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.actionFilter}
            onPress={() => {
              setModalVisible(false);
              valueFilter(time);
            }}>
            <Text style={styles.textFilter}>Terapkan</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}

export default WelcomeScreen;
