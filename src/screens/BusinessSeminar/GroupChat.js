/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {ScrollView, StatusBar, FlatList, RefreshControl} from 'react-native';
import styles from '../../screens/styles/BusinessSeminarStyle';
import InputText from '../../components/InputText';
import IconSearch from '../../assets/icons/ic_search.svg';
import {View, TouchableOpacity, Image, Text} from 'react-native';
import {Card} from 'native-base';
import {blue2} from '../../constants/colors';
import axios from 'axios';
import config from '../../config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import actions from '../../actions';

const uri = 'api/v1/mobile/';
const filter = 'business_talks?q%5Btalk_type_eq%5D=grupchat';

function Item(props) {
  const {value, navigation} = props;
  return (
    <View>
      <Card style={styles.radiusCard}>
        <TouchableOpacity
          style={styles.areaCardChat}
          onPress={() => {
            navigation.navigate('detailSeminar', {value});
          }}>
          <TouchableOpacity
            style={styles.titleChat}
            onPress={() => {
              navigation.navigate('detailSeminar', {value});
            }}>
            <Text style={styles.titleCardChat}>{value.title}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.coverChat}
            onPress={() => {
              navigation.navigate('detailSeminar', {value});
            }}>
            <Image
              style={styles.imgCoverChat}
              source={{
                uri: value.image.url,
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            disabled={value.has_joined}
            style={styles.btnRegisterChat}
            onPress={() => {
              navigation.navigate('detailSeminar', {value});
            }}>
            <Text style={styles.titleRegistrasi}>
              {value.has_joined ? 'TERDAFTAR' : 'DAFTAR'}
            </Text>
          </TouchableOpacity>
        </TouchableOpacity>
      </Card>
    </View>
  );
}

function Seminar(props) {
  const [token, setToken] = useState(null);
  const [search, setSearch] = useState('');
  const [datas, setDatas] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  const [pagination, setPagination] = useState('');

  const getToken = async () => {
    const value = await AsyncStorage.getItem('access_token');
    setToken(value);
  };
  getToken();
  useEffect(() => {
    if (token !== null) {
      getData();
    }
  }, [token]);

  function getData(next) {
    axios
      .get(
        config.base_url +
          uri +
          filter +
          (search !== '' ? '&search=' + search : '') +
          '&page=' +
          (next ? pagination.next_page : 1),
        {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        },
      )
      .then((response) => {
        if (next) {
          setDatas([...datas, ...response.data.data]);
        } else {
          setDatas(response.data.data);
        }
        setPagination(response.data.pagination);
        setRefreshing(false);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          actions.removeToken();
          props.navigation.replace('Auth');
        }
      });
  }

  const renderItem = ({item}) => (
    <Item value={item} token={token} navigation={props.navigation} />
  );

  function onRefresh() {
    setRefreshing(true);
    getData();
  }

  function updateMasterState(attr, value) {
    setSearch(value);
  }

  return (
    <ScrollView
      style={styles.contentSeminar}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }>
      <StatusBar
        animated={true}
        backgroundColor={blue2}
        barStyle={'content-light'}
      />
      <View style={styles.searchContainer}>
        <InputText
          type={'searchBisinis'}
          placeholder={'Cari'}
          showMessage={''}
          value={search}
          attrName={'search'}
          message={''}
          updateMasterState={updateMasterState}
          icons={
            <IconSearch
              onPress={() => {
                getData();
              }}
            />
          }
        />
      </View>
      <View style={styles.divider} />
      {datas.length === 0 || token === null ? (
        <Text style={styles.textNodata}>Data tidak ditemukan.</Text>
      ) : (
        <View style={styles.areaCard}>
          <FlatList
            data={datas}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            onEndReached={() => {
              if (pagination !== null && pagination.next_page !== null) {
                getData('', '', true);
              }
            }}
            onEndReachedThreshold={7}
          />
        </View>
      )}
    </ScrollView>
  );
}

export default Seminar;
