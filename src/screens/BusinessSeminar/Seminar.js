/* eslint-disable radix */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {ScrollView, StatusBar, FlatList, RefreshControl} from 'react-native';
import styles from '../../screens/styles/BusinessSeminarStyle';
import InputText from '../../components/InputText';
import IconSearch from '../../assets/icons/ic_search.svg';
import {View, TouchableOpacity, Image, Text} from 'react-native';
import {Card} from 'native-base';
import {blue2} from '../../constants/colors';
import axios from 'axios';
import config from '../../config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import actions from '../../actions';
import {Button} from 'react-native-elements';
import FilterIcon from '../../assets/icons/ic_filter';
import FilterCoimponent from './filter';

function Item(props) {
  const {navigation, value} = props;
  return (
    <View>
      <Card style={styles.radiusCard}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('detailSeminar', {value});
          }}>
          <Image
            style={styles.imgCover}
            source={{
              uri: value?.image?.url,
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.areaCard}
          onPress={() => {
            navigation.navigate('detailSeminar', {value});
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('detailSeminar', {value});
            }}>
            <Text style={styles.titleCard}>{value.title}</Text>
          </TouchableOpacity>
          <View style={styles.areaContent}>
            <Image source={require('../../assets/icons/person-seminar.png')} />
            <Text style={styles.textContent}>{value.speaker}</Text>
          </View>
          <View style={styles.areaContent}>
            <Image source={require('../../assets/icons/calendar.png')} />
            <Text style={styles.textContent}>
              {actions.FormatDate(new Date(value.event_on))}
            </Text>
          </View>
          <TouchableOpacity
            disabled={value.has_joined}
            style={styles.btnRegister}
            onPress={() => {
              navigation.navigate('detailSeminar', {value});
            }}>
            <Text style={styles.titleRegistrasi}>
              {value.has_joined ? 'TERDAFTAR' : 'DAFTAR'}
            </Text>
          </TouchableOpacity>
        </TouchableOpacity>
      </Card>
    </View>
  );
}

function Seminar(props) {
  const [token, setToken] = useState(null);
  const [search, setSearch] = useState('');
  const [datas, setDatas] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [time, setTime] = useState('');
  const [pagination, setPagination] = useState('');

  const getToken = async () => {
    const value = await AsyncStorage.getItem('access_token');
    setToken(value);
  };
  getToken();
  useEffect(() => {
    if (token !== null) {
      getData();
    }
  }, [token]);

  function getData(query = '', times = '', next = false) {
    var d = new Date();
    var date =
      d.getDate() +
      '-' +
      (parseInt(d.getMonth() + 1) < 10
        ? '0' + parseInt(d.getMonth() + 1)
        : parseInt(d.getMonth() + 1)) +
      '-' +
      d.getFullYear();
    const uri = 'api/v1/mobile/business_talks?q%5Btalk_type_eq%5D=sentraminar';
    const filter_time = '&filter_time=' + times;
    const sear = '&search=' + query;
    const rangeDate =
      '&q[event_on_gteq]=' +
      date +
      ' 00:00&q[event_on_lteq]=' +
      date +
      ' 23:59';
    let path = uri;
    if (times === 'today') {
      path += rangeDate + (query === '' ? '' : sear);
    } else {
      path += filter_time + (query === '' ? '' : sear);
    }
    axios
      .get(
        config.base_url + path + '&page=' + (next ? pagination.next_page : 1),
        {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        },
      )
      .then((response) => {
        if (next) {
          setDatas([...datas, ...response.data.data]);
        } else {
          setDatas(response.data.data);
        }
        setPagination(response.data.pagination);
        setRefreshing(false);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          actions.removeToken();
          props.navigation.replace('Auth');
        }
      });
  }

  const renderItem = ({item}) => (
    <Item value={item} navigation={props.navigation} />
  );

  function onRefresh() {
    setRefreshing(true);
    getData(search, time);
  }

  function updateMasterState(_attr, value) {
    setSearch(value);
  }

  return (
    <ScrollView
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }>
      <StatusBar
        animated={true}
        backgroundColor={blue2}
        barStyle={'content-light'}
      />
      <FilterCoimponent
        modalVisible={modalVisible}
        time_props={time}
        valueFilter={(val) => {
          setTime(val);
          getData(search, val);
        }}
        setModalVisible={(val) => {
          setModalVisible(val);
        }}
      />
      <View style={styles.searchContainer}>
        <View>
          <Button
            icon={<FilterIcon />}
            title={'Saring'}
            type={'outlined'}
            buttonStyle={styles.filterButton}
            titleStyle={styles.cardContent}
            onPress={() => {
              setModalVisible(true);
            }}
          />
        </View>
        <InputText
          type={'search'}
          placeholder={'Cari'}
          showMessage={''}
          value={search}
          attrName={'search'}
          message={''}
          updateMasterState={updateMasterState}
          icons={
            <IconSearch
              onPress={() => {
                getData(search, time);
              }}
            />
          }
        />
      </View>
      <View style={styles.divider} />
      {datas.length === 0 || token === null ? (
        <Text style={styles.textNodata}>Data tidak ditemukan.</Text>
      ) : (
        <View style={styles.areaCard}>
          <FlatList
            data={datas}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            onEndReached={() => {
              if (pagination !== null && pagination.next_page !== null) {
                getData('', '', true);
              }
            }}
            onEndReachedThreshold={7}
          />
        </View>
      )}
    </ScrollView>
  );
}

export default Seminar;
