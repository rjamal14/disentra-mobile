import React from 'react';
import {View, Text, Image, TouchableOpacity, ScrollView} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from '../../screens/styles/BusinessSeminarStyle';
import {Card} from 'native-base';
import actions from '../../actions';

function WelcomeScreen(props) {
  const {type, value, variant} = props.route.params;
  return (
    <SafeAreaView>
      <ScrollView>
        {type === 'success' ? (
          <Image
            style={styles.imgResult}
            source={require('../../assets/images/success.png')}
          />
        ) : (
          <Image
            style={styles.imgResult}
            source={require('../../assets/images/failure.png')}
          />
        )}
        <View style={styles.spacingCard}>
          <Text style={styles.textSelamat}>
            {type === 'success'
              ? 'Selamat! Pendaftaran Berhasil'
              : 'Kuota Sudah Penuh'}
          </Text>
          <Text style={styles.textDesc2}>
            {type === 'success'
              ? 'Anda baru saja mendaftarkan diri pada Sentraminar:'
              : 'Mohon maaf, kuota Sentraminar berikut sudah penuh.'}
          </Text>
        </View>
        <View style={styles.areaCard}>
          {variant === 'sentraminar' ? (
            <Card style={[styles.radiusCard, styles.spacingCard]}>
              <View>
                <Text style={styles.titleCard}>{value.title}</Text>
              </View>
              <Text style={styles.textTheme}>{value.theme}</Text>
              <View style={styles.areaContent}>
                <Image source={require('../../assets/icons/calendar.png')} />
                <Text style={styles.textContent}>
                  {actions.FormatDate(new Date(value.event_on)) +
                    ', ' +
                    value.start_time +
                    '-' +
                    value.end_time +
                    ' WIB'}
                </Text>
              </View>
            </Card>
          ) : (
            <Card style={styles.radiusCard}>
              <TouchableOpacity style={styles.areaCardChat}>
                <TouchableOpacity style={styles.titleChatResult}>
                  <Text style={styles.titleCardChat}>{value.title}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.coverChat}>
                  <Image
                    style={styles.imgCoverChat}
                    source={{
                      uri: value.image.url,
                    }}
                  />
                </TouchableOpacity>
              </TouchableOpacity>
            </Card>
          )}
        </View>
        <Text style={styles.textDesc3}>
          {type === 'success'
            ? 'Konfirmasi pendaftaran akan dikirimkan melalui'
            : 'Silahkan pilih ' +
              (variant === 'sentraminar' ? 'Sentraminar' : 'Grup Chat') +
              ' lainnya'}
        </Text>
        <Text style={styles.textDesc2}>
          {type === 'success' ? 'notifikasi mobile DiSentra.' : ''}
        </Text>
        <TouchableOpacity
          style={styles.btnRegisterDetail2}
          onPress={() => {
            props.navigation.navigate('ContentScreen');
          }}>
          <Text style={styles.titleRegistrasiDetail}>
            KEMBALI KE HALAMAN UTAMA
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
}

export default WelcomeScreen;
