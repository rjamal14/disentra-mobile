import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Modal,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from '../../screens/styles/BusinessSeminarStyle';
import {blue2} from '../../constants/colors';
import {Header, Left, Body, Right, Title} from 'native-base';
import actions from '../../actions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import config from '../../config';
import ImageViewer from 'react-native-image-zoom-viewer';

const uri = 'api/v1/mobile/join_business_talk';

function WelcomeScreen(props) {
  const value = props.route.params.value;
  const {navigation} = props;
  const [token, setToken] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const getToken = async () => {
    const values = await AsyncStorage.getItem('access_token');
    setToken(values);
  };
  getToken();
  let img = [{url: value.image.url}];
  function Register() {
    axios
      .post(
        config.base_url + uri,
        {
          id: value.id,
        },
        {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        },
      )
      .then((response) => {
        navigation.navigate('resultSeminar', {
          value: value,
          type: 'success',
          variant: value.talk_type,
        });
      })
      .catch((error) => {
        navigation.navigate('resultSeminar', {
          value: value,
          type: 'error',
          variant: value.talk_type,
        });
        if (error.response.status === 401) {
          actions.removeToken();
          navigation.replace('Auth');
        }
      });
  }
  return (
    <SafeAreaView>
      <ScrollView>
        <Header style={styles.header} androidStatusBarColor={blue2}>
          <Left>
            <TouchableOpacity
              style={styles.backButton}
              onPress={() => {
                navigation.goBack();
              }}>
              <Image source={require('../../assets/icons/arrow-left.png')} />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title style={styles.textHeader}>
              {value.talk_type === 'sentraminar' ? 'Sentraminar' : 'GROUP CHAT'}
            </Title>
          </Body>
          <Right />
        </Header>
        <TouchableOpacity
          onPress={() => {
            setShowModal(true);
          }}>
          <Image
            style={styles.imgCover2}
            source={{
              uri: value.image.url,
            }}
          />
        </TouchableOpacity>
        <View style={styles.areaCard}>
          <View>
            <Text style={styles.titleCard}>{value.title}</Text>
          </View>
          <Text style={styles.textTheme}>{value.theme}</Text>
          <View style={styles.areaContent}>
            <Image source={require('../../assets/icons/person-seminar.png')} />
            <Text style={styles.textContent}>{value.speaker}</Text>
          </View>
          <View style={styles.areaContent}>
            <Image source={require('../../assets/icons/calendar.png')} />
            <Text style={styles.textContent}>
              {actions.FormatDate(new Date(value.event_on)) +
                ', ' +
                value.start_time +
                '-' +
                value.end_time +
                ' WIB'}
            </Text>
          </View>
          <View style={styles.areaContent}>
            <Image source={require('../../assets/icons/healthcare.png')} />
            <Text style={styles.textContent}>
              Protokol Kesehatan: {value.health_protocol ? 'Ya' : 'Tidak'}
            </Text>
          </View>
          <TouchableOpacity
            disabled={value.has_joined}
            style={styles.btnRegisterDetail}
            onPress={() => {
              Register();
            }}>
            <Text style={styles.titleRegistrasi}>
              {value.has_joined ? 'TERDAFTAR' : 'DAFTAR'}
            </Text>
          </TouchableOpacity>
          <Text style={styles.textDesc}>DESKRIPSI</Text>
          <Text style={styles.textTheme}>
            {value.description === undefined ? '' : value.description}
          </Text>
        </View>
      </ScrollView>
      <Modal visible={showModal} transparent={true}>
        <TouchableOpacity
          style={styles.closeMDl}
          onPress={() => {
            setShowModal(false);
          }}>
          <Image
            style={styles.closeBtn}
            source={require('../../assets/icons/close-white.png')}
          />
        </TouchableOpacity>
        <ImageViewer
          imageUrls={img}
          enableSwipeDown
          onSwipeDown={() => {
            setShowModal(false);
          }}
          onCancel={() => {
            setShowModal(false);
          }}
        />
      </Modal>
    </SafeAreaView>
  );
}

export default WelcomeScreen;
