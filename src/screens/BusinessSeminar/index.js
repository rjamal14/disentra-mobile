import React, {useEffect, useState} from 'react';
import {Text, Image} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from '../../screens/styles/BusinessSeminarStyle';
import LinearGradient from 'react-native-linear-gradient';
import {white, white1} from '../../constants/colors';
import Cf from '../../config';
import {Tab, Tabs} from 'native-base';
import GroupChat from './GroupChat';
import Seminar from './Seminar';

function BusinessSeminar(props) {
  const [banner, setBanner] = useState('');
  useEffect(() => {
    getBanner('talk');
  }, []);

  function getBanner(type) {
    fetch(Cf.base_url + Cf.get_banner + type, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        setBanner(responseJson.data[0].image.url);
      });
  }

  return (
    <LinearGradient colors={[white, white1]} style={styles.gradient}>
      <SafeAreaView style={styles.container}>
        <Image style={styles.logo} source={{uri: banner}} />
        <Tabs tabContainerStyle={styles.bg1}>
          <Tab
            tabStyle={styles.bg1}
            textStyle={styles.text}
            activeTabStyle={styles.activeSeminar}
            activeTextStyle={styles.activeTextSeminar}
            heading="SENTRAMINAR">
            <Seminar navigation={props.navigation} />
          </Tab>
          <Tab
            tabStyle={styles.bg1}
            textStyle={styles.text}
            activeTabStyle={styles.activeChat}
            activeTextStyle={styles.activeTextChat}
            heading="GROUP CHAT">
            <GroupChat navigation={props.navigation} />
          </Tab>
        </Tabs>
      </SafeAreaView>
    </LinearGradient>
  );
}

export default BusinessSeminar;
