import React, {Component} from 'react';
import {
  Image,
  Text,
  View,
  TouchableNativeFeedback,
  Linking,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import styles from '../screens/styles/ContactUsStyle';
import BankIcon from '../assets/icons/ic_bank.svg';
import TelephoneIcon from '../assets/icons/ic_telephone.svg';
import Mail from '../assets/icons/ic_mail_contact.svg';
export default class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEmailSend: false,
      phone: '02214049',
    };
  }

  render() {
    return (
      <SafeAreaView>
        <ScrollView>
          <Image
            style={styles.banner}
            source={require('../assets/images/banner.png')}
          />
          <View style={styles.containerBannerText}>
            <Text style={styles.textBannerHeader}>KONTAK KAMI</Text>
            <Text style={styles.textBannerContent}>
              Butuh bantuan? Segera hubungi kami untuk melayani Anda.
            </Text>
          </View>
          <View style={styles.contentContainer}>
            <View style={styles.cards}>
              <BankIcon />
              <View style={styles.cardsContent}>
                <Text style={styles.cardTitle}>Kantor Pusat Bank BJB</Text>
                <Text style={styles.cardContent}>
                  Menara Bank BJB{'\n'}Jalan Naripan No. 12-14{'\n'}Bandung -
                  40111
                </Text>
              </View>
            </View>
            <View style={styles.cards}>
              <Mail />
              <View style={styles.cardsContent}>
                <Text style={styles.cardTitle}>Email Pengaduan</Text>
                <TouchableOpacity
                  onPress={() => {
                    Linking.openURL(
                      'mailto:bjbcare@bankbjb.co.id?subject=&body=',
                    );
                  }}>
                  <Text style={styles.cardContentMail}>
                    bjbcare@bankbjb.co.id
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.cards}>
              <TelephoneIcon />
              <View style={styles.cardsContent}>
                <Text style={styles.cardTitle}>Kontak Bank BJB</Text>
                <Text style={styles.cardContent}>Telp. (14049)</Text>
                <TouchableNativeFeedback
                  onPress={() => {
                    Linking.openURL(`tel:${this.state.phone}`);
                  }}>
                  <View style={styles.callCenterButton}>
                    <Text style={styles.callCenterButtonText}>CALL CENTER</Text>
                  </View>
                </TouchableNativeFeedback>
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
