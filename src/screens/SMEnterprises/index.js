/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StatusBar,
  FlatList,
  RefreshControl,
  Image,
  ActivityIndicator,
} from 'react-native';
import styles from '../styles/SMEnterprisesStyle';
import InputText from '../../components/InputText';
import IconSearch from '../../assets/icons/ic_search.svg';
import {View, TouchableOpacity, Text} from 'react-native';
import {Card} from 'native-base';
import {blue2} from '../../constants/colors';
import axios from 'axios';
import config from '../../config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import actions from '../../actions';
import FilterCoimponent from './filter';
import {Button} from 'react-native-elements';
import FilterIcon from '../../assets/icons/ic_filter';

const uri = 'api/v1/mobile/';
const filter = 'umkm_markets?';

function Item(props) {
  const {value, navigation} = props;
  let index = value.images.findIndex((x) => x.is_thumbnail === true);
  return (
    <View>
      <Card style={styles.radiusCard}>
        <View style={styles.areaCardChat}>
          <View style={styles.coverChat}>
            <ActivityIndicator style={styles.loader} color="#165581" />
            <Image
              style={styles.imgCoverChat}
              resizeMode="cover"
              source={{
                uri: value.images[index].image.url,
              }}
            />
          </View>
          <View style={styles.titleChat}>
            <Text style={styles.titleCardChat}>{value.name}</Text>
            <Text style={styles.textDesc}>{value.business_field.name}</Text>
            <Text numberOfLines={3} style={styles.textDesc2}>
              {value.description}
            </Text>
          </View>
          <TouchableOpacity
            style={styles.btnRegisterChat}
            onPress={() => {
              navigation.navigate('detailSMEnterprises', {value});
            }}>
            <Text style={styles.titleRegistrasi}>LIHAT</Text>
          </TouchableOpacity>
        </View>
      </Card>
    </View>
  );
}

function Seminar(props) {
  const [token, setToken] = useState(null);
  const [search, setSearch] = useState('');
  const [datas, setDatas] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [BusinessField, setBusinessField] = useState('');
  const [id_city, setIdCity] = useState('');
  const [data_city, setDataCity] = useState('');
  const [banner, setBanner] = useState('');
  const [pagination, setPagination] = useState('');

  useEffect(() => {
    getBanner('umkm');
  }, []);

  function getBanner(type) {
    fetch(config.base_url + config.get_banner + type, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        setBanner(responseJson.data[0].image.url);
      });
  }

  const getToken = async () => {
    const value = await AsyncStorage.getItem('access_token');
    setToken(value);
  };
  getToken();
  useEffect(() => {
    if (token !== null) {
      getData();
    }
  }, [token]);

  function getData(Business_id = '', idcity = '', next = false) {
    axios
      .get(
        config.base_url +
          uri +
          filter +
          (search !== '' ? 'search=' + search : '') +
          (Business_id !== ''
            ? '&q[business_field_id_eq]=' + Business_id
            : '') +
          (idcity !== '' ? '&q[city_id_eq]=' + idcity : '') +
          '&page=' +
          (next ? pagination.next_page : 1),
        {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        },
      )
      .then((response) => {
        if (next) {
          setDatas([...datas, ...response.data.data]);
        } else {
          setDatas(response.data.data);
        }
        setPagination(response.data.pagination);
        setRefreshing(false);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          actions.removeToken();
          props.navigation.replace('Auth');
        }
      });
  }

  const renderItem = ({item}) => (
    <Item value={item} token={token} navigation={props.navigation} />
  );

  function onRefresh() {
    setRefreshing(true);
    getData();
  }

  function updateMasterState(attr, value) {
    setSearch(value);
  }

  return (
    <SafeAreaView
      style={styles.contentSeminar}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }>
      <StatusBar
        animated={true}
        backgroundColor={blue2}
        barStyle={'content-light'}
      />
      <FilterCoimponent
        modalVisible={modalVisible}
        BusinessFieldID={BusinessField}
        valueFilter={(id, id2, val) => {
          setBusinessField(id);
          setIdCity(id2);
          setDataCity(val);
          getData(id, id2);
        }}
        id_city={id_city}
        data_city={data_city}
        setModalVisible={(val) => {
          setModalVisible(val);
        }}
      />
      <Image style={styles.logo} source={{uri: banner}} />
      <View style={styles.searchContainer}>
        <Button
          icon={<FilterIcon />}
          title={'Saring'}
          type={'outlined'}
          buttonStyle={styles.filterButton}
          titleStyle={styles.cardContent}
          onPress={() => {
            setModalVisible(true);
          }}
        />
        <InputText
          type={'search'}
          placeholder={'Cari'}
          showMessage={''}
          value={search}
          attrName={'search'}
          message={''}
          updateMasterState={updateMasterState}
          icons={
            <IconSearch
              onPress={() => {
                getData(BusinessField, id_city);
              }}
            />
          }
        />
      </View>
      <View style={styles.divider} />
      {datas.length === 0 || token === null ? (
        <Text style={styles.textNodata}>Data tidak ditemukan.</Text>
      ) : (
        <View style={styles.areaCard}>
          <FlatList
            data={datas}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            onEndReached={() => {
              if (pagination !== null && pagination.next_page !== null) {
                getData('', '', true);
              }
            }}
            onEndReachedThreshold={7}
          />
        </View>
      )}
    </SafeAreaView>
  );
}

export default Seminar;
