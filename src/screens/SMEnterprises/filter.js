/* eslint-disable no-shadow */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Keyboard,
  FlatList,
} from 'react-native';
import {Header, Body, ListItem} from 'native-base';
import styles from '../styles/SMEnterprisesStyle';
import axios from 'axios';
import config from '../../config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import actions from '../../actions';
import InputText from '../../components/InputText';
import IconX from '../../assets/icons/x.svg';

const uri = 'api/v1/mobile/';
const filter = 'business_fields';

const filter_cities = 'cities/search_index?page=1&per_page=10&q[name_cont]=';

function WelcomeScreen(props) {
  const {
    modalVisible,
    setModalVisible,
    BusinessFieldID,
    valueFilter,
    id_city,
    data_city,
  } = props;
  const [token, setToken] = useState(null);
  const [search, setSearch] = useState('');
  const [datas, setDatas] = useState([]);
  const [BusinessField, setBusinessField] = useState('');
  const [cities, setCities] = useState([]);
  const [city_id, setCity_id] = useState('');
  const [keyboardUp, setKeyboardUp] = useState(false);

  const getToken = async () => {
    const value = await AsyncStorage.getItem('access_token');
    setToken(value);
  };
  getToken();
  useEffect(() => {
    if (token !== null) {
      getData();
    }
  }, [token]);

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
    };
  }, []);

  const _keyboardDidShow = () => {
    setKeyboardUp(true);
  };

  const _keyboardDidHide = () => {
    setKeyboardUp(false);
  };

  function getData() {
    axios
      .get(
        config.base_url +
          uri +
          filter +
          (search !== '' ? 'search=' + search : ''),
        {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        },
      )
      .then((response) => {
        setDatas(response.data.data);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          actions.removeToken();
          props.navigation.replace('Auth');
        }
      });
  }

  function getCities(val = '') {
    axios
      .get(config.base_url + uri + filter_cities + val, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((response) => {
        setCities(response.data.data);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          actions.removeToken();
          props.navigation.replace('Auth');
        }
      });
  }

  function Item(props) {
    const {value} = props;
    return (
      <ListItem
        onPress={() => {
          setSearch(value.name);
          setCity_id(value.id);
          setCities([]);
        }}>
        <Text>{value.name}</Text>
      </ListItem>
    );
  }

  const renderItem = ({item}) => (
    <Item value={item} token={token} navigation={props.navigation} />
  );

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setCities([]);
        setModalVisible(false);
      }}>
      <View style={styles.modalContainer} />
      <View style={styles.modalContentContainer}>
        <Header style={styles.header}>
          <Body style={styles.header2}>
            <View style={styles.saring}>
              <Text>Saring</Text>
            </View>
            <TouchableOpacity
              style={styles.close}
              onPress={() => {
                if (
                  BusinessFieldID === BusinessField &&
                  id_city === city_id &&
                  search === data_city
                ) {
                  setModalVisible(false);
                  setCities([]);
                } else {
                  Alert.alert(
                    'Perubahan belum diterapkan',
                    'Anda akan menerapkan perubahan?',
                    [
                      {
                        text: 'Tidak',
                        onPress: () => {
                          setBusinessField(BusinessFieldID);
                          setCities([]);
                          setModalVisible(false);
                        },
                        style: 'cancel',
                      },
                      {
                        text: 'Ya',
                        onPress: () => {
                          valueFilter(BusinessField, city_id, search);
                          setCities([]);
                          setModalVisible(false);
                        },
                      },
                    ],
                    {cancelable: false},
                  );
                }
              }}>
              <Image source={require('../../assets/icons/close.png')} />
            </TouchableOpacity>
          </Body>
        </Header>
        <ScrollView style={styles.modalContentContainer2}>
          <Text style={styles.filterHeader}>KATEGORI</Text>
          <View style={styles.filterItemContainer}>
            {datas.map((val) => {
              return (
                <TouchableOpacity
                  style={
                    BusinessField === val.id
                      ? styles.filterItemActive
                      : styles.filterItem
                  }
                  onPress={() => {
                    setBusinessField(val.id);
                  }}>
                  <Text style={styles.filterItemText}>{val.name}</Text>
                </TouchableOpacity>
              );
            })}
          </View>
          <Text style={styles.filterHeader}>KOTA/KABUPATEN</Text>
          <View style={styles.filterItemContainer}>
            <InputText
              type={'searchBisnis'}
              placeholder={'Cari'}
              showMessage={''}
              value={search}
              attrName={'search'}
              message={''}
              updateMasterState={(attr, val) => {
                getCities(val);
                setSearch(val);
              }}
              icons={
                <IconX
                  onPress={() => {
                    setSearch('');
                    setCity_id('');
                    setCities([]);
                  }}
                />
              }
            />
            {cities.length > 0 ? (
              <View style={styles.filterList}>
                <FlatList
                  data={cities}
                  renderItem={renderItem}
                  keyExtractor={(item) => item.id}
                />
              </View>
            ) : null}
          </View>
        </ScrollView>
        <View style={keyboardUp ? styles.actionHide : styles.action}>
          <TouchableOpacity
            style={styles.actionReset}
            onPress={() => {
              setModalVisible(false);
              valueFilter('');
              setCities([]);
              setBusinessField('');
              setBusinessField('');
            }}>
            <Text style={styles.textReset}>Reset</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.actionFilter}
            onPress={() => {
              setModalVisible(false);
              setCities([]);
              valueFilter(BusinessField, city_id, search);
            }}>
            <Text style={styles.textFilter}>Terapkan</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}

export default WelcomeScreen;
