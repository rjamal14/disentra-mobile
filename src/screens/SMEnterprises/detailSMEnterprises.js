import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  SafeAreaView,
  Modal,
} from 'react-native';
import styles from '../styles/SMEnterprisesStyle';
import {blue2} from '../../constants/colors';
import {Header, Left, Body, Right, Title} from 'native-base';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import ImageViewer from 'react-native-image-zoom-viewer';
const BannerWidth = Dimensions.get('window').width;

function WelcomeScreen(props) {
  const value = props.route.params.value;
  const [activeDot, setActiveDot] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const [indexImg, setIndexImg] = useState(null);
  let img = [];

  value.images
    .filter((o) => o.is_thumbnail === false)
    .map((item) => {
      img.push({url: item.image.url});
    });

  function renderPage({item, index}) {
    return (
      <TouchableOpacity
        onPress={() => {
          setShowModal(true);
          setIndexImg(index);
        }}>
        <Image
          style={styles.imgCoverDetail}
          resizeMode="cover"
          source={{
            uri: item.image.url,
          }}
        />
      </TouchableOpacity>
    );
  }
  const {navigation} = props;
  return (
    <SafeAreaView>
      <ScrollView>
        <Header style={styles.header} androidStatusBarColor={blue2}>
          <Left>
            <TouchableOpacity
              style={styles.backButton}
              onPress={() => {
                navigation.goBack();
              }}>
              <Image source={require('../../assets/icons/arrow-left.png')} />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title style={styles.textHeader}>Pasar UMKM</Title>
          </Body>
          <Right />
        </Header>
        {value.images.length > 0 ? (
          <View>
            <Carousel
              sliderWidth={BannerWidth}
              sliderHeight={BannerWidth}
              itemWidth={BannerWidth}
              data={value.images.filter((o) => o.is_thumbnail === false)}
              scrollEnabled={value.images.length - 1 === 1 ? false : true}
              disableScrollViewPanResponder
              renderItem={renderPage}
              hasParallaxImages={false}
              loop={false}
              onSnapToItem={(index) => {
                setActiveDot(index);
              }}
            />
            <Pagination
              dotsLength={
                value.images.filter((o) => o.is_thumbnail === false).length
              }
              activeDotIndex={activeDot}
              containerStyle={styles.containerStyle}
              dotStyle={styles.dotStyle}
              inactiveDotStyle={styles.inactiveDotStyle}
              inactiveDotOpacity={1}
              inactiveDotScale={0.6}
              activeOpacity={2}
            />
          </View>
        ) : null}
        <View style={styles.areaCard}>
          <View>
            <Text style={styles.titleCard}>{value.name}</Text>
          </View>
          <View style={styles.areaContent}>
            <Image source={require('../../assets/icons/business_field.png')} />
            <Text style={styles.textContent}>{value.business_field.name}</Text>
          </View>
          <View style={styles.areaContent}>
            <Image source={require('../../assets/icons/pin.png')} />
            <Text style={styles.textContent}>{value.city.name}</Text>
          </View>
          <View style={styles.areaContent}>
            <Image source={require('../../assets/icons/external-link.png')} />
            <Text style={styles.textContent}>{value.link}</Text>
          </View>
          <TouchableOpacity
            style={styles.btnRegisterDetail}
            onPress={() => {
              navigation.navigate('WebView', {
                link: value.link,
                header: 'Berita',
                title: value.name,
              });
            }}>
            <Text style={styles.titleRegistrasi}>KUNJUNGI WEBSITE</Text>
          </TouchableOpacity>
          <Text style={styles.textDesc}>DESKRIPSI</Text>
          <Text style={styles.textTheme}>
            {value.description === undefined ? '' : value.description}
          </Text>
        </View>
      </ScrollView>
      <Modal visible={showModal} transparent={true}>
        <TouchableOpacity
          style={styles.closeMDl}
          onPress={() => {
            setShowModal(false);
          }}>
          <Image
            style={styles.closeBtn}
            source={require('../../assets/icons/close-white.png')}
          />
        </TouchableOpacity>
        <ImageViewer
          imageUrls={img}
          index={indexImg}
          enableSwipeDown
          onSwipeDown={() => {
            setShowModal(false);
          }}
          onCancel={() => {
            setShowModal(false);
          }}
        />
      </Modal>
    </SafeAreaView>
  );
}

export default WelcomeScreen;
