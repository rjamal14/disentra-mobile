import React, {Component} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {SafeAreaView} from 'react-native-safe-area-context';
import {
  TouchableOpacity,
  Image,
  View,
  Text,
  ScrollView,
  RefreshControl,
  PermissionsAndroid,
} from 'react-native';
import {white, white1, blue2} from '../constants/colors';
import {Tab, Tabs, Header, Left, Right, Body, Title} from 'native-base';
import {Overlay} from 'react-native-elements';
import styles from '../screens/styles/MyAccountStyle';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import MapView, {Marker} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import axios from 'axios';
import VerifyIcon from '../assets/icons/ic_blue_check.svg';
import BasicIcon from '../assets/icons/ic_yellow_check.svg';
import LogoutIcon from '../assets/icons/ic_logout.svg';
import actions from '../actions';
import Cf from '../config';

const uri = 'api/v1/user';

export default class MyAccountScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: '',
      userProfile: '',
      refreshData: false,
      modalConfirm: false,
      modalPicker: false,
      userPhoto: '',
      identityImage: '',
      latitude: 0.0,
      longitude: 0.0,
      currentLat: 0.0,
      currentLong: 0.0,
    };
  }

  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.fetchUserData();
    });
    this.fetchUserData();
  }

  async _requestLocation() {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Izin Lokasi',
        message:
          'BJB Disentra perlu izin untuk mengakses layanan lokasi perangkat',
        buttonNegative: 'Tolak',
        buttonPositive: 'Izinkan',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
        interval: 10000,
        fastInterval: 5000,
      })
        .then((data) => {
          Geolocation.getCurrentPosition(
            (position) => {
              this.setState({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
              });
            },
            (error) => {},
            {enableHighAccuracy: false, timeout: 15000, maximumAge: 0},
          );
        })
        .catch((err) => {});
    } else {
    }
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  _onRefresh = () => {
    this.fetchUserData();
  };

  fetchUserData() {
    AsyncStorage.getItem('access_token').then((token) => {
      axios
        .get(Cf.base_url + uri, {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        })
        .then((res) => {
          if (res.data.data.user_profile == null){
            this.setState({
              userData: res.data.data,
              userPhoto: res.data.data.photo.url,
            });
            this._requestLocation();
          } else {
            this.setState({
              userData: res.data.data,
              userPhoto: res.data.data.photo.url,
              userProfile: res.data.data.user_profile,
              identityImage: res.data.data.user_profile.identity_photo.url,
            });
            if (res.data.data.user_profile.longitude === null) {
              this._requestLocation();
            } else {
              this.setState({
                latitude: res.data.data.user_profile.latitude,
                longitude: res.data.data.user_profile.longitude,
              });
            }
          }
        })
        .catch((error) => {
          if (error.response.status === 401) {
            actions.removeToken();
            this.props.navigation.replace('Auth');
          }
        });
    });
  }

  render() {
    return (
      <LinearGradient colors={[white, white1]} style={styles.gradient}>
        <SafeAreaView style={styles.container}>
          <Header style={styles.header} androidStatusBarColor={blue2}>
            <Left>
              <TouchableOpacity
                style={styles.backButton}
                onPress={() => {
                  this.props.navigation.goBack();
                }}>
                <Image source={require('../assets/icons/arrow-left.png')} />
              </TouchableOpacity>
            </Left>
            <Body>
              <Title style={styles.textHeader}>Akun Saya</Title>
            </Body>
            <Right />
          </Header>
          <Tabs tabContainerStyle={styles.bg1}>
            <Tab
              tabStyle={styles.bg1}
              textStyle={styles.text}
              activeTabStyle={styles.active}
              activeTextStyle={styles.activeText}
              heading="DATA DIRI">
              <ScrollView
                keyboardShouldPersistTaps="always"
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshData}
                    onRefresh={this._onRefresh}
                  />
                }>
                <View style={styles.infoContainer}>
                  <Text style={styles.textLabel2}>Foto Diri</Text>
                  {this.state.userPhoto === null ? (
                    <Image
                      source={require('../assets/images/avatar.png')}
                      style={styles.avatar}
                    />
                  ) : (
                    <Image
                      source={{uri: this.state.userPhoto}}
                      style={styles.avatar}
                    />
                  )}
                  <View style={styles.areaHeader}>
                    <Text style={styles.textContent}>
                      {this.state.userData.verified === true
                        ? 'Terverifikasi'
                        : 'Basic'}
                    </Text>
                    {this.state.userData.verified === true ? (
                      <VerifyIcon />
                    ) : (
                      <BasicIcon />
                    )}
                  </View>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate('RenewProfile', {
                        type: 'profile',
                      });
                    }}
                    style={styles.btnRenew}>
                    <Text style={styles.textSaying}>PERBAHARUI</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.infoContainer1}>
                  <View style={styles.separator}>
                    <Text style={styles.textLabel}>Nama Lengkap</Text>
                    <Text style={styles.textContent}>
                      {this.state.userData.name}
                    </Text>
                  </View>
                  <View style={styles.separator}>
                    <Text style={styles.textLabel}>Nomor Handphone</Text>
                    <Text style={styles.textContent}>
                      {this.state.userData.phone_number}
                    </Text>
                  </View>
                  <View style={styles.separator}>
                    <Text style={styles.textLabel}>
                      Nomor Rekening Bank BJB
                    </Text>
                    <Text style={styles.textContent}>
                      {this.state.userProfile === ''
                        ? '-'
                        : this.state.userProfile.account_number}
                    </Text>
                  </View>
                  <View style={styles.separator}>
                    <Text style={styles.textLabel}>Foto KTP</Text>
                    {this.state.identityImage === '' ? (
                      <Image
                        source={require('../assets/images/placeholder.png')}
                        style={styles.fotoKtp}
                      />
                    ) : (
                      <Image
                        source={{uri: this.state.identityImage}}
                        style={styles.fotoKtp}
                      />
                    )}
                  </View>
                  <View style={styles.separator}>
                    <Text style={styles.textLabel}>E-Mail</Text>
                    <Text style={styles.textContent}>
                      {this.state.userData.email}
                    </Text>
                  </View>
                </View>
                <View style={styles.infoContainer2}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate('RenewPassword');
                    }}
                    style={styles.btnOutlined}>
                    <Text style={styles.buttonOutlinedTitle}>
                      UBAH KATA SANDI
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({modalConfirm: true});
                    }}
                    style={styles.btnSolid}>
                    <Text style={styles.buttonSolidTittle}>KELUAR</Text>
                    <LogoutIcon />
                  </TouchableOpacity>
                </View>
              </ScrollView>
            </Tab>
            <Tab
              tabStyle={styles.bg1}
              textStyle={styles.text}
              activeTabStyle={styles.active2}
              activeTextStyle={styles.activeText2}
              heading="DATA USAHA">
              <ScrollView
                keyboardShouldPersistTaps="always"
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshData}
                    onRefresh={this._onRefresh}
                  />
                }>
                <View style={styles.infoContainer1}>
                  <View style={styles.separator}>
                    <Text style={styles.textLabel}>Nama Usaha</Text>
                    <Text style={styles.textContent}>
                      {this.state.userProfile === ''
                        ? '-'
                        : this.state.userProfile.bussiness_name}
                    </Text>
                  </View>
                  <View style={styles.separator}>
                    <Text style={styles.textLabel}>Derskripsi Usaha</Text>
                    <Text style={styles.textContent}>
                      {this.state.userProfile === ''
                        ? '-'
                        : this.state.userProfile.bussiness_description}
                    </Text>
                  </View>
                  <View style={styles.separator}>
                    <Text style={styles.textLabel}>Alamat</Text>
                    <Text style={styles.textContent}>
                      {this.state.userProfile === ''
                        ? '-'
                        : this.state.userProfile.bussiness_address}
                    </Text>
                  </View>
                  <View style={styles.separator}>
                    <Text style={styles.textLabel}>Lokasi Pada Peta</Text>
                    <MapView
                      style={styles.maps}
                      initialRegion={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: 0.005,
                        longitudeDelta: 0.005,
                      }}>
                      <Marker
                        coordinate={{
                          latitude:
                            this.state.latitude === null
                              ? 0.0
                              : this.state.latitude,
                          longitude:
                            this.state.longitude === null
                              ? 0.0
                              : this.state.longitude,
                        }}
                      />
                    </MapView>
                    <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate('RenewProfile', {
                          type: 'business',
                        });
                      }}
                      style={styles.btnRenew}>
                      <Text style={styles.textSaying}>PERBAHARUI</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </ScrollView>
            </Tab>
          </Tabs>
          <Overlay
            isVisible={this.state.modalConfirm}
            onBackdropPress={() => {
              this.setState({modalConfirm: false});
            }}>
            <View>
              <Text style={styles.textDesc}>Keluar dari akun?</Text>
              <View style={styles.areaHeader}>
                <TouchableOpacity
                  onPress={() => {
                    actions.removeToken();
                    this.props.navigation.replace('Auth');
                    this.setState({modalConfirm: false});
                  }}
                  style={styles.btnConfirmSolid}>
                  <Text style={styles.buttonSolidTittle}>Ya</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({modalConfirm: false});
                  }}
                  style={styles.btnConfirmOutline}>
                  <Text style={styles.buttonOutlinedTittle}>Batal</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Overlay>
        </SafeAreaView>
      </LinearGradient>
    );
  }
}
