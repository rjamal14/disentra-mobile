import React from 'react';
import {
  Image,
  Text,
  View,
  KeyboardAvoidingView,
  StatusBar,
  TouchableNativeFeedback,
  ActivityIndicator,
  ToastAndroid,
  PermissionsAndroid,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import Cf from '../config';
import styles from './styles/RegisterStyle';
import {white, white1, blue2} from '../constants/colors';
import InputText from '../components/InputText';
import LinearGradient from 'react-native-linear-gradient';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import Geolocation from 'react-native-geolocation-service';
import Geocoder from 'react-native-geocoding';
import AlertPro from 'react-native-alert-pro';
import _ from '../helpers/responsive';
import ft from '../constants/fonts';
import AutoComplete from "react-native-autocomplete-modal";
import AsyncStorage from '@react-native-async-storage/async-storage';


export default class BussinessRegistrationScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFocused: false,
      isError: false,
      isLoading: false,
      bussinessName: '',
      bussinessDescription:'',
      bussinessAddress:'',
      longitude:'',
      latitude:'',
      region: {
        latitude: -6.1753924,
        longitude: 106.8271528,
        latitudeDelta: 0.005,
        longitudeDelta: 0.005
      },
      isMapReady: false,
      regionChangeProgress: false,
      check: false,
      alamatedit:'',
      province:'',
      district:'',
      city:'',
      village:'',
      idProvince:'',
      idCity:'',
      idDistrict:'',
      idVillage:'',
      listProvince: [],
      listCity: [],
      listDistrict: [],
      listVillage: [],
      scroll: true,
      totalSteps: "",
      currentStep: ""
    };
  }


  componentDidMount() {
    this.init();
    Geocoder.init('AIzaSyC1qr-VmCGcWfp-hwV6NfzbAUKAKevaA-4');
    this.checkGPS();
    Geolocation.getCurrentPosition(
      (position) => {
        let region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.005,
          longitudeDelta: 0.005
        };
        this.setState({
          region: region,
          check: false,
          loading: false
        });
      },
      error => {
      },
      { enableHighAccuracy: false, timeout: 80000, maximumAge: 0 },
    );
    this.getProvince();
  }

  init = async () => {
    this.setState({
      bussinessName: await AsyncStorage.getItem('bussinessName'),
      bussinessDescription: await AsyncStorage.getItem('bussinessDescription'),
      bussinessAddress: await AsyncStorage.getItem('bussinessAddress'),
      province: await AsyncStorage.getItem('province'),
      idProvince: await AsyncStorage.getItem('idProvince'),
      city: await AsyncStorage.getItem('city'),
      idCity: await AsyncStorage.getItem('idCity'),
      district: await AsyncStorage.getItem('district'),
      idDistrict: await AsyncStorage.getItem('idDistrict'),
      village: await AsyncStorage.getItem('village'),
      idVillage: await AsyncStorage.getItem('idVillage'),
    })
  }

  async getValue(value){
    try {
      let a = await AsyncStorage.getItem(value);
      return a;
    } catch (error) {
      
    }
  }
  getProvince() {
    fetch(Cf.base_url + Cf.get_provinces_url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
      .then((res) => res.json())
      .then((response) => {
        if(response.data!== undefined){
          this.setState({
            listProvince: response.data
          })
        }else {
          this.notifyMessage('Gagal mendapatkan daftar provinsi, mohon cek koneksi internet Anda')
        }
      });
  }

  getCity(id) {
    fetch(Cf.base_url + Cf.get_cites_by_prov_id_url + id, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
      .then((res) => res.json())
      .then((response) => {
        if(response.data!== undefined){
          this.setState({
            listCity: response.data
          })
        }else {
          this.notifyMessage('Gagal mendapatkan daftar Kota/Kabupaten, mohon cek koneksi internet Anda')
        }
      });
  }

  getDistrict(id) {
    fetch(Cf.base_url + Cf.get_districts_by_city_id_url + id, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
      .then((res) => res.json())
      .then((response) => {
        if(response.data!== undefined){
          this.setState({
            listDistrict: response.data
          })
        }else {
          this.notifyMessage('Gagal mendapatkan daftar Kecamatan, mohon cek koneksi internet Anda')
        }
      });
  }

  getVillage(id) {
    fetch(Cf.base_url + Cf.get_villages_by_district_id_url + id, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
      .then((res) => res.json())
      .then((response) => {
        if(response.data!== undefined){
          this.setState({
            listVillage: response.data
          })
        }else {
          this.notifyMessage('Gagal mendapatkan daftar Desa/Kelurahan, mohon cek koneksi internet Anda')
        }
      });
  }

  checkGPS() {
    this.AlertPro.close();
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
      .then(data => {
        this.GPS();
      }).catch(err => {
        if (err.code == "ERR00") {
          this.AlertPro.open();
        }
        this.setState({
          check: false
        });
      });
  }

 async GPS() {
   const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: "Perizinan Lokasi Aplikasi DiSentra",
        message: "Aplikasi DiSentra Memerlukan Izin Layanan Lokasi",
        buttonNegative: "Batal",
        buttonPositive: "OK"
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      Geolocation.getCurrentPosition(
      (position) => {
        let region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.005,
          longitudeDelta: 0.005
        };
        this.setState({
          region: region,
          check: false,
          loading: false
        });
      },
      error => {
        this.GPSLow();
      },
      { enableHighAccuracy: true, timeout: 10000, maximumAge: 0 },
    );
    } else {
      this.notifyMessage("GPS Tidak diizinkan")
    }
  }

  GPSLow() {
    Geolocation.getCurrentPosition(
      (position) => {
        let region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.005,
          longitudeDelta: 0.005
        };
        this.setState({
          region: region,
          check: false,
          loading: false
        });
      },
      error => {
      },
      { enableHighAccuracy: false, timeout: 10000, maximumAge: 0 },
    );
  }

  onMapReady = () => {
    this.setState({ isMapReady: true });
  }

  onRegionChange(region) {
    this.setState({
      region,
      regionChangeProgress: true
    });
    Geocoder.from(region.latitude, region.longitude)
      .then(json => {
        this.setState({
          alamatedit:json.results[0].formatted_address,
        })
      })
      .catch(error => console.warn(error));
  }

  _updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };


  notifyMessage(msg) {
    if (Platform.OS === 'android') {
      ToastAndroid.show(msg, ToastAndroid.LONG)
    } else {
      AlertIOS.alert(msg);
    }
  }

  validate(){
    if (this.state.bussinessName === '') {
      this.notifyMessage('Nama Usaha Tidak Boleh Kosong');
    }
    if (this.state.bussinessDescription === '') {
      this.notifyMessage('Deskripsi Usaha Tidak Boleh Kosong');
    }
    if(this.state.bussinessAddress === ''){
      this.notifyMessage('Alamat Usaha Tidak Boleh Kosong')
    }
    if (this.state.province === '') {
      this.notifyMessage('Provinsi Tidak Boleh Kosong');
    }
    if (this.state.city === '') {
      this.notifyMessage('Kota/Kabupaten Tidak Boleh Kosong');
    }
    if (this.state.district === '') {
      this.notifyMessage('Kecamatan Tidak Boleh Kosong');
    }
    if (this.state.village === '') {
      this.notifyMessage('Desa/Kelurahan Tidak Boleh Kosong');
    }

    if(this.state.bussinessName === '' ||
    this.state.bussinessAddress === '' ||
    this.state.bussinessDescription === '' ||
    this.state.province === '' ||
    this.state.city === '' ||
    this.state.district === '' ||
    this.state.village === '' ){
    }else{
      this.finish();
    }
  }

  _handleRegister() {
    this.setState({isLoading: true});
    let form = new FormData()
    form = this.props.route.params.data
    form.append('user[user_profile_attributes][bussiness_name]', this.state.bussinessName);
    form.append('user[user_profile_attributes][bussiness_description]', this.state.bussinessDescription);
    form.append('user[user_profile_attributes][bussiness_address]', this.state.bussinessAddress);
    form.append('user[user_profile_attributes][longitude]', this.state.region.longitude);
    form.append('user[user_profile_attributes][latitude]', this.state.region.latitude);
    form.append('user[user_profile_attributes][province_id]', this.state.idProvince);
    form.append('user[user_profile_attributes][city_id]', this.state.idCity);
    form.append('user[user_profile_attributes][district_id]', this.state.idDistrict);
    form.append('user[user_profile_attributes][village_id]', this.state.idVillage);
    
    fetch(Cf.base_url + Cf.user_register_url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: form,
    })
      .then((res) => res.json())
      .then((response) => {
        if(response.code === 200){
          this.notifyMessage('Registrasi sukses, Silahkan Konfirmasi Email Anda Terlebih Dahulu untuk Melakukan Login');
          this.setState({isLoading: false});
          const keys = ['bussinessName', 'bussinessDescription', 'bussinessAddress', 'province', 'idProvince', 'city', 
          'idCity', 'district', 'idDistrict', 'village', 'idVillage'];
          this.clearForm(keys);
          this.props.navigation.navigate('Login');
        }else{
          this.notifyMessage(response.error);
          this.setState({
            isError: true,
              isLoading: false,
              errorMessage: response.error
          })
        }
      });
  }

  clearForm = async (keys) => {
    await AsyncStorage.multiRemove(keys);
  }
  

  renderFinish(){
    if(this.state.isLoading === true){
      return <View style={styles.rightBottomButton} >
      <ActivityIndicator color="#fff" size="small"/>
    </View>
    }else{
      return <TouchableNativeFeedback onPress={() => { this.validate() }}> 
      <View style={styles.rightBottomButton} >
        <Text style={styles.textRightButton}>Selesai</Text>
      </View>
    </TouchableNativeFeedback>
    }
  }

  finish = async ()=>{
    await AsyncStorage.setItem('bussinessName', this.state.bussinessName);
    await AsyncStorage.setItem('bussinessDescription', this.state.bussinessDescription);
    await AsyncStorage.setItem('bussinessAddress', this.state.bussinessAddress);
    await AsyncStorage.setItem('province', this.state.province);
    await AsyncStorage.setItem('idProvince', this.state.idProvince.toString());
    await AsyncStorage.setItem('city', this.state.city);
    await AsyncStorage.setItem('idCity', this.state.idCity.toString());
    await AsyncStorage.setItem('district', this.state.district);
    await AsyncStorage.setItem('idDistrict', this.state.idDistrict.toString());
    await AsyncStorage.setItem('village', this.state.village);
    await AsyncStorage.setItem('idVillage', this.state.idVillage.toString());
    this._handleRegister();
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container}>
        <StatusBar
          animated={true}
          backgroundColor={blue2}
          barStyle={'content-light'}
        />
        <LinearGradient colors={[white, white1]} style={styles.gradient}>
          <SafeAreaView style={styles.innerContainer}>
            <Image
              style={styles.handAbove}
              source={require('../assets/images/bg-hand-above.png')}
            />
            <View style={styles.headerContainer}>
              <Image
                style={styles.logoIcon1}
                source={require('../assets/images/logo-color.png')}
              />
              <Image
                style={styles.logoIcon}
                source={require('../assets/images/logo-bjb-color.png')}
              />
            </View>
            <View style={styles.dotContainer}>
              <View style={styles.dotActive}>
                <Text style={styles.textDotActive}>1</Text>
              </View>
              <View style={styles.underline1} />
              <View style={styles.dotActive}>
                <Text style={styles.textDotActive}>2</Text>
              </View>
            </View>
            <View style={styles.dotLabelContainer}>
              <View>
                <Text style={styles.textLabelInactive}>Data Diri</Text>
              </View>
              <View>
                <Text style={styles.textLabelActive}>Data Usaha</Text>
              </View>
            </View>
            <ScrollView>
            {this.state.isError ? (
              <Text style={styles.errorMessage}>{this.state.errorMessage}</Text>
            ) : null}
            <View>
              <Text style={styles.textLabelInput}>Nama Usaha</Text>
              <InputText
              type={'default'}
              placeholder={''}
              showMessage={''}
              value={this.state.bussinessName}
              attrName={'bussinessName'}
              updateMasterState={this._updateMasterState}
              message={''}
            />
            </View>
            <View>
              <Text style={styles.textLabelInput}>Deskripsi Usaha</Text>
              <TextInput
              multiline
              numberOfLines={4}
              style={styles.inputContainerStyle}
              value={this.state.bussinessDescription}
              onChangeText={(text) => this.setState({bussinessDescription:text})}
            />
            </View>
            <View>
              <Text style={styles.textLabelInput}>Alamat</Text>
              <TextInput
              multiline
              numberOfLines={3}
              style={styles.inputContainerStyle}
              value={this.state.bussinessAddress}
              onChangeText={(text) => this.setState({bussinessAddress:text})}
            />
            </View>
            <Text style={styles.textLabelInput}>Provinsi</Text>
            <AutoComplete
              onSelect={data => {
                  this.getCity(data.id);
                  this.setState({
                  province: data.name,
                  idProvince: data.id
                });
              }}
              dataSource={this.state.listProvince}
              textLabel={this.state.province}
              style={styles.autocomplete}
              searchPlaceholder='Pilih Provinsi'
              cancelText="Close"
              textColor={blue2}
              searchField="name"/>
            <Text style={styles.textLabelInput}>Kota/Kabupaten</Text>
            <AutoComplete
              disable={!this.state.province}
              disableMsg={"Harap Pilih Provinsi"}
              onSelect={data => {
                  this.getDistrict(data.id);
                  this.setState({
                  city: data.name,
                  idCity: data.id
                });
              }}
              dataSource={this.state.listCity}
              textLabel={this.state.city}
              style={styles.autocomplete}
              searchPlaceholder='Pilih Kota/Kabupaten'
              cancelText="Close"
              textColor={blue2}
              searchField="name"/>
            <Text style={styles.textLabelInput}>Kecamatan</Text>
            <AutoComplete
              disable={!this.state.city}
              disableMsg={"Harap Pilih Kota/Kabupaten"}
              onSelect={data => {
                  this.getVillage(data.id);
                  this.setState({
                  district: data.name,
                  idDistrict: data.id
                });
              }}
              dataSource={this.state.listDistrict}
              textLabel={this.state.district}
              style={styles.autocomplete}
              searchPlaceholder='Pilih Kecamatan'
              cancelText="Close"
              textColor={blue2}
              searchField="name"/>
            <Text style={styles.textLabelInput}>Kelurahan</Text>
            <AutoComplete
              disable={!this.state.district}
              disableMsg={"Harap Pilih Kecamatan"}
              onSelect={data => {
                  this.setState({
                  village: data.name,
                  idVillage: data.id
                });
              }}
              dataSource={this.state.listVillage}
              textLabel={this.state.village}
              style={styles.autocomplete}
              searchPlaceholder='Pilih Desa/Kelurahan'
              cancelText="Close"
              textColor={blue2}
              searchField="name"/>
            <View>
            <Text style={styles.textLabelInput}>Pilih Lokasi Pada Peta</Text>
            <Text style={styles.textLabelInput2}>* Tap satu kali pada maps untuk memindahkan lokasi</Text>
            <MapView
              provider={PROVIDER_GOOGLE}
              style={styles.map}
              onPress={(e) => {
                let region = {
                  latitude: e.nativeEvent.coordinate.latitude,
                  longitude: e.nativeEvent.coordinate.longitude,
                  latitudeDelta: 0.005,
                  longitudeDelta: 0.005
                }
                this.onRegionChange(region);

              }}
              region={this.state.region}
              initialRegion={this.state.region}
            >
              <Marker
                draggable
                coordinate={{
                  latitude: this.state.region.latitude,
                  longitude: this.state.region.longitude,
                }}
                title={"Lokasi Anda"}
                onDragEnd={(e) => {
                  let region = {
                    latitude: e.nativeEvent.coordinate.latitude,
                    longitude: e.nativeEvent.coordinate.longitude,
                    latitudeDelta: 0.005,
                    longitudeDelta: 0.005
                  }
                  this.onRegionChange(region);

                }}
              >
                <Image
                  source={require("../assets/icons/pin_map.png")}
                  style={{ width: _.wp(7), height: _.wp(7) }}
                />
              </Marker>
            </MapView>
            </View>
            </ScrollView>
            <View style={styles.bottomLayout}>
            <TouchableNativeFeedback onPress={() => { this.props.navigation.goBack(); }}> 
              <View style={styles.leftBottomButton} >
                <Text style={styles.textLeftButton}>Kembali</Text>
              </View>
            </TouchableNativeFeedback>
            {this.renderFinish()}
            </View>
          </SafeAreaView>
        </LinearGradient>
        <AlertPro
          ref={ref => {
            this.AlertPro = ref;
          }}
          onConfirm={() => this.checkGPS()}
          title="Peringatan!"
          message="Anda harus mengaktifkan GPS terlebih dahulu"
          textConfirm="OK"
          closeOnPressMask={false}
          showCancel={false}
          customStyles={{
            container: {
              shadowOpacity: 0.1,
              shadowRadius: 10
            },
            buttonCancel: {
              backgroundColor: "#FFFFFF",
              borderWidth: 1,
              borderColor: blue2
            },
            buttonConfirm: {
              backgroundColor: white
            },
            textCancel: {
              fontFamily: ft.mur,
              color: blue2
            },
            textConfirm: {
              fontFamily: ft.mur,
              color: blue2,
            },
            title: {
              fontFamily: ft.mur,
              color: blue2
            },
            message: {
              fontFamily: ft.mur,
              color: blue2,
              fontSize: _.fz(8),
            }
          }}
        />
      </KeyboardAvoidingView>
    );
  }
}
