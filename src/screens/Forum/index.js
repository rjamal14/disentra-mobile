import React from 'react';
import {View, Text, Image} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from '../../screens/styles/ForumStyle';
import LinearGradient from 'react-native-linear-gradient';
import {white, white1} from '../../constants/colors';
import {Header, Body, Button} from 'native-base';
import Logo from '../../assets/images/logo-disentra-color.svg';

function WelcomeScreen(props) {
  return (
    <LinearGradient colors={[white, white1]} style={styles.gradient}>
      <Header style={styles.header}>
        <Body>
          <View style={styles.konten}>
            <Logo height={50} width={100} />
          </View>
        </Body>
      </Header>
      <SafeAreaView style={styles.container}>
        <View style={styles.welcomeMessage}>
          <Image
            style={styles.logo}
            source={require('../../assets/images/optimation.png')}
          />
        </View>
        <Text style={styles.textContent}>Fitur Ini Sedang Dioptimasi</Text>
        <Text style={styles.textContent2}>
          Tunggu notifikasi dari kami, ya.
        </Text>
      </SafeAreaView>
    </LinearGradient>
  );
}

export default WelcomeScreen;
