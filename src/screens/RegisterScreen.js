import React from 'react';
import {
  Image,
  Text,
  View,
  KeyboardAvoidingView,
  StatusBar,
  TouchableNativeFeedback,
  ActivityIndicator,
  Alert,
  TextInput,
  Modal,
  FlatList,
  TouchableOpacity,
  PermissionsAndroid,
} from 'react-native';
import styles from '../screens/styles/RegisterStyle';
import {white, white1, blue2} from '../constants/colors';
import InputText from '../components/InputText';
import LinearGradient from 'react-native-linear-gradient';
import {regEmail} from '../constants/commons';
import CameraIcon from '../assets/icons/ic_camera.svg';
import {ScrollView} from 'react-native-gesture-handler';
import {Overlay} from 'react-native-elements';
import ViewPager from '@react-native-community/viewpager';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import Geocoder from 'react-native-geocoding';
import Geolocation from 'react-native-geolocation-service';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import SearchIcon from '../assets/icons/ic_search';
import ImageSuccess from '../assets/images/success-check.svg';
import Icon from 'react-native-vector-icons/FontAwesome';
import Cf from '../config';
import _ from '../helpers/responsive';

export default class RegisterScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFocused: false,
      isError: false,
      errorMessage: '',
      errorMessage1: '',
      errorAvatar: '',
      errorID: '',
      name: '',
      messageName: '',
      isErrorName: false,
      showMessageName: false,
      phone: '',
      messagePhone: '',
      isErrorPhone: false,
      showMessagePhone: false,
      accountNumber: '',
      messageAccountNumber: '',
      isErrorAccountNumber: false,
      showMessageAccountNumber: false,
      email: '',
      messageEmail: '',
      isErrorEmail: false,
      showMessageEmail: false,
      password: '',
      messagePassword: '',
      isErrorPassword: false,
      showMessagePassword: false,
      passwordConfirmation: '',
      messagePasswordConfirmation: '',
      isErrorPasswordConfirmation: false,
      showMessagePasswordConfirmation: false,
      avatarSource: null,
      pic: null,
      idData: null,
      idImage: null,
      idPath: null,
      idSource: null,
      idUri: null,
      data: null,
      avatarUri: null,
      form: undefined,
      success: false,
      modalAlert: false,
      modalProvince: false,
      modalCity: false,
      modalDistrict: false,
      modalVillage: false,
      businessName: '',
      messageBusinessName: '',
      showMessageBusinessName: false,
      isErrorBusinessName: false,
      businessDescription: '',
      messageBusinessDescription: '',
      businessAddress: '',
      messageBusinessAddress: '',
      showMessageBusinessAddress: false,
      region: {
        latitude: 0.0,
        longitude: 0.0,
        latitudeDelta: 0.05,
        longitudeDelta: 0.05,
      },
      errorProvince: '',
      errorCity: '',
      errorDistrict: '',
      errorVillage: '',
      current: 0,
      page: 0,
      searchQuery: '',
      provinceList: [],
      cityList: [],
      districtList: [],
      villageList: [],
      province: {
        id: '',
        name: '',
      },
      city: {
        id: '',
        name: '',
      },
      district: {
        id: '',
        name: '',
      },
      village: {
        id: '',
        name: '',
      },
    };
    this.ScrollView = React.createRef();
    this.ScrollView1 = React.createRef();
    this.viewPager = React.createRef();
  }

  pickImage() {
    let opt = {
      mediaType: 'photo',
      quality: 0.25,
    };
    launchImageLibrary(opt, (response) => {
      let source = {uri: response.uri};
      this.setState({
        avatarSource: source,
        pic: response.data,
        avatarUri: response.uri,
      });
    });
  }

  takeIdPhoto() {
    let opt = {
      mediaType: 'photo',
      quality: 0.25,
    };
    launchCamera(opt, (response) => {
      let source = {uri: response.uri};
      this.setState({
        idPath: response,
        idData: response.data,
        idSource: source,
        idUri: response.uri,
      });
    });
  }

  async checkGPS() {
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
      interval: 10000,
      fastInterval: 5000,
    })
      .then((data) => {
        PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        ).then((granted) => {
          if (granted) {
            this.GPS();
          }
        });
      })
      .catch((err) => {
        if (err.code === 'ERR00') {
        }
      });
  }

  GPS() {
    Geolocation.getCurrentPosition(
      (position) => {
        let region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.005,
          longitudeDelta: 0.005,
        };
        this.setState({
          region: region,
        });
      },
      (_error) => {},
      {enableHighAccuracy: false, timeout: 10000, maximumAge: 0},
    );
  }

  renderId() {
    if (this.state.idSource == null) {
      return (
        <View style={styles.idPlaceholder}>
          <CameraIcon style={styles.cameraIcon} />
        </View>
      );
    } else {
      return <Image source={this.state.idSource} style={styles.idImage} />;
    }
  }

  componentDidMount() {
    this.searchAddress('Provinsi');
    Geocoder.init('AIzaSyBoB9RDDPJ4SA80pEamYqQp5UaTez87qbc');
    this.checkGPS();
  }

  onRegionChange(region) {
    this.setState({
      region,
    });
  }

  searchAddress(type) {
    let selector = '';
    if (type === 'Provinsi') {
      selector = Cf.get_provinces_url;
    } else if (type === 'Kota') {
      selector = Cf.get_cites_by_prov_id_url + this.state.province.id;
    } else if (type === 'Kecamatan') {
      selector = Cf.get_districts_by_city_id_url + this.state.city.id;
    } else if (type === 'Kelurahan') {
      selector = Cf.get_villages_by_district_id_url + this.state.district.id;
    }
    fetch(Cf.base_url + selector, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((response) => {
        if (response.data !== undefined) {
          if (type === 'Provinsi') {
            this.setState({
              provinceList: response.data,
            });
          } else if (type === 'Kota') {
            this.setState({
              cityList: response.data,
            });
          } else if (type === 'Kecamatan') {
            this.setState({
              districtList: response.data,
            });
          } else if (type === 'Kelurahan') {
            this.setState({
              villageList: response.data,
            });
          }
        }
      })
      .catch((error) => {});
  }

  _filterData(data, query) {
    if (query === '') {
      return data;
    } else {
      const filtered = data.filter((entry) =>
        Object.values(entry).some(
          (val) => typeof val === 'string' && val.includes(query.toUpperCase()),
        ),
      );
      return filtered;
    }
  }

  _renderAddressModal(type) {
    let datas = [];
    if (type === 'Provinsi') {
      datas = this.state.provinceList;
      return (
        <Modal
          animationType="slide"
          visible={this.state.modalProvince}
          onClose={() => {
            this.setState({searchQuery: ''});
          }}>
          <View style={styles.container}>
            <View style={styles.searchContainer}>
              <TouchableOpacity
                style={styles.closeButton}
                onPress={() => {
                  this.setState({modalProvince: false, searchQuery: ''});
                }}>
                <Icon name="close" size={30} color={blue2} />
              </TouchableOpacity>
              <InputText
                type={'searchMed'}
                placeholder={'Cari ' + type}
                showMessage={''}
                attrName={'searchQuery'}
                value={this.state.searchQuery}
                updateMasterState={this._updateMasterState}
                message={{
                  type: '',
                  text: '',
                }}
                icons={<SearchIcon />}
              />
            </View>
            <FlatList
              data={this._filterData(datas, this.state.searchQuery)}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      province: {
                        id: item.id,
                        name: item.name,
                      },
                      city: {
                        id: '',
                        name: '',
                      },
                      district: {
                        id: '',
                        name: '',
                      },
                      village: {
                        id: '',
                        name: '',
                      },
                      searchQuery: '',
                      modalProvince: false,
                    });
                  }}
                  style={styles.listItem}
                  key={item.id.toString()}>
                  <Text style={styles.listItemText}>{item.name}</Text>
                </TouchableOpacity>
              )}
              keyExtractor={(item) => item.id.toString()}
            />
          </View>
        </Modal>
      );
    } else if (type === 'Kota') {
      datas = this.state.cityList;
      return (
        <Modal
          animationType="slide"
          visible={this.state.modalCity}
          onClose={() => {
            this.setState({searchQuery: ''});
          }}>
          <View style={styles.container}>
            <View style={styles.searchContainer}>
              <TouchableOpacity
                style={styles.closeButton}
                onPress={() => {
                  this.setState({modalCity: false, searchQuery: ''});
                }}>
                <Icon name="close" size={30} color={blue2} />
              </TouchableOpacity>
              <InputText
                type={'searchMed'}
                placeholder={'Cari ' + type}
                showMessage={''}
                attrName={'searchQuery'}
                value={this.state.searchQuery}
                updateMasterState={this._updateMasterState}
                message={{
                  type: '',
                  text: '',
                }}
                icons={<SearchIcon />}
              />
            </View>
            <FlatList
              data={this._filterData(datas, this.state.searchQuery)}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      city: {
                        id: item.id,
                        name: item.name,
                      },
                      district: {
                        id: '',
                        name: '',
                      },
                      village: {
                        id: '',
                        name: '',
                      },
                      searchQuery: '',
                      modalCity: false,
                    });
                  }}
                  style={styles.listItem}
                  key={item.id.toString()}>
                  <Text style={styles.listItemText}>{item.name}</Text>
                </TouchableOpacity>
              )}
              keyExtractor={(item) => item.id.toString()}
            />
          </View>
        </Modal>
      );
    } else if (type === 'Kecamatan') {
      datas = this.state.districtList;
      return (
        <Modal
          animationType="slide"
          visible={this.state.modalDistrict}
          onClose={() => {
            this.setState({searchQuery: ''});
          }}>
          <View style={styles.container}>
            <View style={styles.searchContainer}>
              <TouchableOpacity
                style={styles.closeButton}
                onPress={() => {
                  this.setState({modalDistrict: false, searchQuery: ''});
                }}>
                <Icon name="close" size={30} color={blue2} />
              </TouchableOpacity>
              <InputText
                type={'searchMed'}
                placeholder={'Cari ' + type}
                showMessage={''}
                attrName={'searchQuery'}
                value={this.state.searchQuery}
                updateMasterState={this._updateMasterState}
                message={{
                  type: '',
                  text: '',
                }}
                icons={<SearchIcon />}
              />
            </View>
            <FlatList
              data={this._filterData(datas, this.state.searchQuery)}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      district: {
                        id: item.id,
                        name: item.name,
                      },
                      village: {
                        id: '',
                        name: '',
                      },
                      searchQuery: '',
                      modalDistrict: false,
                    });
                  }}
                  style={styles.listItem}
                  key={item.id.toString()}>
                  <Text style={styles.listItemText}>{item.name}</Text>
                </TouchableOpacity>
              )}
              keyExtractor={(item) => item.id.toString()}
            />
          </View>
        </Modal>
      );
    } else if (type === 'Kelurahan') {
      datas = this.state.villageList;
      return (
        <Modal
          animationType="slide"
          visible={this.state.modalVillage}
          onClose={() => {
            this.setState({searchQuery: ''});
          }}>
          <View style={styles.container}>
            <View style={styles.searchContainer}>
              <TouchableOpacity
                style={styles.closeButton}
                onPress={() => {
                  this.setState({modalVillage: false, searchQuery: ''});
                }}>
                <Icon name="close" size={30} color={blue2} />
              </TouchableOpacity>
              <InputText
                type={'searchMed'}
                placeholder={'Cari ' + type}
                showMessage={''}
                attrName={'searchQuery'}
                value={this.state.searchQuery}
                updateMasterState={this._updateMasterState}
                message={{
                  type: '',
                  text: '',
                }}
                icons={<SearchIcon />}
              />
            </View>
            <FlatList
              data={this._filterData(datas, this.state.searchQuery)}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      village: {
                        id: item.id,
                        name: item.name,
                      },
                      searchQuery: '',
                      modalVillage: false,
                    });
                  }}
                  style={styles.listItem}
                  key={item.id.toString()}>
                  <Text style={styles.listItemText}>{item.name}</Text>
                </TouchableOpacity>
              )}
              keyExtractor={(item) => item.id.toString()}
            />
          </View>
        </Modal>
      );
    }
  }

  _updateMasterState = (attrName, value) => {
    if (attrName === 'name' && this.state.isErrorName) {
      this.setState({
        isErrorName: false,
        showMessageName: false,
      });
    }
    if (attrName === 'phone' && this.state.isErrorPhone) {
      this.setState({
        isErrorPhone: false,
        showMessagePhone: false,
      });
    }
    if (attrName === 'accountNumber' && this.state.isErrorAccountNumber) {
      this.setState({
        isErrorAccountNumber: false,
        showMessageAccountNumber: false,
      });
    }
    if (attrName === 'email' && this.state.isErrorEmail) {
      this.setState({
        isErrorEmail: false,
        showMessageEmail: false,
      });
    }
    if (attrName === 'password' && this.state.isErrorPassword) {
      this.setState({
        isErrorPassword: false,
        showMessagePassword: false,
      });
    }
    if (
      attrName === 'passwordConfirmation' &&
      this.state.isErrorPasswordConfirmation
    ) {
      this.setState({
        isErrorPasswordConfirmation: false,
        showMessagePasswordConfirmation: false,
      });
    }
    this.setState({[attrName]: value});
  };

  _handleRegister(data) {
    this.setState({modalAlert: true});
    fetch(Cf.base_url + Cf.user_register_url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: data,
    })
      .then((res) => res.json())
      .then((response) => {
        if (response.code === 200) {
          this.setState({success: true});
          setTimeout(() => {
            this.setState({modalAlert: false});
            this.props.navigation.navigate('Login');
          }, 3000);
        } else {
          this.setState({
            isError: true,
            isLoading: false,
            errorMessage: response.error[0],
            modalAlert: false,
          });
          if (this.state.current === 1) {
            this.move(-1);
          }
          this.ScrollView.current.scrollTo({y: 0, animated: true});
        }
      });
  }

  confirmationStep() {
    Alert.alert(
      'Lanjut isi Data Usaha?',
      '',
      [
        {
          text: 'Tidak',
          onPress: () => {
            let form = new FormData();
            form.append('user[email]', this.state.email);
            form.append('user[password]', this.state.password);
            form.append(
              'user[password_confirmation]',
              this.state.passwordConfirmation,
            );
            form.append('user[name]', this.state.name);
            form.append('user[phone_number]', this.state.phone);
            form.append('user[user_type]', 'mobile');
            form.append('user[photo]', {
              uri: this.state.avatarUri,
              type: 'image/jpeg',
              name: this.state.name + Date.now() + 'ava.jpeg',
            });
            form.append(
              'user[user_profile_attributes][account_number]',
              this.state.accountNumber,
            );
            form.append('user[user_profile_attributes][identity_photo]', {
              uri: this.state.idUri,
              type: 'image/jpeg',
              name: this.state.name + Date.now() + 'id.jpeg',
            });
            this._handleRegister(form);
          },
          style: 'cancel',
        },
        {
          text: 'Ya',
          onPress: () => {
            this.move(1);
          },
        },
      ],
      {cancelable: false},
    );
  }

  validate() {
    //Password Confirmation Validation
    if (this.state.passwordConfirmation === '') {
      this.setState({
        isErrorPasswordConfirmation: true,
        showMessagePasswordConfirmation: true,
        messagePasswordConfirmation: 'Konfirmasi Password Tidak Boleh Kosong',
      });
      this.ScrollView.current.scrollTo({y: 900, animated: true});
    } else {
      this.setState({
        isErrorPasswordConfirmation: false,
        showMessagePasswordConfirmation: false,
      });
    }

    //Password Validation
    if (this.state.password === '') {
      this.setState({
        isErrorPassword: true,
        showMessagePassword: true,
        messagePassword: 'Password Tidak Boleh Kosong',
      });
      this.ScrollView.current.scrollTo({y: 800, animated: true});
    } else {
      this.setState({
        isErrorPassword: false,
        showMessagePassword: false,
      });
    }

    //Email Validation
    if (this.state.email === '') {
      this.setState({
        isErrorEmail: true,
        showMessageEmail: true,
        messageEmail: 'Email Tidak Boleh Kosong',
      });
      this.ScrollView.current.scrollTo({y: 700, animated: true});
    } else if (!regEmail.test(this.state.email)) {
      this.setState({
        isErrorEmail: true,
        showMessageEmail: true,
        messageEmail: 'Email Tidak Valid',
      });
      this.ScrollView.current.scrollTo({y: 700, animated: true});
    } else {
      this.setState({
        isErrorEmail: false,
        showMessageEmail: false,
      });
    }

    //ID Image Validation
    if (this.state.idUri == null) {
      this.setState({errorID: 'Pilih Foto KTP terlebih dahulu'});
      this.ScrollView.current.scrollTo({y: 482, animated: true});
    } else {
      this.setState({errorID: ''});
    }

    //Account Number Validation
    if (this.state.accountNumber === '') {
      this.setState({
        isErrorAccountNumber: true,
        showMessageAccountNumber: true,
        messageAccountNumber: 'No Rekening Tidak Boleh Kosong',
      });
      this.ScrollView.current.scrollTo({y: 383, animated: true});
    } else if (this.state.accountNumber.length < 13) {
      this.setState({
        isErrorAccountNumber: true,
        showMessageAccountNumber: true,
        messageAccountNumber: 'No Rekening Tidak Sesuai, Minimal 13 Digit',
      });
      this.ScrollView.current.scrollTo({y: 383, animated: true});
    } else {
      this.setState({
        isErrorAccountNumber: false,
        showMessageAccountNumber: false,
      });
    }

    //Phone field validation
    if (this.state.phone === '') {
      this.setState({
        isErrorPhone: true,
        showMessagePhone: true,
        messagePhone: 'No Handphone Tidak Boleh Kosong',
      });
      this.ScrollView.current.scrollTo({y: 283, animated: true});
    } else {
      this.setState({
        isErrorPhone: false,
        showMessagePhone: false,
      });
    }

    //Name field validation
    if (this.state.name === '') {
      this.setState({
        isErrorName: true,
        showMessageName: true,
        messageName: 'Nama Lengkap Tidak Boleh Kosong',
      });
      this.ScrollView.current.scrollTo({y: 184, animated: true});
    } else {
      this.setState({
        isErrorName: false,
        showMessageName: false,
      });
    }

    //Avatar Image Validation
    if (this.state.avatarSource == null) {
      this.setState({errorAvatar: 'Pilih Foto Profil terlebih dahulu'});
      this.ScrollView.current.scrollTo({y: 0, animated: true});
    } else {
      this.setState({errorAvatar: ''});
    }
    if (
      this.state.name === '' ||
      this.state.phone === '' ||
      this.state.accountNumber === '' ||
      this.state.accountNumber.length < 13 ||
      this.state.email === '' ||
      this.state.password === '' ||
      this.state.passwordConfirmation === '' ||
      this.state.avatarSource === null ||
      this.state.idUri === null ||
      !regEmail.test(this.state.email)
    ) {
    } else {
      this.confirmationStep();
    }
  }

  validate2() {
    //Village Validation
    if (this.state.village.name === '') {
      this.setState({
        errorVillage: 'Pilih Kelurahan Terlebih Dahulu',
      });
      this.ScrollView1.current.scrollTo({y: 673, animated: true});
    } else {
      this.setState({
        errorVillage: '',
      });
    }

    //District Validation
    if (this.state.district.name === '') {
      this.ScrollView1.current.scrollTo({y: 582, animated: true});
      this.setState({
        errorDistrict: 'Pilih District Terlebih Dahulu',
      });
    } else {
      this.setState({
        errorDistrict: '',
      });
    }

    //City
    if (this.state.city.name === '') {
      this.setState({errorCity: 'Pilih Kota Terlebih Dahulu'});
      this.ScrollView1.current.scrollTo({y: 492, animated: true});
    } else {
      this.setState({errorCity: ''});
    }

    //Province
    if (this.state.province.name === '') {
      this.ScrollView1.current.scrollTo({y: 401, animated: true});
      this.setState({
        errorProvince: 'Pilih Provinsi Terlebih Dahulu',
      });
    } else {
      this.setState({
        errorProvince: '',
      });
    }

    //Alamat validation
    if (this.state.businessAddress === '') {
      this.setState({
        messageBusinessAddress: 'Alamat Usaha Tidak Boleh Kosong',
      });
      this.ScrollView1.current.scrollTo({y: 270, animated: true});
    } else {
      this.setState({
        messageBusinessAddress: '',
      });
    }

    //Description validation
    if (this.state.businessDescription === '') {
      this.setState({
        messageBusinessDescription: 'Deskripsi Usaha Tidak Boleh Kosong',
      });
      this.ScrollView1.current.scrollTo({y: 103, animated: true});
    } else {
      this.setState({
        messageBusinessDescription: '',
      });
    }

    //Usaha Validation
    if (this.state.businessName === '') {
      this.setState({
        messageBusinessName: 'Nama Usaha Tidak Boleh Kosong',
        isErrorBusinessName: true,
        showMessageBusinessName: true,
      });
      this.ScrollView1.current.scrollTo({y: 0, animated: true});
    } else {
      this.setState({messageBusinessName: '', showMessageBusinessName: false});
    }
    if (
      this.state.businessName === '' ||
      this.state.businessAddress === '' ||
      this.state.businessDescription === '' ||
      this.state.province.name === '' ||
      this.state.city.name === '' ||
      this.state.district.name === '' ||
      this.state.village.name === '' ||
      this.state.region.latitude === 0.0 ||
      this.state.region.longitude === 0.0
    ) {
    } else {
      let form = new FormData();
      form.append('user[email]', this.state.email);
      form.append('user[password]', this.state.password);
      form.append(
        'user[password_confirmation]',
        this.state.passwordConfirmation,
      );
      form.append('user[name]', this.state.name);
      form.append('user[phone_number]', this.state.phone);
      form.append('user[user_type]', 'mobile');
      form.append('user[photo]', {
        uri: this.state.avatarUri,
        type: 'image/jpeg',
        name: this.state.name + Date.now() + 'ava.jpeg',
      });
      form.append(
        'user[user_profile_attributes][account_number]',
        this.state.accountNumber,
      );
      form.append('user[user_profile_attributes][identity_photo]', {
        uri: this.state.idUri,
        type: 'image/jpeg',
        name: this.state.name + Date.now() + 'id.jpeg',
      });
      form.append(
        'user[user_profile_attributes][bussiness_name]',
        this.state.businessName,
      );
      form.append(
        'user[user_profile_attributes][bussiness_description]',
        this.state.businessDescription,
      );
      form.append(
        'user[user_profile_attributes][bussiness_address]',
        this.state.businessAddress,
      );
      form.append(
        'user[user_profile_attributes][longitude]',
        this.state.region.longitude,
      );
      form.append(
        'user[user_profile_attributes][latitude]',
        this.state.region.latitude,
      );
      form.append(
        'user[user_profile_attributes][province_id]',
        this.state.province.id,
      );
      form.append('user[user_profile_attributes][city_id]', this.state.city.id);
      form.append(
        'user[user_profile_attributes][district_id]',
        this.state.district.id,
      );
      form.append(
        'user[user_profile_attributes][village_id]',
        this.state.village.id,
      );
      this._handleRegister(form);
    }
  }

  renderAvatar() {
    if (this.state.avatarSource == null) {
      return (
        <View style={styles.portraitImage}>
          <CameraIcon />
        </View>
      );
    } else {
      return <Image source={this.state.avatarSource} style={styles.images} />;
    }
  }

  nextPage() {
    this.setState({current: this.state.current + 1});
    this.viewPager.current.setPage(this.state.current);
  }

  prevPage() {
    this.setState({current: this.state.current - 1});
    this.viewPager.current.setPage(this.state.current);
  }

  onPageScroll = (e: PageScrollEvent) => {
    this.setState({
      current: e.nativeEvent.position,
    });
  };

  move = (delta: number) => {
    const page = this.state.current + delta;
    if (this.state.current >= 0) {
      this.setState({page: page});
      this.go(page);
    } else {
      this.setState({page: page});
      this.go(page);
    }
  };

  go = (page: number) => {
    this.viewPager.current.setPage(page);
  };

  render() {
    return (
      <KeyboardAvoidingView style={styles.container1}>
        <StatusBar
          animated={true}
          backgroundColor={blue2}
          barStyle={'content-light'}
        />
        <LinearGradient colors={[white, white1]} style={styles.gradient}>
          <View style={styles.containerHeader}>
            <Image
              style={styles.handAbove}
              source={require('../assets/images/bg-hand-above.png')}
            />
            <View style={styles.headerContainer}>
              <Image
                style={styles.logoIcon1}
                source={require('../assets/images/logo-color.png')}
              />
              <Image
                style={styles.logoIcon}
                source={require('../assets/images/logo-bjb-color.png')}
              />
            </View>
            {this.state.current == 0 ? (
              <View style={styles.dotContainer}>
                <View style={styles.dotActive}>
                  <Text style={styles.textDotActive}>1</Text>
                </View>
                <View style={styles.underline1} />
                <View style={styles.dotInactive}>
                  <Text style={styles.textDotInactive}>2</Text>
                </View>
              </View>
            ) : (
              <View style={styles.dotContainer}>
                <View style={styles.dotActive}>
                  <Text style={styles.textDotActive}>1</Text>
                </View>
                <View style={styles.underline1} />
                <View style={styles.dotActive}>
                  <Text style={styles.textDotActive}>2</Text>
                </View>
              </View>
            )}
            {this.state.current == 0 ? (
              <View style={styles.dotLabelContainer}>
                <View>
                  <Text style={styles.textLabelActive}>Data Diri</Text>
                </View>
                <View>
                  <Text style={styles.textLabelInactive}>Data Usaha</Text>
                </View>
              </View>
            ) : (
              <View style={styles.dotLabelContainer}>
                <View>
                  <Text style={styles.textLabelInactive}>Data Diri</Text>
                </View>
                <View>
                  <Text style={styles.textLabelActive}>Data Usaha</Text>
                </View>
              </View>
            )}
          </View>
          <ViewPager
            scrollEnabled={false}
            style={styles.viewPager}
            initialPage={0}
            ref={this.viewPager}
            onPageScroll={this.onPageScroll}>
            <View key={1} style={styles.innerContainer}>
              <ScrollView ref={this.ScrollView}>
                {this.state.errorMessage === '' ? null : (
                  <Text style={styles.errorMessage}>
                    {this.state.errorMessage}
                  </Text>
                )}
                <View>
                  <Text style={styles.textContent1}>
                    Foto Diri {'\n'} (*Maximum 5MB)
                  </Text>
                  <TouchableNativeFeedback
                    onPress={() => {
                      this.pickImage();
                    }}>
                    {this.renderAvatar()}
                  </TouchableNativeFeedback>
                  {this.state.errorAvatar === '' ? null : (
                    <Text style={styles.errorMessage3}>
                      {this.state.errorAvatar}
                    </Text>
                  )}
                </View>
                <View>
                  <Text style={styles.textLabelInput}>Nama Lengkap*</Text>
                  <InputText
                    type={'default'}
                    placeholder={''}
                    showMessage={this.state.showMessageName}
                    value={this.state.name}
                    attrName={'name'}
                    updateMasterState={this._updateMasterState}
                    message={{
                      type: this.state.isErrorName ? 'error' : '',
                      text: this.state.messageName,
                    }}
                  />
                </View>
                <View>
                  <Text style={styles.textLabelInput}>Nomor Handphone*</Text>
                  <InputText
                    type={'number'}
                    placeholder={''}
                    showMessage={this.state.showMessagePhone}
                    value={this.state.phone}
                    attrName={'phone'}
                    updateMasterState={this._updateMasterState}
                    message={{
                      type: this.state.isErrorPhone ? 'error' : '',
                      text: this.state.messagePhone,
                    }}
                  />
                </View>
                <View>
                  <Text style={styles.textLabelInput}>
                    Nomor Rekening Bank BJB*
                  </Text>
                  <InputText
                    type={'number'}
                    placeholder={''}
                    showMessage={this.state.showMessageAccountNumber}
                    value={this.state.accountNumber}
                    attrName={'accountNumber'}
                    updateMasterState={this._updateMasterState}
                    message={{
                      type: this.state.isErrorAccountNumber ? 'error' : '',
                      text: this.state.messageAccountNumber,
                    }}
                  />
                </View>
                <View>
                  <Text style={styles.textLabelInput}>Foto KTP*</Text>
                  <TouchableNativeFeedback
                    onPress={() => {
                      this.takeIdPhoto();
                    }}>
                    {this.renderId()}
                  </TouchableNativeFeedback>
                  {this.state.errorID === '' ? null : (
                    <Text style={styles.errorMessage4}>
                      {this.state.errorID}
                    </Text>
                  )}
                </View>
                <View>
                  <Text style={styles.textLabelInput}>Email*</Text>
                  <InputText
                    type={'email'}
                    placeholder={''}
                    showMessage={this.state.showMessageEmail}
                    value={this.state.email}
                    attrName={'email'}
                    updateMasterState={this._updateMasterState}
                    message={{
                      type: this.state.isErrorEmail ? 'error' : '',
                      text: this.state.messageEmail,
                    }}
                  />
                </View>
                <View>
                  <Text style={styles.textLabelInput}>Kata Sandi*</Text>
                  <InputText
                    type={'password'}
                    placeholder={''}
                    showMessage={this.state.showMessagePassword}
                    value={this.state.password}
                    attrName={'password'}
                    updateMasterState={this._updateMasterState}
                    message={{
                      type: this.state.isErrorPassword ? 'error' : '',
                      text: this.state.messagePassword,
                    }}
                  />
                </View>
                <View>
                  <Text style={styles.textLabelInput}>
                    Konfirmasi Kata Sandi*
                  </Text>
                  <InputText
                    type={'password'}
                    placeholder={''}
                    showMessage={this.state.showMessagePasswordConfirmation}
                    value={this.state.passwordConfirmation}
                    attrName={'passwordConfirmation'}
                    updateMasterState={this._updateMasterState}
                    message={{
                      type: this.state.isErrorPasswordConfirmation
                        ? 'error'
                        : '',
                      text: this.state.messagePasswordConfirmation,
                    }}
                  />
                </View>
                <View>
                  <Text style={styles.textLink2}>*Tidak Boleh Kosong</Text>
                </View>
              </ScrollView>
            </View>
            <View key={2} style={styles.innerContainer}>
              <ScrollView ref={this.ScrollView1}>
                {this.state.errorMessage1 ? (
                  <Text style={styles.errorMessage}>
                    {this.state.errorMessage1}
                  </Text>
                ) : null}
                <View>
                  <Text style={styles.textLabelInput}>Nama Usaha</Text>
                  <InputText
                    type={'default'}
                    placeholder={''}
                    showMessage={this.state.showMessageBusinessName}
                    value={this.state.businessName}
                    attrName={'businessName'}
                    updateMasterState={this._updateMasterState}
                    message={{
                      type: this.state.isErrorBusinessName ? 'error' : '',
                      text: this.state.messageBusinessName,
                    }}
                  />
                </View>
                <View>
                  <Text style={styles.textLabelInput}>Deskripsi Usaha</Text>
                  <TextInput
                    multiline
                    numberOfLines={5}
                    style={styles.inputContainerStyle}
                    value={this.state.businessDescription}
                    onChangeText={(text) =>
                      this.setState({businessDescription: text})
                    }
                  />
                  {this.state.messageBusinessDescription === '' ? null : (
                    <Text style={styles.errorMessage2}>
                      {this.state.messageBusinessDescription}
                    </Text>
                  )}
                </View>
                <View>
                  <Text style={styles.textLabelInput}>Alamat</Text>
                  <TextInput
                    multiline
                    numberOfLines={3}
                    style={styles.inputContainerStyle}
                    value={this.state.businessAddress}
                    onChangeText={(text) =>
                      this.setState({businessAddress: text})
                    }
                  />
                  {this.state.messageBusinessAddress === '' ? null : (
                    <Text style={styles.errorMessage5}>
                      {this.state.messageBusinessAddress}
                    </Text>
                  )}
                </View>
                <View>
                  <Text style={styles.textLabelInput}>Provinsi</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({modalProvince: true});
                    }}
                    style={styles.autocomplete}>
                    <Text>
                      {this.state.province.name === ''
                        ? 'Pilih Provinsi'
                        : this.state.province.name}
                    </Text>
                  </TouchableOpacity>
                  {this._renderAddressModal(
                    'Provinsi',
                    this.state.provinceList,
                  )}
                  {this.state.errorProvince === '' ? null : (
                    <Text style={styles.errorMessage1}>
                      {this.state.errorProvince}
                    </Text>
                  )}
                </View>
                <View>
                  <Text style={styles.textLabelInput}>Kota/Kabupaten</Text>
                  <TouchableOpacity
                    onPress={() => {
                      if (this.state.province.id === '') {
                        this.setState({
                          errorCity: 'Pilih provinsi lebih dahulu',
                        });
                      } else {
                        this.searchAddress('Kota');
                        this.setState({modalCity: true, errorCity: ''});
                      }
                    }}
                    style={styles.autocomplete}>
                    <Text>
                      {this.state.city.name === ''
                        ? 'Pilih Kota/Kabupaten'
                        : this.state.city.name}
                    </Text>
                  </TouchableOpacity>
                  {this.state.errorCity === '' ? null : (
                    <Text style={styles.errorMessage1}>
                      {this.state.errorCity}
                    </Text>
                  )}
                  {this._renderAddressModal('Kota', this.state.cityList)}
                </View>
                <View>
                  <Text style={styles.textLabelInput}>Kecamatan</Text>
                  <TouchableOpacity
                    onPress={() => {
                      if (this.state.city.id === '') {
                        this.setState({
                          errorDistrict: 'Pilih Kota/Kabupaten lebih dahulu',
                        });
                      } else {
                        this.searchAddress('Kecamatan');
                        this.setState({modalDistrict: true, errorDistrict: ''});
                      }
                    }}
                    style={styles.autocomplete}>
                    <Text>
                      {this.state.district.name === ''
                        ? 'Pilih Kecamatan'
                        : this.state.district.name}
                    </Text>
                  </TouchableOpacity>
                  {this.state.errorDistrict === '' ? null : (
                    <Text style={styles.errorMessage1}>
                      {this.state.errorDistrict}
                    </Text>
                  )}
                  {this._renderAddressModal('Kecamatan', this.state.cityList)}
                </View>
                <View>
                  <Text style={styles.textLabelInput}>Kelurahan</Text>
                  <TouchableOpacity
                    onPress={() => {
                      if (this.state.district.id === '') {
                        this.setState({
                          errorVillage: 'Pilih Kecamatan lebih dahulu',
                        });
                      } else {
                        this.searchAddress('Kelurahan');
                        this.setState({modalVillage: true, errorVillage: ''});
                      }
                    }}
                    style={styles.autocomplete}>
                    <Text>
                      {this.state.village.name === ''
                        ? 'Pilih Kelurahan'
                        : this.state.village.name}
                    </Text>
                  </TouchableOpacity>
                  {this.state.errorVillage === '' ? null : (
                    <Text style={styles.errorMessage1}>
                      {this.state.errorVillage}
                    </Text>
                  )}
                  {this._renderAddressModal('Kelurahan', this.state.cityList)}
                </View>
                <View>
                  <Text style={styles.textLabelInput}>
                    Pilih Lokasi Pada Peta
                  </Text>
                  <Text style={styles.textLabelInput2}>
                    * Tap satu kali pada maps untuk memindahkan lokasi
                  </Text>
                  <MapView
                    provider={PROVIDER_GOOGLE}
                    style={styles.map}
                    onPress={(e) => {
                      let region = {
                        latitude: e.nativeEvent.coordinate.latitude,
                        longitude: e.nativeEvent.coordinate.longitude,
                        latitudeDelta: 0.005,
                        longitudeDelta: 0.005,
                      };
                      this.onRegionChange(region);
                    }}
                    region={this.state.region}
                    initialRegion={this.state.region}>
                    <Marker
                      draggable
                      coordinate={{
                        latitude: this.state.region.latitude,
                        longitude: this.state.region.longitude,
                      }}
                      title={'Lokasi Anda'}
                      onDragEnd={(e) => {
                        let region = {
                          latitude: e.nativeEvent.coordinate.latitude,
                          longitude: e.nativeEvent.coordinate.longitude,
                          latitudeDelta: 0.005,
                          longitudeDelta: 0.005,
                        };
                        this.onRegionChange(region);
                      }}>
                      <Image
                        source={require('../assets/icons/pin_map.png')}
                        style={{width: _.wp(7), height: _.wp(7)}}
                      />
                    </Marker>
                  </MapView>
                </View>
              </ScrollView>
            </View>
          </ViewPager>
          <View style={styles.bottomLayout}>
            {this.state.current == 0 ? (
              <TouchableNativeFeedback
                onPress={() => {
                  this.props.navigation.goBack(null);
                }}>
                <View style={styles.leftBottomButton}>
                  <Text style={styles.textLeftButton}>Batal</Text>
                </View>
              </TouchableNativeFeedback>
            ) : (
              <TouchableNativeFeedback
                onPress={() => {
                  this.move(-1);
                }}>
                <View style={styles.leftBottomButton}>
                  <Text style={styles.textLeftButton}>Kembali</Text>
                </View>
              </TouchableNativeFeedback>
            )}
            {this.state.current == 0 ? (
              <TouchableNativeFeedback
                onPress={() => {
                  this.validate();
                }}>
                <View style={styles.rightBottomButton}>
                  <Text style={styles.textRightButton}>Lanjut</Text>
                </View>
              </TouchableNativeFeedback>
            ) : (
              <TouchableNativeFeedback
                onPress={() => {
                  this.validate2();
                }}>
                <View style={styles.rightBottomButton}>
                  <Text style={styles.textRightButton}>Selesai</Text>
                </View>
              </TouchableNativeFeedback>
            )}
          </View>
          <Overlay isVisible={this.state.modalAlert}>
            <View style={styles.modalAlert}>
              {this.state.success === false ? (
                <ActivityIndicator size={'large'} color={blue2} />
              ) : (
                <View style={styles.containerHeader}>
                  <ImageSuccess />
                  <Text style={styles.textAlert}>
                    Registrasi sukses, E-mail konfirmasi telah dikirim ke
                    {' ' + this.state.email}. Segera konfirmasi akun anda untuk
                    dapat melakukan login.
                  </Text>
                </View>
              )}
            </View>
          </Overlay>
        </LinearGradient>
      </KeyboardAvoidingView>
    );
  }
}
