import React, {useState} from 'react';
import {DrawerContentScrollView} from '@react-navigation/drawer';
import {Image, TouchableOpacity, Text, View, SafeAreaView} from 'react-native';
import styles from '../screens/styles/CustomDrawerStyle';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Header, Body} from 'native-base';
import actions from '../actions';
import Logo from '../assets/images/logo-disentra-color.svg';

function CustomDrawerContent(props) {
  const [name, setName] = useState(null);
  const [email, setEmail] = useState(null);
  const [verified, setVerified] = useState(null);
  const [photo, setPhoto] = useState(null);

  const getName = async () => {
    let valName = await AsyncStorage.getItem('name');
    setName(valName);
  };
  const getEmail = async () => {
    const valEmail = await AsyncStorage.getItem('user_email');
    setEmail(valEmail);
  };
  const getVerified = async () => {
    const valVerified = await AsyncStorage.getItem('verified');
    setVerified(valVerified);
  };
  const getPhoto = async () => {
    const valPhoto = await AsyncStorage.getItem('photo');
    setPhoto(valPhoto);
  };
  getName();
  getEmail();
  getVerified();
  getPhoto();
  const {navigation} = props;
  return (
    <SafeAreaView style={styles.flex}>
      <Header style={styles.header}>
        <Body>
          <View style={styles.konten}>
            <Logo height={50} width={100} />
          </View>
          <TouchableOpacity
            style={styles.close}
            onPress={() => {
              navigation.closeDrawer();
            }}>
            <Image source={require('../assets/icons/close.png')} />
          </TouchableOpacity>
        </Body>
      </Header>
      <DrawerContentScrollView {...props}>
        <View style={styles.areaHeader}>
          {photo ? (
            <Image
              style={styles.avatar}
              source={{
                uri: photo,
              }}
            />
          ) : (
            <Image
              style={styles.avatar}
              source={require('../assets/images/avatar.png')}
            />
          )}
          <Text style={styles.name}>{name}</Text>
          <View style={styles.verifiedArea}>
            <Text style={styles.verified}>{verified}</Text>
            {verified === 'Terverifikasi' ? (
              <Image
                style={styles.verifiedIcon}
                source={require('../assets/icons/verified.png')}
              />
            ) : (
              <Image
                style={styles.verifiedIcon}
                source={require('../assets/icons/basic.png')}
              />
            )}
          </View>
          <Text style={styles.email}>{email}</Text>
          <View style={styles.divider} />
          <View style={styles.spacing} />
          <TouchableOpacity
            style={styles.listMenu}
            onPress={() => {
              navigation.navigate('Account');
            }}>
            <Text style={styles.textMenu}>Akun Saya</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listMenu}
            onPress={() => {
              navigation.navigate('BusinessSeminar');
            }}>
            <Text style={styles.textMenu}>Bincang Bisnis</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listMenu}
            onPress={() => {
              navigation.navigate('LearningCenter');
            }}>
            <Text style={styles.textMenu}>Sentra Belajar</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listMenu}
            onPress={() => {
              navigation.navigate('BusinessClinic');
            }}>
            <Text style={styles.textMenu}>Klinik Bisnis</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listMenu}
            onPress={() => {
              navigation.navigate('SMEnterprises');
            }}>
            <Text style={styles.textMenu}>Pasar UMKM</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listMenu}
            onPress={() => {
              navigation.navigate('Forum');
            }}>
            <Text style={styles.textMenu}>Ruang Pertemuan</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listMenu}
            onPress={() => {
              navigation.navigate('Forum');
            }}>
            <Text style={styles.textMenu}>Temu Bisnis</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listMenu}
            onPress={() => {
              navigation.navigate('Forum');
            }}>
            <Text style={styles.textMenu}>Info Pasar</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listMenu}
            onPress={() => {
              navigation.navigate('Forum');
            }}>
            <Text style={styles.textMenu}>Forum</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listMenu}
            onPress={() => {
              navigation.navigate('Kontak');
            }}>
            <Text style={styles.textMenu}>Kontak Kami</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.listMenu}
            onPress={() => {
              actions.removeToken();
              navigation.replace('Auth');
            }}>
            <Text style={styles.textMenu}>Keluar</Text>
          </TouchableOpacity>
        </View>
      </DrawerContentScrollView>
    </SafeAreaView>
  );
}

export default CustomDrawerContent;
