/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Linking,
  View,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from './styles/ContentStyle';
import LinearGradient from 'react-native-linear-gradient';
import {white, white1, blue2} from '../constants/colors';
import {Header, Body} from 'native-base';
import axios from 'axios';
import config from '../config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import actions from '../actions';
import Carousel, {ParallaxImage, Pagination} from 'react-native-snap-carousel';
const BannerWidth = Dimensions.get('window').width;
const uri = 'api/v1/mobile/banners?banner_type=home&q[status_eq]=active';
import _ from '../helpers/responsive';

import Logo from '../assets/images/logo-disentra-color.svg';
import BanBincang from '../assets/images/bnr-bincang-bisnis.svg';
import BanInfoPasar from '../assets/images/bnr-info-pasar.svg';
import BanKlinik from '../assets/images/bnr-klinik-bisnis.svg';
import BanRuang from '../assets/images/bnr-ruang.svg';
import BanTemu from '../assets/images/bnr-temu.svg';
import BanUmkm from '../assets/images/bnr-umkm.svg';
import BanSentra from '../assets/images/brn-sentra.svg';

function WelcomeScreen(props) {
  const [token, setToken] = useState(null);
  const [banner, setBanner] = useState([]);
  const [activeDot, setActiveDot] = useState(1);
  const getToken = async () => {
    const value = await AsyncStorage.getItem('access_token');
    setToken(value);
  };
  getToken();
  useEffect(() => {
    if (token !== null) {
      getData();
    }
  }, [token]);

  function getData() {
    axios
      .get(config.base_url + uri, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((response) => {
        setBanner(response.data.data);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          actions.removeToken();
          props.navigation.replace('Auth');
        }
      });
  }

  function renderPage({item, index}, parallaxProps) {
    return (
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('WebView', {
            link: item.link,
            header: 'Berita',
            title: 'Banner',
          });
        }}
        style={styles.item}>
        <ParallaxImage
          source={{uri: item.image.url}}
          containerStyle={styles.imageContainer}
          style={styles.image}
          parallaxFactor={0.4}
          {...parallaxProps}
        />
      </TouchableOpacity>
    );
  }

  return (
    <LinearGradient colors={[white, white1]} style={styles.gradient}>
      <Header style={styles.header} androidStatusBarColor={blue2}>
        <Body>
          <View style={styles.konten}>
            <Logo height={50} width={100} />
          </View>
        </Body>
      </Header>
      <SafeAreaView style={styles.container}>
        <ScrollView style={styles.welcomeMessage}>
          {banner.length > 0 ? (
            <View style={styles.Carousel}>
              <Carousel
                sliderWidth={BannerWidth}
                sliderHeight={BannerWidth}
                itemWidth={BannerWidth - 60}
                data={banner}
                renderItem={renderPage}
                hasParallaxImages={true}
                autoplay={true}
                loop={true}
                onSnapToItem={(index) => {
                  setActiveDot(index);
                }}
              />
              <Pagination
                dotsLength={banner.length}
                activeDotIndex={activeDot}
                containerStyle={styles.containerStyle}
                dotStyle={styles.dotStyle}
                inactiveDotStyle={styles.inactiveDotStyle}
                inactiveDotOpacity={1}
                inactiveDotScale={0.6}
                activeOpacity={2}
              />
            </View>
          ) : null}
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate('BusinessSeminar');
            }}>
            <View style={styles.konten}>
              <BanBincang height={130} width={_.wp(90)} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate('LearningCenter');
            }}>
            <View style={styles.konten}>
              <BanSentra height={130} width={_.wp(90)} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate('SMEnterprises');
            }}>
            <View style={styles.konten}>
              <BanUmkm height={130} width={_.wp(90)} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate('BusinessClinic');
            }}>
            <View style={styles.konten}>
              <BanKlinik height={130} width={_.wp(90)} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate('Forum');
            }}>
            <View style={styles.konten}>
              <BanRuang height={130} width={_.wp(90)} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate('Forum');
            }}>
            <View style={styles.konten}>
              <BanTemu height={130} width={_.wp(90)} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate('Forum');
            }}>
            <View style={styles.konten}>
              <BanInfoPasar height={130} width={_.wp(90)} />
            </View>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    </LinearGradient>
  );
}

export default WelcomeScreen;
