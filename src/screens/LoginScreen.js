import React from 'react';
import {
  Image,
  Text,
  View,
  KeyboardAvoidingView,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Cf from '../config';
import styles from '../screens/styles/LoginStyle';
import {white, white1} from '../constants/colors';
import {ButtonL} from '../components/Buttons';
import InputText from '../components/InputText';
import LinearGradient from 'react-native-linear-gradient';
import {regEmail} from '../constants/commons';
import IconMail from '../assets/icons/ic_mail.svg';
import IconLock from '../assets/icons/ic_lock.svg';
import IconGoogle from '../assets/icons/ic_google.svg';
import {GoogleSignin} from '@react-native-community/google-signin';
GoogleSignin.configure({
  scopes: ['email'],
  webClientId: Cf.key_google_auth,
  offlineAccess: true,
});
export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFocused: false,
      isError: false,
      showMessageEmail: false,
      showMessagePassword: false,
      isErrorEmail: false,
      isErrorPassword: false,
      email: '',
      password: '',
      errorMessage: '',
      messageEmail: '',
      messagePassword: '',
      isLoading: false,
      isLoading2: false,
    };
  }

  signIn = async () => {
    this.setState({isLoading2: true});
    await GoogleSignin.hasPlayServices();
    const userInfo = await GoogleSignin.signIn();
    fetch(
      Cf.base_url +
        'api/v1/users/auth/google_oauth2/callback?code=' +
        userInfo.serverAuthCode +
        '&scope=' +
        userInfo.scopes +
        '&authuser=0&prompt=none#',
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    )
      .then((res) => res.json())
      .then((response) => {
        if (response.access_token !== undefined) {
          this.storeData(response);
          setTimeout(() => {
            this.props.navigation.replace('Drawers');
          }, 1000);
        } else {
          setTimeout(() => {
            this.setState({
              isError: true,
              isLoading2: false,
              errorMessage: response.error_description,
            });
          }, 2000);
        }
      });
  };

  _updateMasterState = (attrName, value) => {
    if (attrName === 'email' && this.state.isErrorEmail) {
      this.setState({
        isErrorEmail: false,
        showMessageEmail: false,
      });
    }
    if (attrName === 'password' && this.state.isErrorPassword) {
      this.setState({
        isErrorPassword: false,
        showMessagePassword: false,
      });
    }
    this.setState({[attrName]: value});
  };

  storeData = async (response) => {
    await AsyncStorage.setItem('user_email', response.data.email);
    await AsyncStorage.setItem('access_token', response.access_token);
    await AsyncStorage.setItem('token_type', response.token_type);
    await AsyncStorage.setItem('created_at', response.created_at.toString());
    await AsyncStorage.setItem('name', response.data.name);
    await AsyncStorage.setItem(
      'verified',
      response.data.verified ? 'Terverifikasi' : 'Basic',
    );
    await AsyncStorage.setItem('photo', response.data.photo.url.toString());
  };

  _handleLogin() {
    this.setState({isLoading: true});
    fetch(Cf.base_url + 'oauth/token', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
        scope: 'mobile',
        grant_type: 'password',
        client_id: Cf.client_id,
        client_secret: Cf.client_secret,
      }),
    })
      .then((res) => res.json())
      .then((response) => {
        if (response.access_token !== undefined) {
          this.storeData(response);
          setTimeout(() => {
            this.props.navigation.replace('Drawers');
            this.setState({
              isLoading: false,
            });
          }, 1000);
        } else {
          setTimeout(() => {
            this.setState({
              isError: true,
              isLoading: false,
              errorMessage:
                response.error_description === 'User not found'
                  ? 'Email tidak terdaftar, tolong cek kembali email anda'
                  : response.error_description === 'Wrong Password'
                  ? 'Kata sandi salah, tolong cek kembali kata sandi anda'
                  : response.error_description,
            });
          }, 2000);
        }
      });
  }

  validate() {
    if (this.state.email === '') {
      this.setState({
        isErrorEmail: true,
        showMessageEmail: true,
        messageEmail: 'Email Tidak Boleh Kosong',
      });
    } else if (!regEmail.test(this.state.email)) {
      this.setState({
        isErrorEmail: true,
        showMessageEmail: true,
        messageEmail: 'Email Tidak Valid',
      });
    } else {
      this.setState({
        isErrorEmail: false,
        showMessageEmail: false,
      });
    }
    if (this.state.password === '') {
      this.setState({
        isErrorPassword: true,
        showMessagePassword: true,
        messagePassword: 'Password Tidak Boleh Kosong',
      });
    } else {
      this.setState({
        isErrorPassword: false,
        showMessagePassword: false,
      });
    }

    if (
      this.state.email === '' ||
      this.state.password === '' ||
      !regEmail.test(this.state.email)
    ) {
    } else {
      this._handleLogin();
    }
  }

  render() {
    return (
      <ScrollView>
        <KeyboardAvoidingView style={styles.container}>
          <LinearGradient colors={[white, white1]} style={styles.gradient}>
            <SafeAreaView style={styles.innerContainer}>
              <Image
                style={styles.logoIcon}
                source={require('../assets/images/logo-bjb-color.png')}
              />
              <Image
                style={styles.handAbove}
                source={require('../assets/images/bg-hand-above.png')}
              />
              <Image
                style={styles.logo}
                source={require('../assets/images/logo-color.png')}
              />
              {this.state.isError ? (
                <Text style={styles.errorMessage}>
                  {this.state.errorMessage}
                </Text>
              ) : null}
              <InputText
                type={'email'}
                placeholder={'Email'}
                showMessage={this.state.showMessageEmail}
                value={this.state.email}
                attrName={'email'}
                updateMasterState={this._updateMasterState}
                message={{
                  type: this.state.isErrorEmail ? 'error' : '',
                  text: this.state.messageEmail,
                }}
                icons={<IconMail />}
              />
              <InputText
                type={'password'}
                placeholder={'Password'}
                showMessage={this.state.showMessagePassword}
                value={this.state.password}
                attrName={'password'}
                updateMasterState={this._updateMasterState}
                message={{
                  type: this.state.isErrorPassword ? 'error' : '',
                  text: this.state.messagePassword,
                }}
                icons={<IconLock />}
              />
              <ButtonL
                title="MASUK"
                loading={this.state.isLoading}
                onPress={() => {
                  this.validate();
                }}
              />
              <View style={styles.containerUnderline}>
                <View style={styles.underline} />
                <Text style={styles.text}>atau</Text>
                <View style={styles.underline} />
              </View>

              <ButtonL
                title="MASUK DENGAN GOOGLE  "
                type="outline-icon"
                loading={this.state.isLoading2}
                icon={<IconGoogle />}
                onPress={() => {
                  this.signIn();
                }}
              />
              <Text
                onPress={() => {
                  this.props.navigation.navigate('Forgot');
                }}
                style={styles.textLink}>
                Lupa kata sandi?
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('Register');
                }}>
                <Text style={styles.textLink2}>
                  Tidak punya akun?<Text style={styles.textLink}> daftar</Text>
                </Text>
              </TouchableOpacity>
            </SafeAreaView>
          </LinearGradient>
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }
}
