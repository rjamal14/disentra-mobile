import React from 'react';
import {View, Text, Image, StatusBar} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from '../screens/styles/WelcomeStyle';
import LinearGradient from 'react-native-linear-gradient';
import {white, white1, blue2} from '../constants/colors';
import {ButtonL} from '../components/Buttons';

function WelcomeScreen(props) {
  return (
    <LinearGradient colors={[white, white1]} style={styles.gradient}>
      <StatusBar
        animated={true}
        backgroundColor={blue2}
        barStyle={'content-light'}
      />
      <SafeAreaView style={styles.container}>
        <Image
          style={styles.logoIcon}
          source={require('../assets/images/logo-bjb-color.png')}
        />
        <View style={styles.welcomeMessage}>
          <Text style={styles.textHeader}> SELAMAT DATANG DI</Text>
          <Image
            style={styles.logo}
            source={require('../assets/images/logo-color.png')}
          />
        </View>
        <Image
          style={styles.handAbove}
          source={require('../assets/images/bg-hand-above.png')}
        />
        <Text style={styles.textContent}>
          Yuk! Bergabung dan jadi UMKM JUARA bersama Komunitas UMKM Bank BJB
        </Text>
        <ButtonL
          title="GABUNG SEKARANG"
          onPress={() => {
            props.navigation.navigate('Register');
          }}
        />
        <ButtonL
          type="outline"
          title="PUNYA AKUN? MASUK"
          onPress={() => {
            props.navigation.navigate('Login');
          }}
        />
      </SafeAreaView>
    </LinearGradient>
  );
}

export default WelcomeScreen;
