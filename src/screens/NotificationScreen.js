import React, {Component} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {SafeAreaView} from 'react-native-safe-area-context';
import {
  TouchableOpacity,
  Image,
  View,
  Text,
  RefreshControl,
  FlatList,
  Modal,
  Linking,
} from 'react-native';
import {white, white1, blue2} from '../constants/colors';
import {Header, Left, Right, Body, Title, Card} from 'native-base';
import styles from '../screens/styles/NotificationStyle';
import NotifImage from '../assets/icons/ic_notif_item.svg';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import 'moment/locale/id';
import axios from 'axios';
import Cf from '../config';
import actions from '../actions';

export default class RenewPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifList: [],
      modalVisible: false,
      selectedItem: '',
      selectedItemEvent: '',
      refresh: false,
      pagination: '',
    };
  }

  fetchNotifData(token, next = false) {
    axios
      .get(
        Cf.base_url +
          Cf.notification +
          '?page=' +
          (next ? this.state.pagination.next_page : 1),
        {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        },
      )
      .then((res) => {
        if (next) {
          var data = this.state.notifList;
          var dataCon = data.concat(res.data.data);
          this.setState({
            notifList: dataCon,
          });
        } else {
          this.setState({
            notifList: res.data.data,
          });
        }
        this.setState({
          pagination: res.data.pagination,
        });
      })
      .catch((error) => {
        if (error.response.status === 401) {
          actions.removeToken();
          this.props.navigation.replace('Auth');
        }
      });
  }

  fetchReadNotifData(token) {
    axios
      .post(
        Cf.base_url + Cf.notification_read,
        {},
        {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        },
      )
      .then((response) => {})
      .catch((error) => {
        if (error.response.status === 401) {
          actions.removeToken();
          this.props.navigation.replace('Auth');
        }
      });
  }

  timeAgo(date) {
    moment.locale('id');
    var m = moment(date);
    var time = m.fromNow();
    return time;
  }

  dateFormat(date) {
    moment.locale('id');
    var m = moment(date);
    var tanggal = m.format('LL');
    return tanggal;
  }

  componentDidMount() {
    AsyncStorage.getItem('access_token').then((token) => {
      this.fetchNotifData(token);
      this.fetchReadNotifData(token);
    });
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      AsyncStorage.getItem('access_token').then((token) => {
        this.fetchNotifData(token);
        this.fetchReadNotifData(token);
      });
    });
  }
  componentWillUnmount() {
    this._unsubscribe();
  }

  renderNotification() {
    if (this.state.notifList.length > 0) {
      return (
        <FlatList
          keyExtractor={(item) => item.id.toString()}
          showsVerticalScrollIndicator={false}
          onEndReached={() => {
            if (
              this.state.pagination !== null &&
              this.state.pagination.next_page !== null
            ) {
              AsyncStorage.getItem('access_token').then((token) => {
                this.fetchNotifData(token, true);
              });
            }
          }}
          onEndReachedThreshold={7}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refresh}
              onRefresh={() => {
                AsyncStorage.getItem('access_token').then((token) => {
                  this.fetchNotifData(token);
                  this.fetchReadNotifData(token);
                });
              }}
            />
          }
          data={this.state.notifList}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  modalVisible: true,
                  selectedItem: item,
                  selectedItemEvent: item.event !== null ? item.event : '',
                });
              }}
              style={styles.notifItem}>
              <View style={styles.notifImage}>
                <NotifImage />
                {!item.has_read ? <View style={styles.badges} /> : null}
                <View />
              </View>
              <View style={styles.contentContainer}>
                <Text numberOfLines={1} style={styles.notifTitle}>
                  {item.title}
                </Text>
                <Text numberOfLines={1} style={styles.notifSubtitle}>
                  {item.event !== null ? item.event.title : ''}
                </Text>
                <Text style={styles.notifTime}>
                  {this.timeAgo(item.created_at)}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      );
    } else {
      return (
        <View style={styles.cardContentEmpty}>
          <Text style={styles.textContent}>Belum ada notifikasi.</Text>
        </View>
      );
    }
  }

  render() {
    return (
      <LinearGradient colors={[white, white1]} style={styles.gradient}>
        <SafeAreaView style={styles.container}>
          <Header style={styles.header} androidStatusBarColor={blue2}>
            <Body>
              <Title style={styles.textHeader}>Notifikasi</Title>
            </Body>
            <Right />
          </Header>
          <View style={styles.cardContentEmpty}>
            {this.renderNotification()}
          </View>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              this.setState({
                modalVisible: false,
              });
            }}>
            <View style={styles.modalContentContainer}>
              <Header style={styles.header} androidStatusBarColor={blue2}>
                <Left>
                  <TouchableOpacity
                    style={styles.backButton}
                    onPress={() => {
                      this.setState({
                        modalVisible: false,
                      });
                    }}>
                    <Image source={require('../assets/icons/arrow-left.png')} />
                  </TouchableOpacity>
                </Left>
                <Body>
                  <Title style={styles.textHeader}>Detail Notifikasi</Title>
                </Body>
                <Right />
              </Header>
              <View style={styles.modalContent}>
                <Text style={styles.notifTitle1}>
                  {this.state.selectedItem.title}
                </Text>
                <Text style={styles.notifTime1}>
                  {this.dateFormat(this.state.selectedItemEvent.event_on) +
                    ' ' +
                    this.state.selectedItemEvent.start_time +
                    ' WIB'}
                </Text>
                <Text style={styles.notifSubtitle1}>
                  Anda baru saja mendaftarkan diri pada
                  {' ' + this.state.selectedItemEvent.talk_type}:
                </Text>
                <Card style={styles.cards}>
                  <Text style={styles.notifTitle2}>
                    {this.state.selectedItemEvent.title}
                  </Text>
                  <View style={styles.MaskedView} />
                  <Image
                    style={styles.eventImage}
                    source={{
                      uri:
                        this.state.selectedItemEvent === ''
                          ? ''
                          : this.state.selectedItemEvent?.image?.url,
                    }}
                  />
                </Card>
                <Text style={styles.notifSubtitle1}>
                  Silahkan bergabung di
                  {' ' + this.state.selectedItemEvent.talk_type + ' '} dengan
                  menekan tautan dibawah ini :
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    Linking.openURL(this.state.selectedItemEvent.link);
                  }}>
                  <Text style={styles.link}>
                    {this.state.selectedItemEvent.link}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        </SafeAreaView>
      </LinearGradient>
    );
  }
}
