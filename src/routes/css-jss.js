import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  iconMenu: {
    width: 28,
    height: 28,
  },
  iconMain: {
    marginBottom: -15,
    marginLeft: 5,
  },
  tabStyle: {
    margin: 10,
  },
  style: {
    height: 72,
  },
  badges: {
    backgroundColor: 'red',
    padding: 4,
    borderRadius: 10,
    position: 'absolute',
    top: 2,
    right: 2,
  },
});
