/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {View} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import SplashScreen from '../screens/SplashScreen';
import WelcomeScreen from '../screens/WelcomeScreen';
import LoginScreen from '../screens/LoginScreen';
import ForgotPassword from '../screens/ForgotPasswordScreen';
import ResetPassword from '../screens/PasswordResetScreen';
import BusinessSeminar from '../screens/BusinessSeminar';
import Forum from '../screens/Forum';
import ContactUs from '../screens/ContactUsScreen';
import DrawerNavigation from '../screens/DrawerNavigation';
import ContentScreen from '../screens/content';
import CustomDrawerContent from '../screens/CustomDrawerContent';
import {createDrawerNavigator} from '@react-navigation/drawer';
import RegisterScreen from '../screens/RegisterScreen';
import BussinessRegistrationScreen from '../screens/BussinessRegistrationScreen';
import LearningCenter from '../screens/LearningCenterScreen';
import WebViewScreen from '../screens/LearningCenterScreen/webview';
import detailSeminar from '../screens/BusinessSeminar/detailSeminar';
import resultSeminar from '../screens/BusinessSeminar/resultSeminar';
import MyAccount from '../screens/MyAccountScreen';
import RenewProfile from '../screens/RenewProfileScreen';
import RenewPassword from '../screens/RenewPassword';
import SMEnterprises from '../screens/SMEnterprises';
import detailSMEnterprises from '../screens/SMEnterprises/detailSMEnterprises';
import Notification from '../screens/NotificationScreen';
import actions from '../actions';

import IconForum from '../assets/icons/ic_users.svg';
import IconNotif from '../assets/icons/ic_notification.svg';
import IconDisentra from '../assets/icons/ic_disentra.svg';
import IconContact from '../assets/icons/ic_contact.svg';
import IconOther from '../assets/icons/ic_other_menu.svg';

import styles from './css-jss';
import BusinessClinic from '../screens/BusinessClinicScreen';
import axios from 'axios';
import config from '../config';

const Stack = createStackNavigator();
const ContentStack = createStackNavigator();
const AuthStack = createStackNavigator();
const AccountStack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const uri = 'api/v1/mobile/notifications_count';

function Content() {
  return (
    <ContentStack.Navigator
      initialRouteName={'ContentScreen'}
      screenOptions={{headerShown: false}}>
      <ContentStack.Screen name="ContentScreen" component={ContentScreen} />
      <ContentStack.Screen name="BusinessSeminar" component={BusinessSeminar} />
      <ContentStack.Screen name="LearningCenter" component={LearningCenter} />
      <ContentStack.Screen name="WebView" component={WebViewScreen} />
      <ContentStack.Screen name="detailSeminar" component={detailSeminar} />
      <ContentStack.Screen name="resultSeminar" component={resultSeminar} />
      <ContentStack.Screen name="BusinessClinic" component={BusinessClinic} />
      <ContentStack.Screen name="SMEnterprises" component={SMEnterprises} />
      <ContentStack.Screen
        name="detailSMEnterprises"
        component={detailSMEnterprises}
      />
    </ContentStack.Navigator>
  );
}

function Account() {
  return (
    <AccountStack.Navigator
      initialRouteName={'MyAccount'}
      screenOptions={{headerShown: false}}>
      <AccountStack.Screen name="MyAccount" component={MyAccount} />
      <AccountStack.Screen name="RenewProfile" component={RenewProfile} />
      <AccountStack.Screen name="RenewPassword" component={RenewPassword} />
    </AccountStack.Navigator>
  );
}

function Auth() {
  return (
    <AuthStack.Navigator
      initialRouteName={'Welcome'}
      screenOptions={{headerShown: false}}>
      <AuthStack.Screen name="Welcome" component={WelcomeScreen} />
      <AuthStack.Screen name="Login" component={LoginScreen} />
      <AuthStack.Screen name="Forgot" component={ForgotPassword} />
      <AuthStack.Screen name="Reset" component={ResetPassword} />
      <AuthStack.Screen name="Register" component={RegisterScreen} />
      <AuthStack.Screen
        name="BussinessRegistration"
        component={BussinessRegistrationScreen}
      />
    </AuthStack.Navigator>
  );
}

function Main() {
  const [notifications, Setnotifications] = useState(0);
  const [token, setToken] = useState(null);

  function getNotification() {
    axios
      .get(config.base_url + uri, {
        headers: {
          Authorization: 'Bearer ' + token,
        },
      })
      .then((response) => {
        Setnotifications(response.data.data.count);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          actions.removeToken();
        }
      });
  }

  const getToken = async () => {
    const value = await AsyncStorage.getItem('access_token');
    setToken(value);
  };
  getToken();
  useEffect(() => {
    if (token !== null) {
      getNotification();
    }
  }, [token]);

  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: () => {
          let iconPath;
          if (route.name === 'Forum') {
            iconPath = <IconForum />;
          } else if (route.name === 'Notifikasi') {
            iconPath = (
              <View>
                <IconNotif />
                {notifications > 0 ? <View style={styles.badges} /> : null}
              </View>
            );
          } else if (route.name === ' ') {
            iconPath = (
              <View style={styles.iconMain}>
                <IconDisentra height={65} width={65} />
              </View>
            );
          } else if (route.name === 'Kontak') {
            iconPath = <IconContact />;
          } else if (route.name === 'Menu Lain') {
            iconPath = <IconOther />;
          }
          return iconPath;
        },
      })}
      initialRouteName=" "
      tabBarOptions={{
        showLabel: true,
        showIcon: true,
        tabStyle: styles.tabStyle,
        style: styles.style,
      }}>
      <Tab.Screen name="Forum" component={Forum} />
      <Tab.Screen name="Notifikasi" component={Notification} />
      <Tab.Screen name=" " component={Content} />
      <Tab.Screen name="Kontak" component={ContactUs} />
      <Tab.Screen name="Menu Lain" component={DrawerNavigation} />
    </Tab.Navigator>
  );
}

function Drawers() {
  return (
    <Drawer.Navigator
      drawerPosition="right"
      initialRouteName="Home"
      drawerContent={(props) => <CustomDrawerContent {...props} />}>
      <Drawer.Screen
        name="Home"
        options={{
          drawerLabel: () => null,
          title: null,
          activeBackgroundColor: 'white',
          drawerIcon: () => null,
        }}
        component={Main}
      />
      <Drawer.Screen name="Account" component={Account} />
    </Drawer.Navigator>
  );
}

function RouterStack() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Splash" component={SplashScreen} />
      <Stack.Screen name="Auth" component={Auth} />
      <Stack.Screen name="Main" component={Main} />
      <Stack.Screen name="Drawers" component={Drawers} />
    </Stack.Navigator>
  );
}

export default RouterStack;
