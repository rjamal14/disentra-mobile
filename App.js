/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import RouterStack from './src/routes/index';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import 'react-native-gesture-handler';

function App() {
  const config = {
    screens: {
      Auth: {
        screens: {
          Reset: 'auth/reset-password',
        },
      },
    },
  };

  const linking = {
    prefixes: ['https://areon.doterb.com', 'areonapp://areon.doterb.com'],
    config,
  };

  return (
    <SafeAreaProvider>
      <NavigationContainer linking={linking}>
        <RouterStack />
      </NavigationContainer>
    </SafeAreaProvider>
  );
}

export default App;
