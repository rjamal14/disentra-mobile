/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-async-storage/async-storage';

const setStringValue = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {}
};

messaging().setBackgroundMessageHandler(async (remoteMessage) => {
  setStringValue('Title', remoteMessage.data.title);
  setStringValue('Body', remoteMessage.data.body);
});

AppRegistry.registerComponent(appName, () => App);
